var cors = require('cors');
var express = require('express'),
  app = express();
  router = express.Router(),
  mongoose = require('mongoose'),
  KategoriMenu = mongoose.model('KategoriMenu');

module.exports = function (app) {
  app.use(cors());
  app.use('/v1', router);
};



// =====================
//-------------
// Kategori-Add
router.post('/kategori', function(req, res, next){
	createIdKategori(req.body.id_brand, req.body.name_kategori, function(result){
		if (result == 'error'){
			res.status(409).json({status: 'Conflict Name Kategori'});
		} else if (result == 'maxKategori'){
			res.status(403).json({status: 'Max Kategori'});
		} else {
			var dataold = {id_brand: req.body.id_brand};
			var datanew = {$push:{kategori_menu:{	id_kategori_menu: result,
													name_kategori_menu: req.body.name_kategori
												}}};
			var options = {};
			KategoriMenu.update(dataold, datanew, options, callback);
			function callback(err){
				if (err){
					console.log(err);
				} else {
					res.status(200).json({status:'Add Kategori Success'});
				};
			};
		};
	});
});

//-------------
// Kategori-Update
router.put('/kategori', function(req, res, next){
	var data = KategoriMenu.findOne({$and:[{id_brand: req.body.id_brand},{'kategori_menu.id_kategori_menu':req.body.id_kategori}]});
	data.exec(function(err, result){
		if (err){
			console.log(err);
		} else {
			if (result == null){
				res.status(404).json({status:'Not found Kategori'});
			} else {
				var available = false;
				for (var i=0; i<result.kategori_menu.length; i++){
					var kategoriResult = result.kategori_menu[i].name_kategori_menu.toLowerCase();
					var kategoriInput = req.body.name_kategori.toLowerCase();	
					if ((kategoriResult == kategoriInput) && (result.kategori_menu[i].deleted == false) && (result.kategori_menu[i].id_kategori_menu != req.body.id_kategori)){
						available = true;
						res.status(409).json({status:'Conflict Name Kategori'});
						break;
					};
					if (i == result.kategori_menu.length-1 && available == false){
						var dataold = {id_brand:req.body.id_brand, 'kategori_menu.id_kategori_menu': req.body.id_kategori};
						var datanew = {'kategori_menu.$.name_kategori_menu': req.body.name_kategori};
						var options = {};
						KategoriMenu.update(dataold, datanew, options, callback);
						function callback(err){
							if (err){
								console.log(err);
							} else {
								res.status(200).json({status:'Update Success'});
							};
						};
					};
				};
			};
		};
	});
});

//-------------
// Kategori-Delete
router.put('/delete/kategori', function(req, res, next){
	var data = KategoriMenu.findOne({$and:[{id_brand: req.body.id_brand},{'kategori_menu.id_kategori_menu':req.body.id_kategori}]});
	data.exec(function(err, result){
		if (err){
			console.log(err);
		} else {
			if (result == null){
				res.status(404).json({status:'Kategori Not Found'});
			} else {
				var dataold = {id_brand:req.body.id_brand, 'kategori_menu.id_kategori_menu': req.body.id_kategori};
				var datanew = {'kategori_menu.$.deleted': true};
				var options = {};
				KategoriMenu.update(dataold, datanew, options, callback);
				function callback(err){
					if (err){
						console.log(err);
					} else {
						res.status(200).json({status:'Update Success'});
					};
				};
			};
		};
	});
});

//-------------
// Kategori-Get
router.get('/kategori/:idBrand', function(req, res, next){
	var data = KategoriMenu.findOne({id_brand: req.params.idBrand});
	data.exec(function(err, result){
		if (err){
			console.log(err);
		} else {
			if (result == null){
				res.status(404).json({status:'Not Found'});
			} else {
				res.status(200).json({result:result});
			};
		};
	});
});
// =====================



function createIdKategori(idBrand, nameKategori, callback){
	var data = KategoriMenu.findOne({id_brand: idBrand});
  	data.exec(function(err, result){
	    if (err){
	      console.log(err);
	    } else {
	    	var idKategori;
	    	var available = false;
	      	var arrayNumber =[];
	      	var number;
	      	for (var i=0; i<result.kategori_menu.length; i++){
	      		var kategoriResult = result.kategori_menu[i].name_kategori_menu.toLowerCase();
				var kategoriInput = nameKategori.toLowerCase();				
      			if (kategoriResult == kategoriInput && result.kategori_menu[i].deleted == false){
      				available = true;	
      				callback('error');
      				break;
      			};
      			if (i == result.kategori_menu.length-1 && available == false){
      				//replace result id_kategori(read: 3 digit terakhir) ke arrayNumber(posisi msh blm urut dr kecil ke besar)
			        for (var i=0; i<result.kategori_menu.length; i++){
			          arrayNumber.push(parseInt(result.kategori_menu[i].id_kategori_menu.substring(2,5)));
			        };
			     	//sort id_kategori(read: 3 digit terakhir) dr bilangan kecil ke besar pada arrayNumber
			        for (var i=0; i<arrayNumber.length;i++){
			          for (var j=0; j<arrayNumber.length; j++){
			            if (arrayNumber[j]>arrayNumber[j+1]){
			              temp = arrayNumber[j];
			              arrayNumber[j] = arrayNumber[j+1];
			              arrayNumber[j+1] = temp;
			            }; 
			          };
			        };
				    //cari nilai id_kategori(read: 3 digit terakhir) yg blm terpakai dr hasil urutan arrayNumber. 
				    //Tujuan: biar gak mubadzir digit. 
			        for (var i=0; i<arrayNumber.length; i++){
			          if (i == 0 && arrayNumber[i] != 1){
			            number = 1;
			            break
			          } else if (i == (arrayNumber.length-1)){
			            number = i+2;
			            break;
			          } else {
			            if (arrayNumber[i+1] != i+2){
			              number = i+2;
			              break;
			            };
			          };
			        };
			    	//susun format id_satuan
			        if (number <= 9){
			          	idKategori = 'K.00'+number;
			        } else if (number <= 99){
			          	idKategori = 'K.0'+number;
			        } else if (number <= 999){
			          	idKategori = 'K.'+number;
			        } else {
			        	idKategori = 'maxKategori';
			        };
			        callback(idKategori);
      			};
	      	};
	    };
	});
};
