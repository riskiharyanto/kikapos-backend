var cors = require('cors');
var fs = require('fs');
var express = require('express'),
  app = express(),
  router = express.Router();

module.exports = function (app) {
  app.use(cors());
  app.use('/v1', router);
};

// =====================
//-------------
// Image-Show Image Brand
router.get('/public/image_upload/image_brand/:nameFile', function(req,res,next){
	console.log(req.params.nameFile);
	fs.readFile('public/image_upload/image_brand/'+req.params.nameFile, function(err, data) {
	  if (err) throw err; // Fail if the file can't be read.
	  res.end(data);
	});
});

//-------------
// Image-Show Image Store
router.get('/public/image_upload/image_store/:nameFile', function(req,res,next){
	console.log(req.params.nameFile);
	fs.readFile('public/image_upload/image_store/'+req.params.nameFile, function(err, data) {
	  if (err) throw err; // Fail if the file can't be read.
	  res.end(data);
	});
});

//-------------
// Image-Show Image User(Ava)
router.get('/public/image_upload/image_user/:nameFile', function(req,res,next){
	console.log(req.params.nameFile);
	fs.readFile('public/image_upload/image_user/'+req.params.nameFile, function(err, data) {
	  if (err) throw err; // Fail if the file can't be read.
	  res.end(data);
	});
});

//-------------
// Image-Show Image User(Ava)
router.get('/public/image_upload/image_menu/:nameFile', function(req,res,next){
	console.log(req.params.nameFile);
	fs.readFile('public/image_upload/image_menu/'+req.params.nameFile, function(err, data) {
	  if (err) throw err; // Fail if the file can't be read.
	  res.end(data);
	});
});

// =====================
