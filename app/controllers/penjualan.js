var cors = require('cors');
var express = require('express'),
  app = express();
  router = express.Router(),
  mongoose = require('mongoose'),
  BahanBaku = mongoose.model('BahanBaku'),
  Menu = mongoose.model('Menu');
  Penjualan = mongoose.model('Penjualan');

module.exports = function (app) {
  app.use(cors());
  app.use('/v1', router);
};



// =====================
//-------------
// Penjualan-Add Penjualan 
router.post('/penjualan', function(req, res, next){
	var datenow = new Date();
	var d = datenow.getDate();
		if (d<10){
			d = "0"+d;
		};
	var m = datenow.getMonth()+1;
		if (m<10){
			m = "0"+m;
		};
	var y = datenow.getFullYear();
	var date = y+"-"+m+"-"+d;

	findStokMenu(req.body.id_brand, req.body.store.id_store, req.body.store.penjualan.detail_penjualan.id_menu, function(result1){
		if(result1 < 1 ){
			res.status(403).json({status:'Stok Habis'});
		} else if (result1 < req.body.store.penjualan.detail_penjualan.qty){
			res.status(403).json({status:'Stok Menu tidak cukup'});
		}else {
			findNestedPenjualan(req.body.id_brand, req.body.store.id_store, req.body.store.penjualan.id_penjualan, req.body.store.penjualan.detail_penjualan.id_menu,  function(result){
				if (result[0] == null){ //brand null
					//input brand, store, penjualan, detail_penjualan on Collection Penjualan
					var input = new Penjualan({ id_brand: req.body.id_brand,
												store: [{
													id_store: req.body.store.id_store, 
													penjualan: [{
														id_penjualan: d+""+m+""+y+".00001",
														tanggal: date,
														user:{
															id_user: req.body.store.penjualan.user.id_user, 
															username: req.body.store.penjualan.user.username
														},
														customer: req.body.store.penjualan.customer,
														detail_penjualan:[{
															id_menu: req.body.store.penjualan.detail_penjualan.id_menu, 
															name_menu: req.body.store.penjualan.detail_penjualan.name_menu,
															price: req.body.store.penjualan.detail_penjualan.price,
															qty: req.body.store.penjualan.detail_penjualan.qty,
															eat_in: req.body.store.penjualan.detail_penjualan.eat_in,
															take_away: req.body.store.penjualan.detail_penjualan.take_away,
															sub_total: req.body.store.penjualan.detail_penjualan.qty*req.body.store.penjualan.detail_penjualan.price,
															kategori:{
																id_kategori_menu: req.body.store.penjualan.detail_penjualan.kategori.id_kategori_menu,
																name_kategori_menu: req.body.store.penjualan.detail_penjualan.kategori.name_kategori_menu
															}
														}],
														total_penjualan: req.body.store.penjualan.detail_penjualan.qty*req.body.store.penjualan.detail_penjualan.price
													}]
												}]
											 });
					input.save(function(err){
						if(err){
							console.log(err);
						} else {
							//idBrand, idStore, idMenu, qty, iStoreP, iPenjualanP, iDPP, status, callback
							getDetailMenu(req.body.id_brand, req.body.store.id_store, req.body.store.penjualan.detail_penjualan.id_menu, req.body.store.penjualan.detail_penjualan.qty, null, null, null, 'add', function(result2){
								if (result2 == 0){
									res.json({status:'Not Success'});
								} else {
									res.status(200).json({status:'Success'});
								};
							});
						};
					});
				} else {
					if (result[1] == null){ //store null
						//push store by id_brand, input penjualan, detail_penjualan on Collection Penjualan
						var dataold = {id_brand: req.body.id_brand};
						var datanew = {$push:{	store:{
													id_store: req.body.store.id_store, 
													penjualan: [{
														id_penjualan: d+""+m+""+y+".00001",
														tanggal: date,
														user:{
															id_user: req.body.store.penjualan.user.id_user, 
															username: req.body.store.penjualan.user.username
														},
														customer: req.body.store.penjualan.customer,
														detail_penjualan:[{
															id_menu: req.body.store.penjualan.detail_penjualan.id_menu, 
															name_menu: req.body.store.penjualan.detail_penjualan.name_menu,
															price: req.body.store.penjualan.detail_penjualan.price,
															qty: req.body.store.penjualan.detail_penjualan.qty,
															eat_in: req.body.store.penjualan.detail_penjualan.eat_in,
															take_away: req.body.store.penjualan.detail_penjualan.take_away,
															sub_total: req.body.store.penjualan.detail_penjualan.qty*req.body.store.penjualan.detail_penjualan.price,
															kategori:{
																id_kategori_menu: req.body.store.penjualan.detail_penjualan.kategori.id_kategori_menu,
																name_kategori_menu: req.body.store.penjualan.detail_penjualan.kategori.name_kategori_menu
															}
														}],
														total_penjualan: req.body.store.penjualan.detail_penjualan.qty*req.body.store.penjualan.detail_penjualan.price
													}]
												}}};
						var options = {};
						Penjualan.update(dataold, datanew, options, callback);
						function callback(err){
							if (err){
								console.log(err);
							} else {
								//idBrand, idStore, idMenu, qty, iStoreP, iPenjualanP, iDPP, status, callback
								getDetailMenu(req.body.id_brand, req.body.store.id_store, req.body.store.penjualan.detail_penjualan.id_menu, req.body.store.penjualan.detail_penjualan.qty, null, null, null, 'add', function(result2){
									if (result2 == 0){
										res.json({status:'Not Success'});
									} else {
										res.status(200).json({status:'Success'});
									};
								});
							};
						}
					} else {
						if(result[2] == null){ //penjualan null
							//create idPenjualan
							//push penjualan by id_brand & id_store, input detail_penjualan on Collection Penjualan
							createIdPenjualan(req.body.id_brand, req.body.store.id_store, date, function(hasil){
								var fUse = [];
									fUse[0] = "id_user";
									fUse[1] = "username";
								var vUse = [];
									vUse[0] = req.body.store.penjualan.user.id_user;
									vUse[1] = req.body.store.penjualan.user.username;
								var oUse = {};
									oUse[fUse[0]] = vUse[0];
									oUse[fUse[1]] = vUse[1];
								var fKat = []; 
									fKat[0] = "id_kategori_menu";
									fKat[1] = "name_kategori_menu";
								var vKat = [];
									vKat[0] = req.body.store.penjualan.detail_penjualan.kategori.id_kategori_menu;
									vKat[0] = req.body.store.penjualan.detail_penjualan.kategori.name_kategori_menu;
								var oKat = {};
									oKat[fKat[0]] = vKat[0];
									oKat[fKat[1]] = vKat[1];
								var fDp = [];
									fDp[0] = "id_menu";
									fDp[1] = "name_menu";
									fDp[2] = "price";
									fDp[3] = "qty";
									fDp[4] = "eat_in";
									fDp[5] = "take_away";
									fDp[6] = "sub_total";
									fDp[7] = "kategori";
								var vDp = [];
									vDp[0] = req.body.store.penjualan.detail_penjualan.id_menu;
									vDp[1] = req.body.store.penjualan.detail_penjualan.name_menu;
									vDp[2] = req.body.store.penjualan.detail_penjualan.price;
									vDp[3] = req.body.store.penjualan.detail_penjualan.qty;
									vDp[4] = req.body.store.penjualan.detail_penjualan.eat_in,
									vDp[5] = req.body.store.penjualan.detail_penjualan.take_away,
									vDp[6] = req.body.store.penjualan.detail_penjualan.qty*req.body.store.penjualan.detail_penjualan.price;
									vDp[7] = oKat;
								var oDp = {};
									oDp[fDp[0]] = vDp[0];
									oDp[fDp[1]] = vDp[1];
									oDp[fDp[2]] = vDp[2];
									oDp[fDp[3]] = vDp[3];
									oDp[fDp[4]] = vDp[4];
									oDp[fDp[5]] = vDp[5];
									oDp[fDp[6]] = vDp[6];
									oDp[fDp[7]] = vDp[7];
								var arrDp = [];
									arrDp.push(oDp);
								var fP = [];
									fP[0] = "id_penjualan";
									fP[1] = "tanggal";
									fP[2] = "user";
									fP[3] = "customer";
									fP[4] = "detail_penjualan";
									fP[5] = "total_penjualan";
								var vP = [];
									vP[0] = hasil;
									vP[1] = date;
									vP[2] = oUse;
									vP[3] = req.body.store.penjualan.customer;
									vP[4] = arrDp;
									vP[5] = req.body.store.penjualan.detail_penjualan.qty*req.body.store.penjualan.detail_penjualan.price;
								var oP = {};
									oP[fP[0]] = vP[0];
									oP[fP[1]] = vP[1];
									oP[fP[2]] = vP[2];
									oP[fP[3]] = vP[3];
									oP[fP[4]] = vP[4];
									oP[fP[5]] = vP[5];
								var fObj = "store."+result[1]+".penjualan";
								var vObj = oP;
								var obj = {};
									obj[fObj] = vObj;
								var dataold = {id_brand: req.body.id_brand};
								var datanew = {$push: obj};
								var options = {};
								Penjualan.update(dataold, datanew, options, callback);
								function callback (err){
									if (err){
										console.log(err);
									} else {
										//idBrand, idStore,  idMenu, qty, iStoreP, iPenjualanP, iDPP, status, callback
										getDetailMenu(req.body.id_brand, req.body.store.id_store,  req.body.store.penjualan.detail_penjualan.id_menu, req.body.store.penjualan.detail_penjualan.qty, null, null, null, 'add', function(result2){
											if (result2 == 0){
												res.json({status:'Not Success'});
											} else {
												res.status(200).json({status:'Success'});
											};
										});
									};
								};
							});
						} else { //result[3] utk detail_penjualan saat cancelPenjualan, result[4] utk totalPenjualan saat checkout 
							if (result[5] == null){ //detail_penjualan null
								//push detail_penjualan by id_brand & id_store & id_penjualan on Collection Penjualan
								var fKat = []; 
									fKat[0] = "id_kategori_menu";
									fKat[1] = "name_kategori_menu";
								var vKat = [];
									vKat[0] = req.body.store.penjualan.detail_penjualan.kategori.id_kategori_menu;
									vKat[0] = req.body.store.penjualan.detail_penjualan.kategori.name_kategori_menu;
								var oKat = {};
									oKat[fKat[0]] = vKat[0];
									oKat[fKat[1]] = vKat[1];
								var fDp = [];
									fDp[0] = "id_menu";
									fDp[1] = "name_menu";
									fDp[2] = "price";
									fDp[3] = "qty";
									fDp[4] = "eat_in";
									fDp[5] = "take_away";
									fDp[6] = "sub_total";
									fDp[7] = "kategori";
								var vDp = [];
									vDp[0] = req.body.store.penjualan.detail_penjualan.id_menu;
									vDp[1] = req.body.store.penjualan.detail_penjualan.name_menu;
									vDp[2] = req.body.store.penjualan.detail_penjualan.price;
									vDp[3] = req.body.store.penjualan.detail_penjualan.qty;
									vDp[4] = req.body.store.penjualan.detail_penjualan.eat_in,
									vDp[5] = req.body.store.penjualan.detail_penjualan.take_away,
									vDp[6] = req.body.store.penjualan.detail_penjualan.qty*req.body.store.penjualan.detail_penjualan.price;
									vDp[7] = oKat;
								var oDp = {};
									oDp[fDp[0]] = vDp[0];
									oDp[fDp[1]] = vDp[1];
									oDp[fDp[2]] = vDp[2];
									oDp[fDp[3]] = vDp[3];
									oDp[fDp[4]] = vDp[4];
									oDp[fDp[5]] = vDp[5];
									oDp[fDp[6]] = vDp[6];
									oDp[fDp[7]] = vDp[7];
								var fObj = "store."+result[1]+".penjualan."+result[2]+".detail_penjualan";
								var vObj = oDp;
								var obj = {};
									obj[fObj] = vObj;
								var dataold = {id_brand: req.body.id_brand};
								var datanew = {$push: obj};
								var options = {};
								Penjualan.update(dataold, datanew, options, callback);
								function callback (err){
									if (err){
										console.log(err);
									} else {
										//idBrand, idStore,  idMenu, qty, iStoreP, iPenjualanP, iDPP, status, callback
										getDetailMenu(req.body.id_brand, req.body.store.id_store, req.body.store.penjualan.detail_penjualan.id_menu, req.body.store.penjualan.detail_penjualan.qty, null, null, null, 'add', function(result2){
											if (result2 == 0){
												res.json({status:'Not Success'});
											} else {
												sumTotalPenjualan(req.body.id_brand, req.body.store.id_store, req.body.store.penjualan.id_penjualan, result[1], result[2], function(hasil){
													res.status(200).json({status:'Success'});
												});
											};
										});
									};
								};
							} else {
								//update detail_penjualan on Collection Penjualan
								var f = [];
								    f[0] = "store."+result[1]+".penjualan."+result[2]+".detail_penjualan."+result[5]+".qty";
								    f[1] = "store."+result[1]+".penjualan."+result[2]+".detail_penjualan."+result[5]+".sub_total";
								    f[2] = "store."+result[1]+".penjualan."+result[2]+".detail_penjualan."+result[5]+".eat_in";
								    f[3] = "store."+result[1]+".penjualan."+result[2]+".detail_penjualan."+result[5]+".take_away";
								var v = [];
								    v[0] = req.body.store.penjualan.detail_penjualan.qty+result[6];
								    v[1] = (req.body.store.penjualan.detail_penjualan.qty+result[6])*result[7];
								    v[2] = req.body.store.penjualan.detail_penjualan.eat_in+result[9];
								    v[3] = req.body.store.penjualan.detail_penjualan.take_away+result[10];
								var obj = {};
								    obj[f[0]] = v[0];
								    obj[f[1]] = v[1];
								    obj[f[2]] = v[2];
								    obj[f[3]] = v[3];
								var dataold = {id_brand: req.body.id_brand};
								var datanew = obj;
								var options = {};
								Penjualan.update(dataold, datanew, options, callback);
								function callback(err){
									if (err){
										console.log(err);
									} else {
										//idBrand, idStore,  idMenu, qty, iStoreP, iPenjualanP, iDPP, status, callback
										getDetailMenu(req.body.id_brand, req.body.store.id_store, req.body.store.penjualan.detail_penjualan.id_menu, req.body.store.penjualan.detail_penjualan.qty, null, null, null, 'add', function(result2){
											if (result2 == 0){
												res.json({status:'Not Success'});
											} else {
												sumTotalPenjualan(req.body.id_brand, req.body.store.id_store, req.body.store.penjualan.id_penjualan, result[1], result[2], function(hasil){
													res.status(200).json({status:'Success'});
												});
											};
										});
									};
								};
							};
						};
					};
				};
			});
		};
	});
});

//-------------
// Penjualan-Update Note 
router.put('/penjualan/note', function(req, res, next){
	findNestedPenjualan(req.body.id_brand, req.body.store.id_store, req.body.store.penjualan.id_penjualan, req.body.store.penjualan.detail_penjualan.id_menu,  function(result){
		if(result[0] == null){ //brand null
			res.status(403).json({status:'Brand Not Found'});
		} else if (result[1] == null){ //store null
			res.status(403).json({status:'Store Not Found'});
		} else if (result[2] == null){ //penjualan null
			res.status(403).json({status:'Penjualan Not Found'});
		} else if (result[5] == null){ //detail_penjualan null
			res.status(403).json({status:'Detail Penjualan Not Found'});
		} else {
			var f = "store."+result[1]+".penjualan."+result[2]+".detail_penjualan."+result[5]+".note";
			var v = req.body.store.penjualan.detail_penjualan.note;
			var obj = {};
			    obj[f] = v;
			var dataold = {id_brand: req.body.id_brand};
			var datanew = obj;
			var options = {};
			Penjualan.update(dataold, datanew, options, callback);
			function callback(err){
				if (err){
					console.log(err);
				} else {
					res.status(200).json({status:'Success'});
				};
			};
		};
	});
});

//-------------
// Penjualan-Cancel Penjualan 
router.put('/penjualan/cancel', function(req, res, next){
	findNestedPenjualan(req.body.id_brand, req.body.id_store, req.body.id_penjualan, "",  function(result){
		if (result[0] == null){ //brand null
			res.status(404).json({status:'Brand Not Found'});
		} else {
			if (result[1] == null){ //store null
				res.status(404).json({status:'Store Not Found'});
			} else {
				if (result[2] == null){ //penjualan null
					res.status(404).json({status:'Penjualan Not Found'});
				} else {
					var arrDetailPenjualan = [];
					var count = 0;
					for (var i=0; i<result[3].length; i++){
						if (result[3][i].cancel == false){ //cari yg detail_penjualanannya cancel false, tampung dlm array
							var field = [];
								field[0] = "dp";
								field[1] = "index";
							var value = [];
								value[0] = result[3][i];
								value[1] = i;
							var obj = {};
								obj[field[0]] = value[0];
								obj[field[1]] = value[1];
							arrDetailPenjualan.push(obj);
							if (i == result[3].length-1){	
								var loopLength = arrDetailPenjualan.length;
								var loopIndex = 0;
								loopOne();

								function loopOne() {
									loopTwo();
								};

								function loopTwo (){
									if (loopIndex == loopLength){
										var f = [];
											f[0] = 'store.'+result[1]+'.penjualan.'+result[2]+'.cancel';
											f[1] = 'store.'+result[1]+'.penjualan.'+result[2]+'.total_penjualan';
										var v = [];
											v[0] = true;
											v[1] = 0;
										var obj = {};
											obj[f[0]] = v[0];
											obj[f[1]] = v[1];
										var dataold = {id_brand: req.body.id_brand};
										var datanew = obj;
										var options = {};
										Penjualan.update(dataold, datanew, options, callback);
										function callback(err){
											if (err){
												console.log(err);
											} else {
												getBahanBakuNew(req.body.id_brand, req.body.id_store, function(hasil){
													res.status(200).json({status:'Success'});
												});
											};
										};
										return false;
									}
									setTimeout(function(){
										console.log(loopIndex);
										getDetailMenu(req.body.id_brand, req.body.id_store, arrDetailPenjualan[loopIndex].dp.id_menu, arrDetailPenjualan[loopIndex].dp.qty, result[1], result[2], arrDetailPenjualan[loopIndex].index, 'cancel', function(hasil){

										})
										loopIndex++;
										loopOne();
									}, 100);
								};
							};
						} else if (i == result[3].length-1){
							var loopLength = arrDetailPenjualan.length;
							var loopIndex = 0;
							loopOne();

							function loopOne() {
								loopTwo();
							};

							function loopTwo (){
								if (loopIndex == loopLength){
									var f = [];
										f[0] = 'store.'+result[1]+'.penjualan.'+result[2]+'.cancel';
										f[1] = 'store.'+result[1]+'.penjualan.'+result[2]+'.total_penjualan';
									var v = [];
										v[0] = true;
										v[1] = 0;
									var obj = {};
										obj[f[0]] = v[0];
										obj[f[1]] = v[1];
									var dataold = {id_brand: req.body.id_brand};
									var datanew = obj;
									var options = {};
									Penjualan.update(dataold, datanew, options, callback);
									function callback(err){
										if (err){
											console.log(err);
										} else {
											getBahanBakuNew(req.body.id_brand, req.body.id_store, function(hasil){
												res.status(200).json({status:'Success'});
											});
										};
									};
									return false;
								}
								setTimeout(function(){
									console.log(loopIndex);
									getDetailMenu(req.body.id_brand, req.body.id_store, arrDetailPenjualan[loopIndex].dp.id_menu, arrDetailPenjualan[loopIndex].dp.qty, result[1], result[2], arrDetailPenjualan[loopIndex].index, 'cancel', function(hasil){

									})
									loopIndex++;
									loopOne();
								}, 100);
							};
						};
					};
				};
			};
		};
	});
});

//-------------
// Penjualan-Cancel Detail Penjualan 
router.put('/penjualan/detail/cancel', function(req, res, next){
	findNestedPenjualan(req.body.id_brand, req.body.id_store, req.body.id_penjualan, req.body.id_menu, function(result){
		if (result[0] == null){ //brand null
			res.status(404).json({status:'Brand Not Found'});
		} else {
			if (result[1] == null){ //store null
				res.status(404).json({status:'Store Not Found'});
			} else {
				if (result[2] == null){ //penjualan null
					res.status(404).json({status:'Penjualan Not Found'});
				} else {
					if (result[4] == null){ //detail_penjualan null
						res.status(404).json({status:'Detail Penjualan Not Found'});
					} else { //result[3] utk detail_penjualan saat cancelPenjualan 
						if (result[8] == false){
							getDetailMenu(req.body.id_brand, req.body.id_store, req.body.id_menu, result[6], result[1], result[2], result[5], 'cancel', function(result2){
								if(result2 == 1){
									sumTotalPenjualan(req.body.id_brand, req.body.id_store, req.body.id_penjualan, result[1], result[2], function(hasil){
										res.status(200).json({status:'Success'});
									});
								};
							});
						} else {
							res.status(200).json({status:'Success'});
						}; 
					};
				};
			};
		};
	});
});

//-------------
// Penjualan-CloseBill Penjualan 
router.put('/penjualan/checkout', function(req, res, next){
	findNestedPenjualan(req.body.id_brand, req.body.store.id_store, req.body.store.penjualan.id_penjualan, "", function(result){
		if (result[0] == null){ //brand null
			res.status(404).json({status:'Brand Not Found'});
		} else {
			if (result[1] == null){ //store null
				res.status(404).json({status:'Store Not Found'});
			} else {
				if (result[2] == null){ //penjualan null
					res.status(404).json({status:'Penjualan Not Found'});
				} else {
					if (req.body.store.penjualan.bayar >= result[4].total_penjualan && result[4].checkout == false){
						var fP = [];
							fP[0] = 'store.'+result[1]+'.penjualan.'+result[2]+'.bayar';
							fP[1] = 'store.'+result[1]+'.penjualan.'+result[2]+'.kembalian';
							fP[2] = 'store.'+result[1]+'.penjualan.'+result[2]+'.checkout';
						var vP = [];
							vP[0] = req.body.store.penjualan.bayar;
							vP[1] = req.body.store.penjualan.bayar - result[4].total_penjualan;
							vP[2] = true;
						var oP = {};
							oP[fP[0]] = vP[0];
							oP[fP[1]] = vP[1];
							oP[fP[2]] = vP[2];
						var dataold = {id_brand: req.body.id_brand};
						var datanew = oP;
						var options = {};
						Penjualan.update(dataold, datanew, options, callback);
						function callback(err){
							if (err){
								console.log(err);
							} else {
								res.status(200).json({status:'Success'});
							};
						};
					} else if (result[4].checkout == true){
						res.status(404).json({status:'sudah melakukan checkout'});
					} else {
						res.status(403).json({status:'uang Kurang'});
					};
				};
			};
		};
	});
});

//-------------
// Penjualan-Show Penjualan 
router.get('/penjualan/:idBrand/:idStore/:idPembelian', function(req, res, next){
	var data = Penjualan.aggregate ([ {$match:{id_brand: req.params.idBrand}},
									  {$unwind: "$store"},
									  {$match:{"store.id_store": req.params.idStore}},
									  {$unwind: "$store.penjualan"},
									  {$match:{"store.penjualan.id_penjualan": req.params.idPembelian}},
									  {$unwind: "$store.penjualan.detail_penjualan"},
									  {$match:{"store.penjualan.detail_penjualan.cancel": false}},
									  {$project:{penjualan: "$store.penjualan"}}
									]);
	data.exec(function(err, result){
		if(err){
			console.log(err);
		} else {
			if (result == ""){
				res.status(404).json({status:'Not Found'});
			} else {
				res.status(200).json(result);
			};
		};
	});
});
// =====================

router.post('/cektest', function(req, res, next){
	var length = 5;
	var index = 0;
	for1();

	function for1() {
		for2();
	}

	function for2 (){
		if (index == length){
			res.status(200).json({status:'success'});
			return false;
		}
		setTimeout(function(){
			console.log(index);
			index++;
			for1();
			
		}, 1500);
	}
});



function createIdPenjualan(idBrand, idStore, tanggal, callback){
	//format idPenjualan : tanggal(ddMMYYYY).XXXXX 
	var idPenjualan;
	var date = new Date();
	var d = date.getDate();
		if (d<10){
			d = "0"+d;
		};
	var m = date.getMonth()+1;
		if (m<10){
			m = "0"+m;
		};
	var y = date.getFullYear();
	var data = Penjualan.aggregate 	([ 	{$match:{id_brand: idBrand}},
										{$unwind: "$store"},
										{$match: {"store.id_store":idStore}},
										{$unwind: "$store.penjualan"},
										{$match:{"store.penjualan.tanggal": new Date(tanggal+" 00:00:00.000Z")}}
									]);
	data.exec(function(err, result){
		if(result == ""){
			idPenjualan = d+""+m+""+y+".00001";
			callback(idPenjualan);
		} else {
			var count = result.length;
			if (count<9){
				idPenjualan = d+""+m+""+y+".0000"+(count+1);
			} else if (count<99){
				idPenjualan = d+""+m+""+y+".000"+(count+1);
			} else if (count<999){
				idPenjualan = d+""+m+""+y+".00"+(count+1);
			} else if (count<9999){
				idPenjualan = d+""+m+""+y+".0"+(count+1);
			} else if (count<99999){
				idPenjualan = d+""+m+""+y+"."+(count+1);
			};
			callback(idPenjualan);
		};
	});
};

function findStokMenu(idBrand, idStore, idMenu, callback){
	var data = 	Menu.findOne({id_brand:idBrand});
	data.exec(function(err, result){
		if(err){
			console.log(err);
		} else {
			if (result == null){
				callback(0);
			} else {
				lS = result.store.length;
				var aS = false
				for(var i = 0; i<lS; i++){
					if (result.store[i].id_store == idStore){
						aS = true;
						var lM = result.store[i].menu.length;
						var aM = false;
						for (var j=0; j<lM; j++){
							if((result.store[i].menu[j].id_menu == idMenu) && (result.store[i].menu[j].deleted == false)){
								aM = true;
								callback(result.store[i].menu[j].stok);
								break;
							};
							if (j == lM-1 && aM == false){
								callback(0);
							};
						};
					};
					if (i == lS-1 && aS == false){
						console.log('store not found');
						callback(0);
					};
				};
			};
		};
	})
};

function findNestedPenjualan(idBrand, idStore, idPenjualan, idMenu, callback){
	var hasil = [];
	var data = Penjualan.findOne({id_brand: idBrand});
	data.exec(function(err, result){
		if (err){
			console.log(err);
		} else {
			if (result == null){
				hasil.push(null);
				callback(hasil); // 0, brand null 
			} else {
				hasil.push('not null'); // 0, brand not null 
				var lS = result.store.length;
				var aS = false;
				for (var i =0; i<lS; i++){
					if (result.store[i].id_store == idStore){
						hasil.push(i); // 1, store not null
						aS = true;
						var lP = result.store[i].penjualan.length;
						var aP = false; 
						for (var j = 0; j<lP; j++){
							if (result.store[i].penjualan[j].id_penjualan == idPenjualan && result.store[i].penjualan[j].cancel == false){
								hasil.push(j); // 2, penjualan not null
								hasil.push(result.store[i].penjualan[j].detail_penjualan); //3 detailPenjualan
								hasil.push(result.store[i].penjualan[j]); //4 data penjualan
								aP = true;
								var lM = result.store[i].penjualan[j].detail_penjualan.length;
								var aM = false;
								for (var k=0; k<lM; k++){
									if (result.store[i].penjualan[j].detail_penjualan[k].id_menu == idMenu){
										hasil.push(k); // 5, detail_penjualan(menu) not null
										hasil.push(result.store[i].penjualan[j].detail_penjualan[k].qty) //6, detail_penjualan(qty)
										hasil.push(result.store[i].penjualan[j].detail_penjualan[k].price) //7, detail_penjualan(price)
										hasil.push(result.store[i].penjualan[j].detail_penjualan[k].cancel) //8, detail_penjualan(cancel)
										hasil.push(result.store[i].penjualan[j].detail_penjualan[k].eat_in) //9, detail_penjualan(eat_in)
										hasil.push(result.store[i].penjualan[j].detail_penjualan[k].take_away) //10, detail_penjualan(take_away)
										aM = true;
										break;
									};
									if (k == lM-1 && aM == false){
										hasil.push(null); // 5, detail_penjualan(menu) null
									};
								};							
							};
							if (j == lP-1 && aP == false){
								hasil.push(null); // 2, penjualan null
							};
						};
					};
					if (i == lS-1 && aS == false){
						hasil.push(null); // 1, store null
					};
				};
				callback(hasil);
			};
		};
	});
};	

function getDetailMenu(idBrand, idStore, idMenu, qty, iStoreP, iPenjualanP, iDPP, status, callback){
	//detailMenu yng disajikan utk function lainnya yaitu detail Menu yg sudah di filter status delete nya
	var data = Menu.findOne({id_brand: idBrand});
	data.exec(function(err, result){
		if (err){
			console.log(err);
		} else {
			if (result == null){
				callback(0);
			} else {
				var lS = result.store.length;
				var aS = false;
				for (var i=0; i<lS; i++){
					if(result.store[i].id_store ==  idStore){
						aS = true;
						var lM = result.store[i].menu.length;
						var aM = false;
						for(var j=0; j<lM; j++){
							if(result.store[i].menu[j].id_menu == idMenu){
								aM = true;
								var arrDetailMenu = [];
								for (var k =0; k<result.store[i].menu[j].detail_menu.length; k++){
									if (result.store[i].menu[j].detail_menu[k].deleted == false){
										var field = [];
											field[0] = "id_bahan_baku";
											field[1] = "deleted";
											field[2] = "jumlah";
										var value = [];
											value[0] = result.store[i].menu[j].detail_menu[k].id_bahan_baku;
											value[1] = result.store[i].menu[j].detail_menu[k].deleted;
											value[2] = result.store[i].menu[j].detail_menu[k].jumlah;
										var obj = {};
											obj[field[0]] = value[0];
											obj[field[1]] = value[1];
											obj[field[2]] = value[2];
										arrDetailMenu.push(obj);
										if (k == result.store[i].menu[j].detail_menu.length-1){
											getBahanBaku(idBrand, idStore, idMenu, arrDetailMenu, qty, iStoreP, iPenjualanP, iDPP, status, function(hasil){
												callback(hasil);
											});
										};
									} else {
										if (k == result.store[i].menu[j].detail_menu.length-1){
											getBahanBaku(idBrand, idStore, idMenu, arrDetailMenu, qty, iStoreP, iPenjualanP, iDPP, status, function(hasil){
												callback(hasil);
											});
										};
									};
								};
								break;
							};
							if (j == lM-1 && aM == false){
								callback(0);
							};
						};
					};
					if (i == lS-1 && aS == false){
						callback(0);
					};
				};
			};
		};
	});
};

function getBahanBaku(idBrand, idStore, idMenu, detailMenu, qty, iStoreP, iPenjualanP, iDPP, status, callback){	
	console.log('bahan baku');
	var data = BahanBaku.findOne({id_brand: idBrand});
	data.exec(function(err, result){
		if(err){
			console.log(err);
		} else {
			if (result == null){
				callback(0);
			} else {
				var lS = result.store.length;
				var aS = false;
				for (var i=0; i<lS; i++){
					if (result.store[i].id_store == idStore){ //store not null
						aS = true;
						updateStokBahanBaku(idBrand, idStore, idMenu, result.store[i].bahan_baku, detailMenu, i, qty, iStoreP, iPenjualanP, iDPP, status, function(hasil){
							callback(hasil);
						});
						break;
					};
					if (i == lS-1 && aS == false){ //store null
						callback(0);
					};
				};
			};
		};
	});
};

function updateStokBahanBaku(idBrand, idStore, idMenu, bahanBaku, detailMenu, iStoreBB, qty, iStoreP, iPenjualanP, iDPP, status, callback){
	if (status == "add"){
		if (detailMenu.length == 0){ //detail menu kosong
			callback(1);
		} else {
			var count = 0;
			for (var i=0; i<detailMenu.length; i++){
				for (var j=0; j<bahanBaku.length; j++){
					if (detailMenu[i].id_bahan_baku == bahanBaku[j].id_bahan_baku && bahanBaku[j].deleted == false && detailMenu[i].deleted == false){
						var f = "store."+iStoreBB+".bahan_baku."+j+".stok"; 
						var v = bahanBaku[j].stok - (qty*detailMenu[i].jumlah);
						var obj ={};
							obj[f] = v;
						var dataold = {id_brand: idBrand};
						var datanew = obj;
						var options = {};
						var fBB = [];
							fBB[0] = "id_bahan_baku";
							fBB[1] = "stok";
						var vBB = [];
							vBB[0] = bahanBaku[j].id_bahan_baku;
							vBB[1] = bahanBaku[j].stok - (qty*detailMenu[i].jumlah);
						var objBB = {};
							objBB[fBB[0]] = vBB[0];
							objBB[fBB[1]] = vBB[1];
						BahanBaku.update(dataold, datanew, options, hasilResult)
						function hasilResult(err, result){
							if (err){
								console.log(err);
							} else {
								count+=1;
								if ( count == detailMenu.length-1){ //jk stok sdh diupdate semua, mk lanjut ke function selanjutnya
									getBahanBakuNew(idBrand, idStore, function(hasil){
										callback(hasil);
									});
								};
							};
						};
						break;
					} else if ((count == 0) && (i == detailMenu.length-1) && (j == bahanBaku.length-1)){ //bahanBaku tdk ada yg diupdate/detail_menu hanya first yg status deleted false
						callback(1);
					};
				};
			};
		};
	} else if (status == 'cancel') {
		if (detailMenu.length == 0){ //detail menu kosong
			callback(1);
		} else {
			var count = 0;
			for (var i=0; i<detailMenu.length; i++){
				for (var j=0; j<bahanBaku.length; j++){
					if (detailMenu[i].id_bahan_baku == bahanBaku[j].id_bahan_baku && bahanBaku[j].deleted == false && detailMenu[i].deleted == false){
						var f = "store."+iStoreBB+".bahan_baku."+j+".stok"; 
						var v = bahanBaku[j].stok + (qty*detailMenu[i].jumlah);
						var obj ={};
							obj[f] = v;
						var dataold = {id_brand: idBrand};
						var datanew = obj;
						var options = {};
						var fBB = [];
							fBB[0] = "id_bahan_baku";
							fBB[1] = "stok";
						var vBB = [];
							vBB[0] = bahanBaku[j].id_bahan_baku;
							vBB[1] = bahanBaku[j].stok + (qty*detailMenu[i].jumlah);
						console.log(bahanBaku[j].name_bahan_baku);
						var objBB = {};
							objBB[fBB[0]] = vBB[0];
							objBB[fBB[1]] = vBB[1];
						BahanBaku.update(dataold, datanew, options, hasilResult);
						function hasilResult(err){
							if (err){
								console.log(err);
							} else {
								count+=1;
								if ( count == detailMenu.length-1){ //jk stok sdh diupdate semua, mk lanjut ke function selanjutnya
									updateDetailPenjualan(idBrand, idStore, iStoreP, iPenjualanP, iDPP, function(hasil){
										callback(hasil);
									});
								};
							};
						};
						break;
					} else if ((count == 0) && (i == detailMenu.length-1) && (j == bahanBaku.length-1)){ //bahanBaku tdk ada yg diupdate/detail_menu hanya first yg status deleted false
						callback(1);
					};
				};
			};
		};
	};
};

function updateDetailPenjualan(idBrand, idStore,  iStoreP, iPenjualanP, iDPP, callback){
	var f = "store."+iStoreP+".penjualan."+iPenjualanP+".detail_penjualan."+iDPP+".cancel";
	var v = true;
	var obj = {};
		obj[f] = v;
	var dataold = {id_brand: idBrand};
	var datanew = obj;
	var options = {};
	Penjualan.update(dataold, datanew, options, hasil);
	function hasil (err){
		if (err){
			console.log(err);
		} else {
			getBahanBakuNew(idBrand, idStore, function(hasil){
				callback(hasil);
			});
		};
	};
};

function getBahanBakuNew (idBrand, idStore, callback){
	var data = BahanBaku.findOne({id_brand: idBrand});
	data.exec(function(err, result){
		if(err){
			console.log(err);
		} else {
			if (result == null){
				callback(0);
			} else {
				var lS = result.store.length;
				var aS = false;
				for (var i=0; i<lS; i++){
					if (result.store[i].id_store == idStore){ //store not null
						aS = true;
						updateAllStokMenu(idBrand, idStore, result.store[i].bahan_baku, function(hasil){
							callback(hasil);
						});
						break;
					};
					if (i == lS-1 && aS == false){ //store null
						callback(1);
					};
				};
			};
		};
	});
};

function updateAllStokMenu(idBrand, idStore, bahanBaku, callback){
	var data  = Menu.findOne({id_brand:idBrand});
	data.exec(function(err, result){
		if(err){
			console.log(err);
		} else {
			if(result == null){
				callback(1);
			} else {
				var lS = result.store.length;
				var aS = false;
				for (var i=0; i<lS; i++){
					if (result.store[i].id_store == idStore){ //store on collection menu
						aS = true;
						var lM = result.store[i].menu.length;
						for (var j=0; j<lM; j++){ //looping menu
							if (result.store[i].menu[j].deleted == false){ //menu yg blm dihapus
								var detailMenu = result.store[i].menu[j].detail_menu;
								var a = [];
								var min;
								if (detailMenu.length == 0){
									var field = "store."+i+".menu."+j+".stok";
									var value = 0;
									var obj ={};
										obj[field] = value;
									var dataold = {id_brand: idBrand};
									var datanew = obj;
									var options = {};
									Menu.update(dataold, datanew, options, success);
									function success (err){
										if (err){
											console.log(err);
										};
									};
								} else {
									for (var l=0; l<detailMenu.length; l++){ // looping detailMenu
										for (var m=0; m<bahanBaku.length; m++){ // looping bahanBaku
											if (detailMenu[l].id_bahan_baku == bahanBaku[m].id_bahan_baku && bahanBaku[m].deleted == false && detailMenu[l].deleted == false){ //jk id_bahan_baku collection detail_menu = id_bahan_baku collection bahan baku dan keduanya blm dihapus 
												var  b = bahanBaku[m].stok/detailMenu[l].jumlah;
												a.push(Math.floor(b)); // push hasil bagi ke var a
											};
										};
										if (l == detailMenu.length-1){ // cari nilai terkecil dari var array a
											if (a.length == 0){
												var field = "store."+i+".menu."+j+".stok";
												var value = 0;
												var obj ={};
													obj[field] = value;
												var dataold = {id_brand: idBrand};
												var datanew = obj;
												var options = {};
												Menu.update(dataold, datanew, options, success);
												function success (err){
													if (err){
														console.log(err);
													};
												};
											} else {
												if (a.length == 1){
													min = a[0];
												} else if (a.length == 2){
													if (a[0] <= a[1]){
														min = a[0]
													} else {
														min = a[1];
													}
												} else {
													for (var k=0; k<a.length; k++){
														if (k == 0){
															min = a[k];
														} else {
															if (a[k]<= min){
																min = a[k]
															};
														};
													};
												};
												var field = "store."+i+".menu."+j+".stok";
												var value = min;
												var obj ={};
													obj[field] = value;
												var dataold = {id_brand: idBrand};
												var datanew = obj;
												var options = {};
												Menu.update(dataold, datanew, options, success);
												function success (err){
													if (err){
														console.log(err);
													};
												};
											};
										};
									};
								};
							};
						};
						callback(1);
						break;
					};
					if (i == lS-1 && aS == false){
						callback(1);
						break;
					};
				};
			};
		};
	});
	//find Menu by deleted false
		//find Bahan by deleted false
	//update Stok
};

function sumTotalPenjualan(idBrand, idStore, idPenjualan, iStoreP, iPenjualanP, callback){
	var f = "store."+iStoreP+".penjualan."+iPenjualanP+".total_penjualan";
	var data = Penjualan.aggregate 	([ 	{$match:{id_brand: idBrand}},
										{$unwind: "$store"},
										{$match: {"store.id_store":idStore}},
										{$unwind: "$store.penjualan"},
										{$match: {"store.penjualan.id_penjualan": idPenjualan}},
										{$unwind: "$store.penjualan.detail_penjualan"},
										{$match: {"store.penjualan.detail_penjualan.cancel": false}},
										{$group:{ _id: null, "total_penjualan": {$sum:"$store.penjualan.detail_penjualan.sub_total" }}}
									]);
	data.exec(function(err, result){
		if (err){
			console.log(err);
		} else {
			//update tdk dijadikan 1, krn agar jalannya eksekusi program terstruktur.
			if (result == ""){
				var v = 0;
				var obj = {};
			    	obj[f] = v;
				var dataold = {id_brand: idBrand};
				var datanew = obj;
				var options = {};
				Penjualan.update(dataold, datanew, options, hasil);
				function hasil(err){
					if (err){
						console.log(err);
					} else {
						callback(1);
					};
				};
			} else {
				var v  = result[0].total_penjualan;
				var obj = {};
				    obj[f] = v;
				var dataold = {id_brand: idBrand};
				var datanew = obj;
				var options = {};
				Penjualan.update(dataold, datanew, options, hasil);
				function hasil(err){
					if (err){
						console.log(err);
					} else {
						callback(1);
					};
				};
			};
		};
	});
};