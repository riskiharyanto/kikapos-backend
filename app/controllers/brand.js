var cors = require('cors');
var ls = require('local-storage');
var express = require('express'),
  app = express();
  router = express.Router(),
  mongoose = require('mongoose'),
  Brand = mongoose.model('Brand'),
  Registration = mongoose.model('Registration'),
  User = mongoose.model('User'),
  SatuanBahan = mongoose.model('SatuanBahan'),
  KategoriMenu = mongoose.model('KategoriMenu');

module.exports = function (app) {
  app.use(cors());
  app.use('/v1', router);
};

// =====================
//-------------
// Brand-Register
router.post('/brand', function(req, res, next){
  var data = Registration.findOne({$and:[{status:false},{email:req.body.user.info_user.email}]});
  data.exec(function(err, result){
    if(err){
      console.log(err);
    } else {
      if(result == null){
        res.status(404).json({status:'Registration Not Found'});
      } else {
        var data = Brand.find({});
        data.exec(function(err, result2){
          if(err){
            console.log(err);
          } else {
            if (result2 == null){
              res.status(404).json({status:'Not Found'});
            } else {
              var check = false;
              for (var i=0; i<result2.length; i++){
                var brandResult = result2[i].name_brand.toLowerCase();
                var brandInput = req.body.name_brand.toLowerCase();
                if (brandResult == brandInput ){
                  check = true;
                  res.status(409).json({status:'Conflict Name/Id Brand'});
                  break;
                };
                if (i == result2.length-1 && check == false){
                  createGenerateIdBrand(function(idBrand){
                    if(idBrand != 'maxBrand'){
                      var input = new Brand ({  id_brand : idBrand,
                                        name_brand : req.body.name_brand,
                                        address_brand : req.body.address_brand,
                                        contact : req.body.contact,
                                        store : [{
                                          id_store: 'store.XXXX',
                                          name_store: 'Head Store',
                                          address_store: req.body.address_brand,
                                          contact_store:{ email_store: req.body.contact.email,
                                                          phone_store: req.body.contact.phone }
                                        }],
                                        social_media: req.body.social_media
                                    });
                      input.save(function(err){
                        if(err){
                          console.log(err);
                        } else {
                          var input = new User ({ id_brand: idBrand,
                                                  store:{
                                                    id_store: 'store.XXXX',
                                                    name_brand : req.body.name_brand,
                                                    name_store: 'Head Store',
                                                    user:{
                                                      info_user:{
                                                        id_user:'SU.01',
                                                        email: result.email,
                                                        username: req.body.user.info_user.username,
                                                        password: result.password,
                                                        phone: req.body.user.info_user.phone,
                                                        role:{
                                                          id_role: 1,
                                                          name_role: ''
                                                        }
                                                      }
                                                    }},
                                                  status: false //post pertama false, klo dah bayar baru true
                                              });
                          input.save(function(err){
                            if(err){
                              console.log(err);
                            } else {
                              var dataold = {email:req.body.user.info_user.email};
                              var datanew = {status: true};
                              var options = {};
                              Registration.update(dataold, datanew, options, callback);
                              function callback (err){
                                if(err){
                                  console.log(err);
                                } else {
                                  inputSatuan(idBrand, function(outSatuan){
                                    if (outSatuan == 'success'){
                                      inputKategoriMenu(idBrand, function(outKategori){
                                        if(outKategori == 'success'){
                                          res.status(200).json({
                                            status:'Input Brand Success', 
                                            idBrand: idBrand
                                          });
                                        } else {
                                          res.status(400).json({status:'Input Kategori Not Success'});
                                        };
                                      });
                                    } else {
                                      res.status(400).json({status:'Input Satuan Not Success'});
                                    };
                                  });
                                };
                              };
                            };  
                          });
                        };
                      });
                    } else {
                      res.status(403).json({status:'Brand Max'});
                    };
                  });
                };
              };
            };
          };
        });
      };
    };
  });
});

//-------------
// Brand-Add Store
router.put('/addStore', function(req, res, next){
  var data = Brand.findOne({id_brand:req.body.id_brand});
  data.exec(function(err, result){
    if (err){
      console.log(err);
    } else {
      if (result == null){
        res.status(404).json({status:'Not Found'});
      } else {
        var nameStore = result.store;
        var check = false;
        for (var i=0; i<nameStore.length; i++){
          var storeResult = nameStore[i].name_store.toLowerCase();
          var storeInput = req.body.name_store.toLowerCase();
          if (storeResult == storeInput && nameStore[i].deleted == false){
            check = true;
            res.status(409).json({status: 'Conflict Name Store'});
            break;
          };
          if (i == nameStore.length-1 && check == false){
            createGenerateIdStore(req.body.id_brand, function(idStore){
              if (idStore == 0){
                res.status(404).json({status:'Brand Not Found'});
              } else if (idStore == 'maxStore'){
                res.status(403).json({status:'Max Store'});
              } else {
                var dataold = {id_brand: req.body.id_brand};
                var datanew = {$push: {store:{  id_store: idStore,
                                                name_store: req.body.name_store,
                                                address_store: {  detail: req.body.address_store.detail,
                                                                  city_id: req.body.address_store.city_id,
                                                                  city_name: req.body.address_store.city_name,
                                                                  province_id: req.body.address_store.province_id,
                                                                  province_name: req.body.address_store.province_name
                                                                },
                                                contact_store: {  email_store: req.body.contact_store.email_store,
                                                                  phone_store: req.body.contact_store.phone_store 
                                                                },
                                                social_media_store: { ig: req.body.social_media_store.ig,
                                                                      fb: req.body.social_media_store.fb
                                                                    }}}};
                var options = {};
                Brand.update(dataold, datanew, options, callback);
                function callback(err, numAffected){
                  if (err){
                    console.log(err);
                  } else {
                    res.status(200).json({  id_store: idStore,
                                            status: 'Add Store Success'});
                  };
                };
              };
            });
          };
        };
      };
    };
  });
});

//-------------
// Brand-Activation Brand by idBrand
router.put('/activateBrand', function(req, res, next){
  var data = Brand.findOne({id_brand: req.body.id_brand});
  data.exec(function(err, result){
    if (err){
      console.log(err);
    } else {
      if (result == null){
        res.json({status:'Brand Not Found'}).status(404);
      } else {
        var dataold = {id_brand: req.body.id_brand};
        var options = {};
        if (result.date_member == null){
          var datanew = {status: true, date_member: new Date() };
        } else {
          var datanew = {status: true };
        };
        Brand.update(dataold,datanew,options,callback);
        function callback(err, numAffected){
          if (err){
            console.log(err);
          } else {
            var dataold = {id_brand: req.body.id_brand};
            var datanew = {status: true};
            var options = {};
            User.update(dataold, datanew, options, callback)
            function callback(err, numAffected){
              if(err){
                console.log(err);
              } else {
                res.json({status:'Brand Activate, User Activate'}).status(200);
              };
            };
          };
        };
      };
    };
  });
});

//-------------
// Brand-Banned Brand by idBrand
router.put('/bannedBrand', function(req, res, next){
  var data = Brand.findOne({id_brand: req.body.id_brand});
  data.exec(function(err, result){
    if (err){
      console.log(err);
    } else {
      if (result == null){
        res.json({status:'Brand Not Found'}).status(404);
      } else {
        var dataold = {id_brand: req.body.id_brand};
        var options = {};
        var datanew = {status: false};
        
        Brand.update(dataold,datanew,options,callback);
        function callback(err, numAffected){
          if (err){
            console.log(err);
          } else {
            var dataold = {id_brand: req.body.id_brand};
            var datanew = {status: false};
            var options = {};
            User.update(dataold, datanew, options, callback)
            function callback(err, numAffected){
              if(err){
                console.log(err);
              } else {
                res.json({status:'Brand Banned, User Banned'}).status(200);
              };
            };
          };
        };
      };
    };
  });
});

//-------------
// Brand-Update Brand by idBrand 
router.put('/updateBrand', function(req, res, next){
  var data = Brand.findOne({id_brand:req.body.id_brand});
  data.exec(function(err, result){
    if (err){
      console.log(err);
    } else {
      if (result != null){
        var data = Brand.find({});
        data.exec(function (err, result2){
          var checkNameBrand = false;
          for (var i=0; i<result2.length; i++){
            var brandResult = result2[i].name_brand.toLowerCase();
            var brandInput = req.body.name_brand.toLowerCase();

            if ((brandResult == brandInput) && (result2[i].id_brand != req.body.id_brand)){
              checkNameBrand = true;
              res.status(409).json({status:'Conflict Name Brand'});
              break;
            };
            if (i == result2.length-1 && checkNameBrand == false){
              var dataold = {id_brand: req.body.id_brand};
              var datanew = {$set:req.body};
              var options = {};
              Brand.update(dataold, datanew, options, callback);

              function callback(err, numAffected){
                if (err){
                  console.log(err);
                } else {
                  res.status(200).json({status:'Update Brand Success'});
                };
              };
            };
          };
        });
      } else {
        res.status(404).json({status: 'Brand Not Found'});
      };
    };
  });
});

//-------------
// Brand-Update Image Brand by idBrand 
router.put('/updateImageBrand', function(req, res, next){
  console.log('ini idBrand');
  console.log(req.body.id_brand);
  var data = Brand.findOne({id_brand:req.body.id_brand});
  data.exec(function(err, result){
    if (err){
      console.log(err);
    } else {
      if (result != null){
        var dataold = {id_brand: req.body.id_brand};
        var datanew = {path_image: req.body.path_image};
        var options = {};
        Brand.update(dataold, datanew, options, callback);

        function callback(err, numAffected){
          if (err){
            console.log(err);
          } else {
            res.json({status:'Update Image Brand Success'}).status(200);
          };
        };
      } else {
        res.json({status: 'Brand Not Found'}).status(404);
      };
    };
  });
});

//-------------
// Brand-Update Store by idBrand idStore
router.put('/updateStore', function(req, res, next){
  var data = Brand.findOne({id_brand:req.body.id_brand});
  data.exec(function (err, result){
    if (err){
      console.log(err);
    } else {
      if (result == null){
        res.status(404).json({status:'Brand Not Found'});
      } else {
        var stores = result.store;
        var checkNameStore = false;
        for (var i=0; i<stores.length; i++){
          var storeResult = stores[i].name_store.toLowerCase();
          var storeInput = req.body.name_store.toLowerCase();
          if ((storeResult == storeInput) && (stores[i].id_store != req.body.id_store) && (stores[i].deleted == false)){ 
            checkNameStore = true;
            res.status(409).json({status:'Conflict'});
            break;
          };
          if (i == stores.length-1 && checkNameStore == false) {
            var dataold = {id_brand: req.body.id_brand,'store.id_store':req.body.id_store};
            var datanew = {$set:{ 'store.$.name_store': req.body.name_store,
                                  'store.$.address_store':{ detail: req.body.address_store.detail,
                                                            city_id: req.body.address_store.city_id,
                                                            city_name: req.body.address_store.city_name,
                                                            province_id: req.body.address_store.province_id,
                                                            province_name: req.body.address_store.province_name
                                                          },
                                  'store.$.contact_store':{ email_store: req.body.contact_store.email_store,
                                                            phone_store: req.body.contact_store.phone_store
                                                          },
                                  'store.$.social_media_store': { ig: req.body.social_media_store.ig,
                                                                  fb: req.body.social_media_store.fb
                                                                }}};
            var options = {};
            Brand.update(dataold, datanew, options, callback);

            function callback(err, numAffected){
              if (err){
                console.log(err);
              } else {
                res.status(200).json({status:'Update Store Success'});
              };
            };
          };
        };
      };
    };
  });
});

//-------------
// Brand-Update Image Store by idBrand idStore
router.put('/updateImageStore', function(req, res, next){
  var data = Brand.findOne({id_brand: req.body.id_brand});
  data.exec(function(err, result){
    if(err){
      console.log(err);
    } else {
      if (result == null){
        res.status(404).json({status:'Brand Not Found'});
      } else {
        var aS = false;
        for (var i=0; i<result.store.length; i++){
          if(result.store[i].id_store == req.body.id_store){
            aS = true;
            var dataold = {id_brand: req.body.id_brand,'store.id_store':req.body.id_store};
            var datanew = {$set:{ 'store.$.path_image': req.body.path_image}};
            var options = {};
            Brand.update(dataold, datanew, options, callback);
            function callback (err){
              if(err){
                console.log(err);
              } else {
                res.status(200).json({status:'Update Image Store Success'});
              };
            };
            break;
          };
          if (i == result.store.length-1 && aS == false){
            res.status(404).json({status:'Store Not Found'});
          };
        };
      };
    };
  });
});

//-------------
// Brand-Delete Store by idBrand 
router.put('/delete/store', function(req, res, next){
  var data = Brand.findOne({id_brand:req.body.id_brand});
  data.exec(function(err, result){
    if(err){
      console.log(err);
    } else {
      if (result == null){
        res.status(404).json({status:'Not Found'});
      } else {
        findStore(result, req.body.id_store, function(hasil){
          if(hasil == 'not found'){
            res.status(404).json({status:'Not Found'});
          } else {
            var field = "store."+hasil+".deleted";
            var value = true;
            var obj = {};
              obj[field] = value;
            var dataold = {id_brand: req.body.id_brand};
            var datanew = obj;
            var options = {};
            Brand.update(dataold, datanew, options, callback);
            function callback(err){
              if(err){
                console.log(err);
              } else {
                deleteUser(req.body.id_brand, req.body.id_store, function(hasil2){
                  res.status(200).json({status:'Success'});
                });
              };
            };
          };
        });
      };
    };
  });
});

//-------------
// Brand-Show Stores by idBrand 
router.get('/stores/:idBrand', function(req, res, next){
  var data = Brand.findOne({id_brand:req.params.idBrand});
  data.exec(function(err, result){
    if (err){
      console.log(err);
    } else {
      if (result == null){
        res.status(404).json({status:'Brand Not Found'});
      } else {
        res.status(200).json(result);
      };
    };
  });
});

//-------------
// Brand-Show Store by idBrand idStore
router.get('/store/:idBrand/:idStore', function(req, res, next){
  var data = Brand.aggregate  ([  { $match: {id_brand:req.params.idBrand}}, //select brand where id_brand: req.params.id
                                  { $unwind: "$store" }, //memecah array pada hasil seleksi brand
                                  { $match: {'store.id_store': req.params.idStore}}, //kondisi array yg akan ditampilkan
                                  { $project: { _id: 0, store: 1 }} //field yg akan ditampilkan (dan banyaknya baris / msh ragu) but ini hanya optional
                              ]);
  data.exec(function(err, result){
    if (err){
      console.log(err);
    } else {
      if (result == ""){
        res.status(404).json({status:'Brand Not Found'});
      } else {
        res.status(200).json(result);
      };
    };
  });
});

//-------------
// Brand-Add SuperUSer (dummy)
router.post('/addSuperUser', function(req, res, next){
  var input = new User ({ id_brand: 'barakpos.000012',
                          status : true,
                    store:{
                      id_store: 'store.XXXX',
                      name_store: 'general',
                      user: {
                        info_user:{
                          id_user:'SU.1',
                          email: req.body.user.info_user.email,
                          username: req.body.user.info_user.username,
                          password: req.body.user.info_user.password,
                          phone: req.body.user.info_user.phone,
                          role:{
                            id_role: 1,
                            name_role: 'Super User'
                          }
                        }
                      }
                    },
                 });
  input.save(function(err){
    if(err){
      console.log(err);
    } else { 
      var dataold = {email:req.body.user.info_user.email};
      var datanew = {status: true};
      var options = {};

      Registration.update(dataold,datanew,options,callback);
      function callback (err, numAffected){
        if(err){
          console.log(err);
        } else{
          res.json({status:'Input Super User Success'}).status(200);
        };
      };
    };
  });
});
// ================



function createGenerateIdBrand(callback){
  //---NOTE, ID BRAND FORMAT, kikapos.XXXXXX(no.urut generate)---//
  var data = Brand.find({});
  data.exec(function(err, result){
    if (err){
      console.log(err);
    } else {
      var idBrand;
      var arrayNumber =[];
      var number;
      if (result == ""){
        idBrand = 'kikaPOS.000001';
        callback(idBrand);
      } else {
      //replace result id_brand(read: 5 digit terakhir) ke arrayNumber(posisi msh blm urut dr kecil ke besar)
        for (var i=0; i<result.length; i++){
          arrayNumber.push(parseInt(result[i].id_brand.substring(8,14)));
        };
      //sort id_brand(read: 5 digit terakhir) dr bilangan kecil ke besar pada arrayNumber
        for (var i=0; i<arrayNumber.length;i++){
          for (var j=0; j<arrayNumber.length; j++){
            if (arrayNumber[j]>arrayNumber[j+1]){
              temp = arrayNumber[j];
              arrayNumber[j] = arrayNumber[j+1];
              arrayNumber[j+1] = temp;
            }; 
          };
        };
      //cari nilai id_brand(read: 5 digit terakhir) yg blm terpakai dr hasil urutan arrayNumber. 
      //Tujuan: biar gak mubadzir digit. 
        for (var i=0; i<arrayNumber.length; i++){
          if (i == 0 && arrayNumber[i] != 1){
            number = 1;
            break
          } else if (i == (arrayNumber.length-1)){
            number = i+2;
            break;
          } else {
            if (arrayNumber[i+1] != i+2){
              number = i+2;
              break;
            };
          };
        };
      //susun format id_brand
        if (number <= 9){
          idBrand = 'kikaPOS.00000'+number;
        } else if (number <= 99){
          idBrand = 'kikaPOS.0000'+number;
        } else if (number <= 999){
          idBrand = 'kikaPOS.000'+number;
        } else if (number <= 9999){
          idBrand = 'kikaPOS.00'+number;
        } else if (number <= 99999){
          idBrand = 'kikaPOS.0'+number;
        } else if (number <= 999999){
          idBrand = 'kikaPOS.'+number;
        } else {
          idBrand = 'maxBrand';
        };
        callback(idBrand);
      };
    };
  });
};

function createGenerateIdStore(idBrand, callback){
  //---NOTE, ID STORE FORMAT, store.XXXX(no.urut generate)---//
  var data = Brand.findOne({id_brand:idBrand});
  data.exec(function(err, result){
    if (err){
      console.log(err);
    } else {
      var idStore;
      var arrayNumber =[];
      var number;
      /*console.log(result);*/
      if (result == null){
        callback(0);
      } else {
        if (result.store.length == 1){
          idStore = 'store.0001';
          callback(idStore);
        } else {
          //replace result id_store(read: 4 digit terakhir) ke arrayNumber(posisi msh blm urut dr kecil ke besar)
          for (var i=1; i<result.store.length; i++){
            arrayNumber.push(parseInt(result.store[i].id_store.substring(6,10)));
          };
        //sort id_store(read: 4 digit terakhir) dr bilangan kecil ke besar pada arrayNumber
          for (var i=0; i<arrayNumber.length;i++){
            for (var j=0; j<arrayNumber.length; j++){
              if (arrayNumber[j]>arrayNumber[j+1]){
                temp = arrayNumber[j];
                arrayNumber[j] = arrayNumber[j+1];
                arrayNumber[j+1] = temp;
              }; 
            };
          };
        //cari nilai id_store(read: 4 digit terakhir) yg blm terpakai dr hasil urutan arrayNumber. 
        //Tujuan: biar gak mubadzir digit. 
          for (var i=0; i<arrayNumber.length; i++){
            if (i == 0 && arrayNumber[i] != 1){
              number = 1;
              break
            } else if (i == (arrayNumber.length-1)){
              number = i+2;
              break;
            } else {
              if (arrayNumber[i+1] != i+2){
                number = i+2;
                break;
              };
            };
          };
        //susun format id_store
          if (number <= 9){
            idStore = 'store.000'+number;
          } else if (number <= 99){
            idStore = 'store.00'+number;
          } else if (number <= 999){
            idStore = 'store.0'+number;
          } else if (number <= 9999){
            idStore = 'store.'+number;
          } else {
            idStore = 'maxStore';
          };
          callback(idStore);
        };
      };
    };
  });
};

function inputSatuan(idBrand, callback){
  var satuanName = ['kg','gram','ons','pcs','liter'];
  var field = [];
    field[0] = "id_satuan";
    field[1] = "name_satuan";
  var value = [];
  var obj = {};
  var arraySatuan = [];
  for (var i=0; i<satuanName.length; i++){
    obj = {};
    value[0] = "S.00"+(i+1);
    value[1] = satuanName[i];
    obj[field[0]] = value[0];
    obj[field[1]] = value[1];
    arraySatuan[i] = obj;
  };
  var data = SatuanBahan.findOne({id_brand:idBrand});
  data.exec(function(err, result){
    if (err){
      console.log(err);
    } else {
      if (result == null){
        //var satuanName = ['kg','gram','ons','pcs'];
        var input = new SatuanBahan ({  id_brand: idBrand,
                                        satuan: arraySatuan
                                    });
        input.save(function(err){
          if (err){
            console.log(err);
          } else {
            callback('success');
          };
        });
      } else {
        callback('error');
      };
    };
  });
};

function inputKategoriMenu(idBrand, callback){
  var kategoriName = ['Coffee','Dessert','Drink'];
  var field = [];
    field[0] = "id_kategori_menu";
    field[1] = "name_kategori_menu";
  var value = [];
  var obj = {};
  var arrayKategori = [];
  for (var i=0; i<kategoriName.length; i++){
    obj = {};
    value[0] = "K.00"+(i+1);
    value[1] = kategoriName[i];
    obj[field[0]] = value[0];
    obj[field[1]] = value[1];
    arrayKategori[i] = obj;
  };

  var data = KategoriMenu.findOne({id_brand:idBrand});
  data.exec(function(err, result){
    if (err){
      console.log(err);
    } else {
      if (result == null){
        //var kategoriName = ['Coffee','Dessert','Drink','Mie'];
        var input = new KategoriMenu ({ id_brand: idBrand,
                                          kategori_menu: arrayKategori
                                      });
        input.save(function(err, result){
          if (err){
            console.log(err);
          } else {
            callback('success');
          };
        });
      } else {
        callback('error');
      };
    };
  });
};

function findStore(dataBrand, idStore, callback){
  var lS = dataBrand.store.length;
  var available = false;
  for (var i=0; i<lS; i++){
    if(dataBrand.store[i].id_store == idStore && dataBrand.store[i].deleted == false){
      callback(i);
      available == true;
      break;
    };
    if (i == lS-1 && available == false){
      callback('not found');
    };
  };
};

function deleteUser(idBrand, idStore, callback){
  var data = User.findOne({id_brand: idBrand});
  data.exec(function(err, result){
    if(err){
      console.log(err);
    } else {
      if (result == null){
        callback(0);
      } else {
        var lS = result.store.length;
        var available = false;
        for (var i=0; i<lS; i++){
          if(result.store[i].id_store == idStore){
            available = true;
            var lU = result.store[i].user.length;
            for (var j=0; j<lU; j++){
              if (result.store[i].user[j].info_user.deleted == false){
                var field = "store."+i+".user."+j+".info_user.deleted";
                var value = true;
                var obj = {};
                  obj[field] = value;
                var dataold = {id_brand: idBrand};
                var datanew = obj;
                var options = {};
                User.update(dataold, datanew, options, hasil);
                function hasil(err){
                  if (err){
                    console.log(err);
                  };
                };
              }; 
            };
          };
          if(available == true){
            callback(1);
            break;
          }else if (available == false && i == lS-1){
            callback(0);
          };
        };
      };
    };
  });
};