var cors = require('cors');
var express = require('express'),
  app = express();
  router = express.Router(),
  mongoose = require('mongoose'),
  SatuanBahan = mongoose.model('SatuanBahan');

module.exports = function (app) {
  app.use(cors());
  app.use('/v1', router);
};



// =====================
//-------------
// Satuan-Add
router.post('/satuan', function(req, res, next){
	createIdSatuan(req.body.id_brand, req.body.name_satuan, function(result){
		if (result == 'error'){
			res.status(409).json({status: 'Conflict Name Satuan'});
		} else if( result == 'maxSatuan'){
			res.status(403).json({status: 'Max Satuan'});
		} else {
			var dataold = {id_brand: req.body.id_brand};
			var datanew = {$push:{satuan:{	id_satuan: result,
											name_satuan: req.body.name_satuan
										}}};
			var options = {};
			SatuanBahan.update(dataold, datanew, options, callback);
			function callback(err){
				if (err){
					console.log(err);
				} else {
					res.status(200).json({status:'Add Satuan Success'});
				};
			};
		};
	});
});

//-------------
// Satuan-Update
router.put('/satuan', function(req, res, next){
	var data = SatuanBahan.findOne({$and:[{id_brand: req.body.id_brand},{'satuan.id_satuan':req.body.id_satuan}]});
	data.exec(function(err, result){
		if (err){
			console.log(err);
		} else {
			if (result == null){
				res.status(404).json({status:'Not found Satuan'});
			} else {
				var available = false;
				for (var i=0; i<result.satuan.length; i++){
					var satuanResult = result.satuan[i].name_satuan.toLowerCase();
        			var satuanInput = req.body.name_satuan.toLowerCase();
					if (satuanResult == satuanInput && result.satuan[i].id_satuan != req.body.id_satuan && result.satuan[i].deleted  == false){
						available = true;
						res.status(409).json({status:'Conflict Name Satuan'});
						break;
					};
					if (i == result.satuan.length-1 && available == false){
						var dataold = {id_brand:req.body.id_brand, 'satuan.id_satuan': req.body.id_satuan};
						var datanew = {'satuan.$.name_satuan': req.body.name_satuan};
						var options = {};
						SatuanBahan.update(dataold, datanew, options, callback);
						function callback(err){
							if (err){
								console.log(err);
							} else {
								res.status(200).json({status:'Update Success'});
							};
						};
					};
				};
			};
		};
	});
});

//-------------
// Satuan-Delete
router.put('/delete/satuan', function(req, res, next){
	var data = SatuanBahan.findOne({$and:[{id_brand: req.body.id_brand},{'satuan.id_satuan':req.body.id_satuan}]});
	data.exec(function(err, result){
		if (err){
			console.log(err);
		} else {
			if (result == null){
				res.status(404).json({status:'Satuan Not Found'});
			} else {
				var dataold = {id_brand:req.body.id_brand, 'satuan.id_satuan': req.body.id_satuan};
				var datanew = {'satuan.$.deleted': true};
				var options = {};
				SatuanBahan.update(dataold, datanew, options, callback);
				function callback(err){
					if (err){
						console.log(err);
					} else {
						res.status(200).json({status:'Update Success'});
					};
				};
			};
		};
	});
});

//-------------
// Satuan-Get
router.get('/satuan/:idBrand', function(req, res, next){
	var data = SatuanBahan.findOne({id_brand: req.params.idBrand});
	data.exec(function(err, result){
		if (err){
			console.log(err);
		} else {
			res.status(200).json(result);
		}
	});
});
// =====================



function createIdSatuan(idBrand, nameSatuan, callback){
	var data = SatuanBahan.findOne({id_brand: idBrand});
  	data.exec(function(err, result){
	    if (err){
	      console.log(err);
	    } else {
	    	var idSatuan;
	    	var available = false;
	      	var arrayNumber =[];
	      	var number;
	      	for (var i=0; i<result.satuan.length; i++){
	      		var satuanResult = result.satuan[i].name_satuan.toLowerCase();
        		var satuanInput = nameSatuan.toLowerCase();
      			if (satuanResult == satuanInput && result.satuan[i].deleted == false){
      				available = true;	
      				callback('error');
      				break;
      			};
      			if (i == result.satuan.length-1 && available == false){
      				//replace result id_satuan(read: 3 digit terakhir) ke arrayNumber(posisi msh blm urut dr kecil ke besar)
			        for (var i=0; i<result.satuan.length; i++){
			          arrayNumber.push(parseInt(result.satuan[i].id_satuan.substring(2,5)));
			        };
				    //sort id_satuan(read: 3 digit terakhir) dr bilangan kecil ke besar pada arrayNumber
			        for (var i=0; i<arrayNumber.length;i++){
			          for (var j=0; j<arrayNumber.length; j++){
			            if (arrayNumber[j]>arrayNumber[j+1]){
			              temp = arrayNumber[j];
			              arrayNumber[j] = arrayNumber[j+1];
			              arrayNumber[j+1] = temp;
			            }; 
			          };
			        };
				    //cari nilai id_satuan(read: 3 digit terakhir) yg blm terpakai dr hasil urutan arrayNumber. 
				    //Tujuan: biar gak mubadzir digit. 
			        for (var i=0; i<arrayNumber.length; i++){
			          if (i == 0 && arrayNumber[i] != 1){
			            number = 1;
			            break
			          } else if (i == (arrayNumber.length-1)){
			            number = i+2;
			            break;
			          } else {
			            if (arrayNumber[i+1] != i+2){
			              number = i+2;
			              break;
			            };
			          };
			        };
				    //susun format id_satuan
			        if (number <= 9){
			        	idSatuan = 'S.00'+number;
			        } else if (number <= 99){
			          	idSatuan = 'S.0'+number;
			        } else if (number <= 999){
			          	idSatuan = 'S.'+number;
			        } else {
			        	idSatuan = 'maxSatuan';
			        };
			        callback(idSatuan);
      			};
	      	};
	    };
	});
};
