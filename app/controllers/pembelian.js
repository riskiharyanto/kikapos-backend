var cors = require('cors');
var express = require('express'),
  app = express();
  router = express.Router(),
  mongoose = require('mongoose'),
  BahanBaku = mongoose.model('BahanBaku'),
  Pembelian = mongoose.model('Pembelian'),
  Menu = mongoose.model('Menu');

module.exports = function (app) {
  app.use(cors());
  app.use('/v1', router);
};



// =====================
//-------------
// Pembelian-Add Pembelian 
router.post('/pembelian', function(req, res, next){
	findNestedPembelian(req.body.id_brand, req.body.id_store, req.body.pembelian.id_pembelian, req.body.pembelian.detail_pembelian.id_bahan_baku, function(result){
		if (result[0] == "null"){
			//input all
			createIdPembelian(req.body.id_brand, req.body.id_store,  req.body.pembelian.tanggal, function(resultidPembelian){
				var input = new Pembelian({ id_brand: req.body.id_brand,
											store: [{
												id_store: req.body.id_store,
												pembelian:[{
													id_pembelian: resultidPembelian,
													user:{
														id_user: req.body.pembelian.user.id_user,
														username: req.body.pembelian.user.username
													},
													tanggal: req.body.pembelian.tanggal,
													jam: req.body.pembelian.jam,
													detail_pembelian:[{
														id_bahan_baku: req.body.pembelian.detail_pembelian.id_bahan_baku,
														name_bahan_baku: req.body.pembelian.detail_pembelian.name_bahan_baku,
														satuan:{
															id_satuan: req.body.pembelian.detail_pembelian.satuan.id_satuan,
															name_satuan: req.body.pembelian.detail_pembelian.satuan.name_satuan
														},
														price: req.body.pembelian.detail_pembelian.price,
														qty: req.body.pembelian.detail_pembelian.qty,
														sub_total: req.body.pembelian.detail_pembelian.price*req.body.pembelian.detail_pembelian.qty
													}],
													total_pembelian: req.body.pembelian.detail_pembelian.price*req.body.pembelian.detail_pembelian.qty,
													status: true
												}]
											}]
										});
				input.save(function(err){
					if (err){
						console.log(err);
					} else {
						console.log('Input 1 Success');
						findNestedBahanBaku(req.body.id_brand, req.body.id_store, req.body.pembelian.detail_pembelian.id_bahan_baku,"input", function(hasil){
							if (hasil[0] == "null"){
								console.log('Brand Not Found');
								res.status(404);
							} else {
								if (hasil[1] == "null"){
									console.log('Store Not Found');
									res.status(404);
								} else {
									if (hasil[2] == "null"){
										console.log('Bahan Baku Not Found');
										res.status(404);
									} else {
										var field = "store."+hasil[1]+".bahan_baku."+hasil[2]+".stok";
										var value = req.body.pembelian.detail_pembelian.qty+hasil[3];
										var obj = {};
										    obj[field] = value;
										var dataold = {id_brand: req.body.id_brand};
										var datanew = {$set: obj};
										var options = {};
										BahanBaku.update(dataold, datanew, options, callback);
										function callback (err){
											if (err){
												console.log(err);
											} else {
												getBahanBakuNew (req.body.id_brand, req.body.id_store, function(hasil2){
													res.status(200).json({status:'Success', id_pembelian: resultidPembelian});
												});
											};
										};
									};
								};
							};
						});
					};
				});
			});
		} else {
			if (result[1] == "null"){
				//push store, input pembelian, input detailPembelian
				createIdPembelian(req.body.id_brand, req.body.id_store, req.body.pembelian.tanggal, function(resultidPembelian){
					var dataold = {id_brand: req.body.id_brand};
					var datanew = {$push:{ store: { 
												id_store: req.body.id_store,
												pembelian:[{
													id_pembelian: resultidPembelian,
													user:{
														id_user: req.body.pembelian.user.id_user,
														username: req.body.pembelian.user.username
													},
													tanggal: req.body.pembelian.tanggal,
													jam: req.body.pembelian.jam, 
													detail_pembelian:[{
														id_bahan_baku: req.body.pembelian.detail_pembelian.id_bahan_baku,
														name_bahan_baku: req.body.pembelian.detail_pembelian.name_bahan_baku,
														satuan:{
															id_satuan: req.body.pembelian.detail_pembelian.satuan.id_satuan,
															name_satuan: req.body.pembelian.detail_pembelian.satuan.name_satuan
														},
														price: req.body.pembelian.detail_pembelian.price,
														qty: req.body.pembelian.detail_pembelian.qty,
														sub_total: req.body.pembelian.detail_pembelian.price*req.body.pembelian.detail_pembelian.qty
													}],
													total_pembelian: req.body.pembelian.detail_pembelian.price*req.body.pembelian.detail_pembelian.qty,
												}]
											}}};
					var options = {};
					Pembelian.update(dataold, datanew, options, callback);
					function callback(err){
						if (err){
							console.log(err);
						} else {
							console.log('Input 2 Success');
							findNestedBahanBaku(req.body.id_brand, req.body.id_store, req.body.pembelian.detail_pembelian.id_bahan_baku, "input", function(hasil){
								if (hasil[0] == "null"){
									console.log('Brand Not Found');
								} else {
									if (hasil[1] == "null"){
										console.log('Store Not Found');
									} else {
										if (hasil[2] == "null"){
											console.log('Bahan Baku Not Found');
										} else {
											var field = "store."+hasil[1]+".bahan_baku."+hasil[2]+".stok";
											var value = req.body.pembelian.detail_pembelian.qty+hasil[3];
											var obj = {};
											    obj[field] = value;
											var dataold = {id_brand: req.body.id_brand};
											var datanew = {$set: obj};
											var options = {};
											BahanBaku.update(dataold, datanew, options, callback);
											function callback (err){
												if (err){
													console.log(err);
												} else {
													getBahanBakuNew (req.body.id_brand, req.body.id_store, function(hasil2){
														res.status(200).json({status:'Success', id_pembelian: resultidPembelian});
													});
												};
											};
										};
									};
								};
							});
						};
					};
				});
			} else { 
				if (result[2] == "null"){
					//push pembelian, input detailPembelian
					createIdPembelian(req.body.id_brand, req.body.id_store, req.body.pembelian.tanggal, function(resultidPembelian){
						var satuanObj = {};
							var field_satuan1 = "id_satuan";
							var field_satuan2 = "name_satuan";
							var value_satuan1 = req.body.pembelian.detail_pembelian.satuan.id_satuan;
							var value_satuan2 = req.body.pembelian.detail_pembelian.satuan.name_satuan;
							satuanObj[field_satuan1] = value_satuan1;
							satuanObj[field_satuan2] = value_satuan2;
						var detailPembelianObj = {};
							var field_detailPembelian1 = "id_bahan_baku";
							var field_detailPembelian2 = "name_bahan_baku";
							var field_detailPembelian3 = "satuan";
							var field_detailPembelian4 = "price";
							var field_detailPembelian5 = "qty";
							var field_detailPembelian6 = "sub_total";
							var value_detailPembelian1 = req.body.pembelian.detail_pembelian.id_bahan_baku;
							var value_detailPembelian2 = req.body.pembelian.detail_pembelian.name_bahan_baku;
							var value_detailPembelian3 = satuanObj;
							var value_detailPembelian4 = req.body.pembelian.detail_pembelian.price;
							var value_detailPembelian5 = req.body.pembelian.detail_pembelian.qty;
							var value_detailPembelian6 = req.body.pembelian.detail_pembelian.price*req.body.pembelian.detail_pembelian.qty;
							detailPembelianObj[field_detailPembelian1] = value_detailPembelian1;
							detailPembelianObj[field_detailPembelian2] = value_detailPembelian2;
							detailPembelianObj[field_detailPembelian3] = value_detailPembelian3;
							detailPembelianObj[field_detailPembelian4] = value_detailPembelian4;
							detailPembelianObj[field_detailPembelian5] = value_detailPembelian5;
							detailPembelianObj[field_detailPembelian6] = value_detailPembelian6;
						var detailPembelianArr = [];
							//space1
							detailPembelianArr.push(detailPembelianObj);
						var userObj = {};
							var field_user1 = "id_user";
							var field_user2 = "username";
							var value_user1 = req.body.pembelian.user.id_user;
							var value_user2 = req.body.pembelian.user.username;
							userObj[field_user1] = value_user1;
							userObj[field_user2] = value_user2;
						var pembelianObj = {};
							var field_pembelian1 = 	"id_pembelian";
							var field_pembelian2 = 	"tanggal";
							var field_pembelian3 = 	"jam";
							var field_pembelian4 =	"user";
							var field_pembelian5 =	"detail_pembelian";
							var field_pembelian6 =	"total_pembelian";
							var field_pembelian7 =	"status";
							var value_pembelian1 =	resultidPembelian;
							var value_pembelian2 =	req.body.pembelian.tanggal;
							var value_pembelian3 =	req.body.pembelian.jam;
							var value_pembelian4 =	userObj;
							var value_pembelian5 =	detailPembelianArr;
							var value_pembelian6 =	req.body.pembelian.detail_pembelian.price*req.body.pembelian.detail_pembelian.qty;
							var value_pembelian7 =	true;
							pembelianObj[field_pembelian1] = value_pembelian1;
							pembelianObj[field_pembelian2] = value_pembelian2;
							pembelianObj[field_pembelian3] = value_pembelian3;
							pembelianObj[field_pembelian4] = value_pembelian4;
							pembelianObj[field_pembelian5] = value_pembelian5;
							pembelianObj[field_pembelian6] = value_pembelian6;
							pembelianObj[field_pembelian7] = value_pembelian7;
						var storeIndex = {};
							var field_storeIndex = "store."+result[1]+".pembelian";
							var value_storeIndex = pembelianObj;
							storeIndex[field_storeIndex] = value_storeIndex;

						var dataold = {id_brand: req.body.id_brand};
						var datanew = {$push: storeIndex};
						var options = {};
						Pembelian.update(dataold, datanew, options, callback);
						function callback(err){
							if (err){
								console.log(err);
							} else {
								console.log('Input 3 Success');
								//res.json({status:'Input 3 Success'}).status(200);
								findNestedBahanBaku(req.body.id_brand, req.body.id_store, req.body.pembelian.detail_pembelian.id_bahan_baku, "input", function(hasil){
									console.log(hasil);
									if (hasil[0] == "null"){
										console.log('Brand Not Found');
									} else {
										if (hasil[1] == "null"){
											console.log('Store Not Found');
										} else {
											if (hasil[2] == "null"){
												console.log('Bahan Baku Not Found');
											} else {
												var field = "store."+hasil[1]+".bahan_baku."+hasil[2]+".stok";
												var value = req.body.pembelian.detail_pembelian.qty+hasil[3];
												var obj = {};
												    obj[field] = value;
												console.log(obj);
												var dataold = {id_brand: req.body.id_brand};
												var datanew = {$set: obj};
												var options = {};
												BahanBaku.update(dataold, datanew, options, callback);
												function callback (err){
													if (err){
														console.log(err);
													} else {
														getBahanBakuNew (req.body.id_brand, req.body.id_store, function(hasil2){
															res.status(200).json({status:'Success', id_pembelian: resultidPembelian});
														});
													};
												};
											};
										};
									};
								});
							};
						};
					});
				} else {
					if (result[3] == "null"){
						//push detailPembelian
						var satuanObj = {};
							var field_satuan1 = "id_satuan";
							var field_satuan2 = "name_satuan";
							var value_satuan1 = req.body.pembelian.detail_pembelian.satuan.id_satuan;
							var value_satuan2 = req.body.pembelian.detail_pembelian.satuan.name_satuan;
							satuanObj[field_satuan1] = value_satuan1;
							satuanObj[field_satuan2] = value_satuan2;
						var detailPembelianObj = {};
							var field_detailPembelian1 = "id_bahan_baku";
							var field_detailPembelian2 = "name_bahan_baku";
							var field_detailPembelian3 = "satuan";
							var field_detailPembelian4 = "price";
							var field_detailPembelian5 = "qty";
							var field_detailPembelian6 = "sub_total";
							var value_detailPembelian1 = req.body.pembelian.detail_pembelian.id_bahan_baku;
							var value_detailPembelian2 = req.body.pembelian.detail_pembelian.name_bahan_baku;
							var value_detailPembelian3 = satuanObj;
							var value_detailPembelian4 = req.body.pembelian.detail_pembelian.price;
							var value_detailPembelian5 = req.body.pembelian.detail_pembelian.qty;
							var value_detailPembelian6 = req.body.pembelian.detail_pembelian.price*req.body.pembelian.detail_pembelian.qty;
							detailPembelianObj[field_detailPembelian1] = value_detailPembelian1;
							detailPembelianObj[field_detailPembelian2] = value_detailPembelian2;
							detailPembelianObj[field_detailPembelian3] = value_detailPembelian3;
							detailPembelianObj[field_detailPembelian4] = value_detailPembelian4;
							detailPembelianObj[field_detailPembelian5] = value_detailPembelian5;
							detailPembelianObj[field_detailPembelian6] = value_detailPembelian6;
						var pembelianIndex = {};
							var field_pembelianIndex = "store."+result[1]+".pembelian."+result[2]+".detail_pembelian";
							var value_pembelianIndex = detailPembelianObj;
							pembelianIndex[field_pembelianIndex] = value_pembelianIndex;
						
						var dataold = {id_brand: req.body.id_brand};
						var datanew = {$push: pembelianIndex};
						var options = {};
						Pembelian.update(dataold, datanew, options, callback);
						function callback(err){
							if (err){
								console.log(err);
							} else {
								//update totalPembelian
								sumTotalPembelian(req.body.id_brand, req.body.id_store, req.body.pembelian.id_pembelian, function(hasil){
									console.log(hasil);
									var field = "store."+result[1]+".pembelian."+result[2]+".total_pembelian";
									var value = hasil;
									var obj = {};
										//space
										obj[field] = value;
									var dataold = {id_brand: req.body.id_brand};
									var datanew = obj;
									var options = {};
									Pembelian.update(dataold, datanew, options, callback);
									function callback (err){
										if (err){
											console.log(err);
										} else {
											console.log('Input 4 Success');
											findNestedBahanBaku(req.body.id_brand, req.body.id_store, req.body.pembelian.detail_pembelian.id_bahan_baku, "input", function(hasil){
												if (hasil[0] == "null"){
													console.log('Brand Not Found');
												} else {
													if (hasil[1] == "null"){
														console.log('Store Not Found');
													} else {
														if (hasil[2] == "null"){
															console.log('Bahan Baku Not Found');
														} else {
															var field = "store."+hasil[1]+".bahan_baku."+hasil[2]+".stok";
															var value = req.body.pembelian.detail_pembelian.qty+hasil[3];
															var obj = {};
															    obj[field] = value;
															var dataold = {id_brand: req.body.id_brand};
															var datanew = {$set: obj};
															var options = {};
															BahanBaku.update(dataold, datanew, options, callback);
															function callback (err){
																if (err){
																	console.log(err);
																} else {
																	getBahanBakuNew (req.body.id_brand, req.body.id_store, function(hasil2){
																		res.status(200).json({status:'Success', id_pembelian:null});
																	});
																};
															};
														};
													};
												};
											});
										};
									};
								});
							};
						};
					} else {
						//update detailPembelian
						var field = [];
						    field[1] = "store."+result[1]+".pembelian."+result[2]+".detail_pembelian."+result[3]+".qty";
						    field[2] = "store."+result[1]+".pembelian."+result[2]+".detail_pembelian."+result[3]+".sub_total";
						var value = [];
						    value[1] = req.body.pembelian.detail_pembelian.qty+result[4];
						    value[2] = (req.body.pembelian.detail_pembelian.qty+result[4])*result[5];
						var obj = {};
						    obj[field[1]] = value[1];
						    obj[field[2]] = value[2];
						var dataold = {id_brand: req.body.id_brand};
						var datanew = {$set: obj};
						var options = {};
						Pembelian.update(dataold, datanew, options, callback);
						function callback(err){
							if (err){
								console.log(err);
							} else {
								//update totalPembelian
								sumTotalPembelian(req.body.id_brand, req.body.id_store, req.body.pembelian.id_pembelian, function(hasil){
									console.log(hasil);
									var field = "store."+result[1]+".pembelian."+result[2]+".total_pembelian";
									var value = hasil;
									var obj = {};
										//space
										obj[field] = value;
									var dataold = {id_brand: req.body.id_brand};
									var datanew = {$set: obj};
									var options = {};
									Pembelian.update(dataold, datanew, options, callback);
									function callback (err){
										if (err){
											console.log(err);
										} else {
											console.log('Input 4 Success');
											findNestedBahanBaku(req.body.id_brand, req.body.id_store, req.body.pembelian.detail_pembelian.id_bahan_baku, "input", function(hasil){
												if (hasil[0] == "null"){
													console.log('Brand Not Found');
												} else {
													if (hasil[1] == "null"){
														console.log('Store Not Found');
													} else {
														if (hasil[2] == "null"){
															console.log('Bahan Baku Not Found');
														} else {
															var field = "store."+hasil[1]+".bahan_baku."+hasil[2]+".stok";
															var value = req.body.pembelian.detail_pembelian.qty+hasil[3];
															var obj = {};
															    obj[field] = value;
															var dataold = {id_brand: req.body.id_brand};
															var datanew = {$set: obj};
															var options = {};
															BahanBaku.update(dataold, datanew, options, callback);
															function callback (err){
																if (err){
																	console.log(err);
																} else {
																	getBahanBakuNew (req.body.id_brand, req.body.id_store, function(hasil2){
																		res.status(200).json({status:'Success', id_pembelian: null});
																	});
																};
															};
														};
													};
												};
											});
										};
									};
								});
							};
						};
					};
				};
			};
		};
	});
});

//-------------
// Pembelian-Delete Pembelian 
router.put('/pembelian/deleted', function(req, res, next){
});

//-------------
// Pembelian-Cancel Pembelian 
router.put('/pembelian/cancel', function(req, res, next){
	cancel(req.body.id_brand, req.body.id_store, req.body.pembelian.id_pembelian, null, "cancelPembelian", function(result){
		if(result[0] == "null"){
			res.status(200).json({status:'Success No update Menu'});
		} else {
			getBahanBakuNew(req.body.id_brand, req.body.id_store, function(result1){
				res.status(200).json({status:'Success'});
			});
		};
		
	});
});

//-------------
// Pembelian-Cancel Detail Pembelian 
router.put('/pembelian/detail/cancel', function(req, res, next){
	cancel(req.body.id_brand, req.body.id_store, req.body.pembelian.id_pembelian, req.body.pembelian.id_bahan_baku, "cancelDetailPembelian", function(result1){
		if (result1[0] == 'not null' && result1[1] == 'not null' && result1[2] == 'not null' && result1[3] == 'not null' && result1[4] == 'not null'){
			sumTotalPembelian(req.body.id_brand, req.body.id_store, req.body.pembelian.id_pembelian, function(result){
				var dataold = {id_brand: req.body.id_brand};
				var f = "store."+result1[5]+".pembelian."+result1[6]+".total_pembelian";
				var v = result;
				var obj ={};
					obj[f] = v;
				var datanew = obj;
				var options = {};
				Pembelian.update(dataold, datanew, options, callback);
				function callback (err){
					if (err){
						console.log(err);
					} else {
						getBahanBakuNew(req.body.id_brand, req.body.id_store, function(result1){
							res.status(200).json({status:'Success'});
						});
					};
				};
			});
		} else {
			res.status(403).json({status:'Forbiden'});
		};
	});
});

//-------------
// Pembelian-Show Pembelian 
router.get('/pembelian/:idBrand/:idStore/:idPembelian', function(req, res, next){
	var data = Pembelian.aggregate ([ {$match:{id_brand: req.params.idBrand}},
									  {$unwind: "$store"},
									  {$match:{"store.id_store": req.params.idStore}},
									  {$unwind: "$store.pembelian"},
									  {$match:{"store.pembelian.id_pembelian": req.params.idPembelian}},
									  {$unwind: "$store.pembelian.detail_pembelian"},
									  {$match:{"store.pembelian.detail_pembelian.cancel": false}},
									  {$project:{pembelian: "$store.pembelian"}}
									]);
	data.exec(function(err, result){
		if(err){
			console.log(err);
		} else {
			if (result == ""){
				res.status(404).json({status:'Not Found'});
			} else {
				res.status(200).json(result);
			};
		};
	});
});
// =====================


function createIdPembelian(idBrand, idStore, tanggal, callback){
	//format idPembelian : tanggal(ddMMYYYY).XXXX
	var idPembelian;
	var d = tanggal.substr(8,2);
	var m = tanggal.substr(5,2);
	var y = tanggal.substr(0,4);
	var date = y+"-"+m+"-"+d;

	var data = Pembelian.aggregate 	([ 	{$match:{id_brand: idBrand}},
										{$unwind: "$store"},
										{$match: {"store.id_store":idStore}},
										{$unwind: "$store.pembelian"},
										{$match:{"store.pembelian.tanggal": new Date(date+" 00:00:00.000Z")}}
									]);
	data.exec(function(err, result){
		if(result == ""){
			idPembelian = d+""+m+""+y+".0001";
			callback(idPembelian);
		} else {
			var count = result.length;
			if (count<9){
				idPembelian = d+""+m+""+y+".000"+(count+1);
			} else if (count<99){
				idPembelian = d+""+m+""+y+".00"+(count+1);
			} else if (count<999){
				idPembelian = d+""+m+""+y+".0"+(count+1);
			} else if (count<9999){
				idPembelian = d+""+m+""+y+"."+(count+1);
			};
			callback(idPembelian);
		};
	});
};

function findNestedPembelian(idBrand, idStore, idPembelian, idBahanBaku, callback){
	// hasil[0] = brand (null/not null)
	// hasil[1] = store (null/index store)
	// hasil[2] = pembelian (null/index pembelian)
	// hasil[3] = detailPembelian (null/ index detailPembelian)
	var hasil =[];
	var lS, lP, lDP;
	var availableStore = false;
	var availablePembelian = false;
	var availableDetailPembelian = false;
	var data = Pembelian.find({id_brand:idBrand});
	data.exec(function(err, result){
		if(err){
			console.log(err);
		} else {
			if (result == null || result == ""){ //brand not found
				hasil.push('null') //0 ==> brand, null
				callback(hasil);
			} else { //brand found
				hasil.push('not null') //0 ==> brand, not null
				lS = result[0].store.length;
				for (var i=0; i<lS; i++){ //looping store
					if (availableDetailPembelian == true){ //break looping store jk detailPembelian available
						break;
					} else {
						if (result[0].store[i].id_store == idStore){ //store found
							availableStore = true;
							hasil.push(i); //1 ==> store, index store
							lP = result[0].store[i].pembelian.length;
							for (var j=0; j<lP; j++){ //looping pembelian
								if (availableDetailPembelian == true){ //break looping pembelian jk detailPembelian available
									break;
								} else {
									if (result[0].store[i].pembelian[j].id_pembelian == idPembelian){ //pembelian found
										availablePembelian = true;
										hasil.push(j); //2 ==> pembelian, index pembelian
										lDP = result[0].store[i].pembelian[j].detail_pembelian.length;
										for (var k=0; k<lDP; k++){ //looping detailPembelian
											if (availableDetailPembelian == true){ //break looping detailPembelian jk detailPembelian available
												break;
											} else {
												if ((result[0].store[i].pembelian[j].detail_pembelian[k].id_bahan_baku == idBahanBaku) && (result[0].store[i].pembelian[j].detail_pembelian[k].cancel == false)){ //detailPembelian found
													availableDetailPembelian = true;
													hasil.push(k); //3 ==> detailPembelian, index detailPembelian
													hasil.push(result[0].store[i].pembelian[j].detail_pembelian[k].qty) //4
													hasil.push(result[0].store[i].pembelian[j].detail_pembelian[k].price) //5
													break;
												};
												if (k == lDP-1 && availableDetailPembelian == false){ //detailPembelian not found
													hasil.push('null') //3 ==> detailPembelian, null
												};
											};
										};
									};
									if (j == lP-1 && availablePembelian == false){ //pembelian not found
										hasil.push('null') //2 ==> pembelian, null
									};
								};
							};
						};
						if (i == lS-1 && availableStore == false){ //store not found
							hasil.push('null'); //1 ==> store, null
						}; 
					};
				};
				callback(hasil);
			};
		};
	});
};

function sumTotalPembelian(idBrand, idStore, idPembelian, callback){
	var data = Pembelian.aggregate 	([ 	{$match:{id_brand: idBrand}},
										{$unwind: "$store"},
										{$match: {"store.id_store":idStore}},
										{$unwind: "$store.pembelian"},
										{$match: {"store.pembelian.id_pembelian": idPembelian}},
										{$unwind: "$store.pembelian.detail_pembelian"},
										{$match: {"store.pembelian.detail_pembelian.cancel": false}},
										{$group:{ _id: null, "total_pembelian": {$sum:"$store.pembelian.detail_pembelian.sub_total" }}}
									]);
	data.exec(function(err, result){
		if (err){
			console.log(err);
		} else {
			if (result == ""){
				callback(0);
			} else {
				callback(result[0].total_pembelian);
			};
		};
	});
};

function findNestedBahanBaku(idBrand, idStore, idBahanBaku, status, callback){
	// hasil[0] = brand (null/not null)
	// hasil[1] = store (null/index store)
	// hasil[2] = bahanBaku (null/index pembelian)
	var bahanBaku = [];
	var data = BahanBaku.findOne({id_brand: idBrand});
	data.exec(function(err, result){
		if (err){
			console.log(err);
		} else {
			var hasil = [];
			var lS, lB;
			var availableStore = false;
			var availableBahanBaku = false;
			if (result == null){
				hasil.push("null"); //0
				callback(hasil);
			} else {
				//console.log(result);
				hasil.push("not null"); //0
				lS = result.store.length;
				for (var i=0; i<lS; i++){
					if (availableBahanBaku == true){
						break;
					} else {
						if (result.store[i].id_store == idStore){
							availableStore = true;
							lB = result.store[i].bahan_baku.length;
							hasil.push(i); //1
							if (status == 'input'){
								for (var j=0; j<lB; j++){
									if (result.store[i].bahan_baku[j].id_bahan_baku == idBahanBaku && result.store[i].bahan_baku[j].deleted == false){
										hasil.push(j); //2
										hasil.push(result.store[i].bahan_baku[j].stok); //3
										availableBahanBaku = true;
										callback(hasil);
									}
									if (j == lB-1 && availableBahanBaku == false){
										hasil.push('null') //2
										callback(hasil);
									};
								};
							};
							if (status == 'cancelPembelian'){
								availableBahanBaku = true;
								var iBB;
								var nullBB = 0;
								for (var j=0; j<idBahanBaku.detail_pembelian.length; j++){
									if (idBahanBaku.detail_pembelian[j].cancel == false){
										bahanBaku.push(idBahanBaku.detail_pembelian[j].id_bahan_baku);
									};
								};
								for (var j=0; j<bahanBaku.length; j++){
									iBB = false;
									for (var k=0; k<lB; k++){
										if (iBB == true){
											break
										} else {
											if (result.store[i].bahan_baku[k].id_bahan_baku == bahanBaku[j] && result.store[i].bahan_baku[k].deleted == false){
												iBB = true;
												var f = "store."+i+".bahan_baku."+k+".stok"; 
												var v = result.store[i].bahan_baku[k].stok - idBahanBaku.detail_pembelian[j].qty;
												var objBB = {};
													objBB[f] = v;
												var dataold = {id_brand: idBrand};
												var datanew = objBB;
												var options = {};
												BahanBaku.update(dataold, datanew, options, success);
												function success (err){
													if (err){
														console.log(err);
													};
												};
											};
										};
										if (k == lB-1 && iBB == false){
											nullBB+= 1;
										}
										
										if (j == bahanBaku.length-1  && nullBB == bahanBaku.length){
											hasil.push('No Update'); //2
											hasil.push(idBahanBaku.index_store); //3
											hasil.push(idBahanBaku.index_pembelian); //4
											callback(hasil);
										};
										if (((j == bahanBaku.length-1 && iBB == true) || (j == bahanBaku.length-1 && (iBB == false && k == lB-1))) && nullBB != bahanBaku.length) {
											hasil.push('Update Success '+(idBahanBaku.detail_pembelian.length-nullBB)+' item'); //2
											hasil.push(idBahanBaku.index_store); //3
											hasil.push(idBahanBaku.index_pembelian); //4
											callback(hasil); 
										};
									};
								};
							};
							if (status == 'cancelDetailPembelian'){
								var iBB = false;
								for (var j=0; j<lB; j++){
									if (iBB == true){
										break;
									} else {
										if (result.store[i].bahan_baku[j].id_bahan_baku == idBahanBaku.id_bahan_baku && result.store[i].bahan_baku[j].deleted == false){
											iBB = true;
											var f = "store."+i+".bahan_baku."+j+".stok"; 
											var v = result.store[i].bahan_baku[j].stok - idBahanBaku.qty;
											var objBB = {};
												objBB[f] = v;
											var dataold = {id_brand: idBrand};
											var datanew = objBB;
											var options = {};
											BahanBaku.update(dataold, datanew, options, success);
											function success (err){
												if (err){
													console.log(err);
												} else {
													hasil.push('Success'); //2
													hasil.push(idBahanBaku.index_store); //3
													hasil.push(idBahanBaku.index_pembelian); //4
													hasil.push(idBahanBaku.index_detail_pembelian); //5
													callback(hasil);
												};
											};
										};
									};
									if (j == lB-1 && iBB == false){
										hasil.push('null'); //2
										hasil.push(idBahanBaku.index_store); //3
										hasil.push(idBahanBaku.index_pembelian); //4
										hasil.push(idBahanBaku.index_detail_pembelian); //5
										callback(hasil);
									};
								};
							};
						};
						if (i == lS-1 && availableStore == false){
							hasil.push('null') //1
							callback(hasil);
						};
					};
				};
			};
		};
	});
};

function cancel(idBrand, idStore, idPembelian, idBahanBaku, status, callback){
	var hasil = [];
	var data = Pembelian.findOne({id_brand: idBrand});
	data.exec(function(err, result){
		if(err){
			console.log(err);
		} else {
			if (result == null){
				hasil.push("null"); //0 ==> brand(pembelian), null
				callback (hasil);
			} else {
				hasil.push("not null"); //0 ==> brand(pembelian), not null
				var availableStore = false;
				var availablePembelian = false;
				var lS, lP, lDP;
				lS = result.store.length;
				for (var i = 0; i<lS; i++){
					if (availablePembelian == true){
						break;
					} else {
						if (result.store[i].id_store == idStore){
							hasil.push("not null") //1 ==> store(pembelian), not null
							availableStore = true;
							lP = result.store[i].pembelian.length;
							for (var j=0; j<lP; j++){
								if (availablePembelian == true){
									break;
								} else {
									if ((result.store[i].pembelian[j].id_pembelian == idPembelian) && (result.store[i].pembelian[j].cancel == false)){
										hasil.push("not null") //2 ==> pembelian(pembelian), not null
										availablePembelian = true;
										lDP = result.store[i].pembelian[j].detail_pembelian.length;
										if (status == "cancelPembelian"){
											if (lDP == 0){
												hasil.push("null") //3 ==> detail_pembelian(pembelian), null
												callback(hasil);
											} else {
												hasil.push("not null") //3 ==> detail_pembelian(pembelian), not null
												//find Bahan Baku
												var objvalue = {};
													objvalue = result.store[i].pembelian[j].detail_pembelian;
												var field  = [];
													field[0] = "detail_pembelian";
													field[1] = "index_store";
													field[2] = "index_pembelian";
												var value = [];
													value[0] = objvalue;
													value[1] = i;
													value[2] = j;
												var obj = {};
													obj[field[0]] = value[0];
													obj[field[1]] = value[1];
													obj[field[2]] = value[2];
												findNestedBahanBaku(idBrand, idStore, obj, status, function(hasil2){
													hasil.push(hasil2);
													var f = "store."+hasil2[3]+".pembelian."+hasil2[4]+".cancel"; 
													var v = true;
													var objP = {}
														objP[f] = v;
													var dataold = {id_brand: idBrand};
													var datanew = objP;
													var options = {};
													Pembelian.update(dataold, datanew, options, success);
													function success (err){
														if (err){
															console.log(err);
														};
														callback(hasil);
													};
												});
											};
										};
										if (status == "cancelDetailPembelian"){
											if (lDP == 0){
												hasil.push("null"); //3 ==> detail_pembelian(pembelian), null
												callback(hasil);
											} else {
												hasil.push("not null"); //3 ==> detail_pembelian(pembelian), not null
												availableDetail = false;
												for (var k=0; k<lDP; k++){
													if (availableDetail == true){
														break;
													} else {
														if ((result.store[i].pembelian[j].detail_pembelian[k].id_bahan_baku == idBahanBaku) && (result.store[i].pembelian[j].detail_pembelian[k].cancel == false)){
															hasil.push('not null'); //4 ==> bahan_baku(pembelian), not null
															availableDetail = true;
															var field =[];
																field[0] = "id_bahan_baku";
																field[1] = "qty";
																field[2] = "index_store"; 
																field[3] = "index_pembelian";
																field[4] = "index_detail_pembelian";
															var value = [];
																value[0] = result.store[i].pembelian[j].detail_pembelian[k].id_bahan_baku;
																value[1] = result.store[i].pembelian[j].detail_pembelian[k].qty;
																value[2] = i;
																value[3] = j;
																value[4] = k;
															var obj = {};
																obj[field[0]] = value[0];
																obj[field[1]] = value[1];
																obj[field[2]] = value[2];
																obj[field[3]] = value[3];
																obj[field[4]] = value[4];
															//find Bahan Baku
															findNestedBahanBaku(idBrand, idStore, obj, status, function(hasil2){
																var f = "store."+hasil2[3]+".pembelian."+hasil2[4]+".detail_pembelian."+hasil2[5]+".cancel"; 
																var v = true;
																var objP = {}
																	objP[f] = v;
																console.log(objP);
																var dataold = {id_brand: idBrand};
																var datanew = objP;
																var options = {};
																Pembelian.update(dataold, datanew, options, success);
																function success (err){
																	if (err){
																		console.log(err);
																	};
																	hasil.push(hasil2[3]); //5 ==> index store
																	hasil.push(hasil2[4]); //6 ==> index pembelian
																	callback(hasil);
																};
															});
														};
														if (k == lDP-1 && availableDetail == false){
															hasil.push('null'); //4 ==> bahan_baku(pembelian), null
															callback(hasil);
														};
													};
												};
											};
										};
										if (status == "delete"){
											var f = "store."+i+".pembelian."+j+".deleted"; 
											var v = true;
											var objP = {}
												objP[f] = v;
											var dataold = {id_brand: idBrand};
											var datanew = objP;
											var options = {};
											Pembelian.update(dataold, datanew, options, success);
											function success (err){
												if (err){
													console.log(err);
												};
												callback(hasil);
											};
										};
									};
									if (j == lP-1 && availablePembelian == false){
										hasil.push("null") //2 ==> pembelian(pembelian), null
										callback(hasil);
									};
								};
							};
						};
						if (i == lS-1 && availableStore == false){
							hasil.push("null") //1 ==> store(pembelian), null
							callback(hasil);
						};
					};
				};
			};
		};
	});
};

function getBahanBakuNew(idBrand, idStore, callback){
	var data = BahanBaku.findOne({id_brand: idBrand});
	data.exec(function(err, result){
		if(err){
			console.log(err);
		} else {
			if (result == null){
				callback(0);
			} else {
				var lS = result.store.length;
				var aS = false;
				for (var i=0; i<lS; i++){
					if (result.store[i].id_store == idStore){ //store not null
						aS = true;
						updateAllStokMenu(idBrand, idStore, result.store[i].bahan_baku, function(hasil){
							callback(hasil);
						});
						break;
					};
					if (i == lS-1 && aS == false){ //store null
						callback(1);
					};
				};
			};
		};
	});
};

function updateAllStokMenu(idBrand, idStore, bahanBaku, callback){
	var data  = Menu.findOne({id_brand:idBrand});
	data.exec(function(err, result){
		if(err){
			console.log(err);
		} else {
			if(result == null){
				callback(1);
			} else {
				var lS = result.store.length;
				var aS = false;
				for (var i=0; i<lS; i++){
					if (result.store[i].id_store == idStore){ //store on collection menu
						aS = true;
						var lM = result.store[i].menu.length;
						for (var j=0; j<lM; j++){ //looping menu
							if (result.store[i].menu[j].deleted == false){ //menu yg blm dihapus
								var detailMenu = result.store[i].menu[j].detail_menu;
								var a = [];
								var min;
								if (detailMenu.length == 0){
									var field = "store."+i+".menu."+j+".stok";
									var value = 0;
									var obj ={};
										obj[field] = value;
									var dataold = {id_brand: idBrand};
									var datanew = obj;
									var options = {};
									Menu.update(dataold, datanew, options, success);
									function success (err){
										if (err){
											console.log(err);
										};
									};
								} else {
									for (var l=0; l<detailMenu.length; l++){ // looping detailMenu
										for (var m=0; m<bahanBaku.length; m++){ // looping bahanBaku
											if (detailMenu[l].id_bahan_baku == bahanBaku[m].id_bahan_baku && bahanBaku[m].deleted == false && detailMenu[l].deleted == false){ //jk id_bahan_baku collection detail_menu = id_bahan_baku collection bahan baku dan keduanya blm dihapus 
												var  b = bahanBaku[m].stok/detailMenu[l].jumlah;
												a.push(Math.floor(b)); // push hasil bagi ke var a
											};
										};
										if (l == detailMenu.length-1){ // cari nilai terkecil dari var array a
											if (a.length == 0){
												var field = "store."+i+".menu."+j+".stok";
												var value = 0;
												var obj ={};
													obj[field] = value;
												var dataold = {id_brand: idBrand};
												var datanew = obj;
												var options = {};
												Menu.update(dataold, datanew, options, success);
												function success (err){
													if (err){
														console.log(err);
													};
												};
											} else {
												if (a.length == 1){
													min = a[0];
												} else if (a.length == 2){
													if (a[0] <= a[1]){
														min = a[0]
													} else {
														min = a[1];
													}
												} else {
													for (var k=0; k<a.length; k++){
														if (k == 0){
															min = a[k];
														} else {
															if (a[k]<= min){
																min = a[k]
															};
														};
													};
												};
												var field = "store."+i+".menu."+j+".stok";
												var value = min;
												var obj ={};
													obj[field] = value;
												var dataold = {id_brand: idBrand};
												var datanew = obj;
												var options = {};
												console.log(datanew);
												Menu.update(dataold, datanew, options, success);
												function success (err){
													if (err){
														console.log(err);
													};
												};
											};
										};
									};
								};
							};
						};
						callback(1);
						break;
					};
					if (i == lS-1 && aS == false){
						callback(1);
						break;
					};
				};
			};
		};
	});
};