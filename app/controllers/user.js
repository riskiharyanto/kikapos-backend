const saltRounds = 10;
var cors = require('cors');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var ls = require('local-storage');
var express = require('express'),
  app = express();
  router = express.Router(),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Brand = mongoose.model('Brand');

module.exports = function (app) {
  app.use(cors());
  app.use('/v1', router);
};
app.set('superSecret', 'UserLogin');

//hapus user diganti jadi deleted false;

//halaman login (email dan password)
//role user
//1 SuperUser => hak akses semua fitur utk semua store
//2 Manager => hak akses fitur laporan utk semua store
//3 Admin => hak akses fitur admin utk semua store
//4 Supervisor => hak akses fitur laporan store tertentu
//5 Logistic Store => hak akses fitur pembelian dan input bahan baku store tertentu 
//6 Kasir Store => hak akses fitur kasir(penjualan) store tertentu
//7 Server Store => hak akses fitur pemesanan(penjualan) store tertentu
//---NOTE, ID USER FORMAT
//SUPER USER      SU.1
//MANAGER         MA.XX   Generate
//ADMIN           AD.XXX  Generate
//SUPERVISOR      SV.XXX  Generate
//LOGISTIC STORE  LS.XXX  Generate
//KASIR STORE     KS.XXX  Generate
//SERVER STORE    SS.XXX  Generate


//!!!!!!!!!!!!!!   NOTE   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//email uniq utk semua user 
//username uniq hanya utk user yg 1 brand, username bisa dipakai org lain apabila beda brand


// =====================
//-------------
// User-Register
router.post('/user', function(req, res, next){
  cekUsernameAdd(req.body.id_brand, req.body.user.info_user.username, function(result){
    if(result == 'null'){
      getIdUser(req.body.id_brand, req.body.id_store, req.body.user.info_user.email, req.body.user.info_user.role.id_role, function(hasil){
        if (hasil[0] == 'Null'){
          if (hasil[1] == 'Not Null'){
            bcrypt.hash(req.body.user.info_user.password, saltRounds, function(err, hash){
              if (hasil[3] != 'max'){
                if (hasil[2] == 'Null'){
                  var dataold = {id_brand: req.body.id_brand};
                  var datanew = {$push: { "store":{ 
                                            id_store: req.body.id_store,
                                            name_store: req.body.name_store,
                                            user:[{
                                              info_user:{
                                                id_user: hasil[3],
                                                email:req.body.user.info_user.email,
                                                username:req.body.user.info_user.username,
                                                password: hash, 
                                                phone:req.body.user.info_user.phone,
                                                role:{
                                                  id_role: req.body.user.info_user.role.id_role,
                                                  name_role: req.body.user.info_user.role.name_role
                                                }
                                              }
                                            }]
                                          }}};
                  var options = {};
                  User.update(dataold, datanew, options, callback);
                  function callback(err, numAffected){
                    if (err){
                      console.log(err);
                    } else {
                      res.status(200).json({status:'Input User Success'});
                    };
                  };
                } else {
                  var dataold = {id_brand: req.body.id_brand,'store.id_store': req.body.id_store};
                  var datanew = {$push: { "store.$.user":{ 
                                            info_user:{
                                              id_user: hasil[3],
                                              email:req.body.user.info_user.email,
                                              username:req.body.user.info_user.username,
                                              password: hash, 
                                              phone:req.body.user.info_user.phone,
                                              role:{
                                                id_role: req.body.user.info_user.role.id_role,
                                                name_role: req.body.user.info_user.role.name_role
                                              }
                                            }
                                          }}};
                  var options = {};
                  User.update(dataold, datanew, options, callback);
                  function callback(err, numAffected){
                    if (err){
                      console.log(err);
                    } else {
                      res.status(200).json({status:'Input User Success'});
                    };
                  };
                };
              } else {
                res.status(403).json({status:'User Max'});
              };
            });
          } else {
             res.status(404).json({status:'Brand Not Found'});
          };
        } else {
          res.status(409).json({status:'Email Conflict'});
        };
      });
    } else {
      res.status(409).json({status:'Username Conflict'});
    };
  });          
});

//-------------
// User-Edit Profile
router.put('/user', function(req, res, next){
  //iStore,iUser diambil dr hasil login(disimpan pd local-storage client)
  cekUsernameUpdate(req.body.id_brand, req.body.iStore, req.body.iUser, req.body.user.info_user.username, function(hasil){
    if(hasil == 'null'){
      var field = [];
        field[0] = "store."+req.body.iStore+".user."+req.body.iUser+".info_user.phone";
        field[1] = "store."+req.body.iStore+".user."+req.body.iUser+".info_user.username"; 
      var value = [];
        value[0] = req.body.user.info_user.phone;
        value[1] = req.body.user.info_user.username;
      var obj = {};
        obj[field[0]] = value[0];
        obj[field[1]] = value[1];
      var dataold = {id_brand: req.body.id_brand};
      var datanew = {$set: obj};
      var options = {};
      User.update(dataold, datanew, options, callback);
      function callback(err){
        if (err){
          console.log(err);
        } else {
          res.status(200).json({status:'Update Success'});
        }; 
      };
    } else {
      res.status(409).json({status:'Username Conflict'});
    };
  });
});

//-------------
// User-Edit Ava Profile
router.put('/userImage', function(req, res, next){
  var field = "store."+req.body.iStore+".user."+req.body.iUser+".info_user.path_image";
  var value = req.body.user.info_user.path_image;
  var obj = {};
    obj[field] = value;
  var dataold = {id_brand: req.body.id_brand};
  var datanew =  obj;
  var options = {};
  User.update(dataold, datanew, options, callback);
  function callback(err){
    if (err){
      console.log(err);
    } else {
      res.json({status:'Update Success'}).status(200);
    }; 
  };
});

//-------------
// User-Change Password
router.put('/password', function(req, res, next){
  var data = User.find({});
  data.exec(function(err, result){
    if (err){
      console.log(err);
    } else {
      if (result == null){
        res.status(404).json({status:'Not Found'});
      } else {
        var password_hash = result[req.body.iBrand].store[req.body.iStore].user[req.body.iUser].info_user.password
        bcrypt.compare(req.body.old_password, password_hash, function(err, compare){
          if (err){
            console.log(err);
          } else {
            if (compare == true){
              bcrypt.hash(req.body.new_password, saltRounds, function(err, hash){
                if(err){
                  console.log(err);
                } else {
                  var field = "store."+req.body.iStore+".user."+req.body.iUser+".info_user.password";
                  var value = hash;
                  var obj = {};
                    obj[field] = value;
                  var dataold = {id_brand: req.body.id_brand};
                  var datanew = {$set: obj};
                  var options = {};
                  User.update(dataold, datanew, options, callback);
                  function callback(err){
                    if(err){
                      console.log(err);
                    } else {
                      res.status(200).json({status:'Change Password Success'});
                    };
                  };
                };
              });
            } else {
              res.status(403).json({status:'Password Old Wrong'});
            };
          };
        });
      };
    };
  });
}); 

//-------------
// User-Delete 
router.put('/delete/user', function(req,res,next){
  console.log(req.body.email);
  findUser(req.body.email, function(result){
    if (result == null){
      res.status(404).json({status:'User Not Found'});
    } else {
      var field = "store."+result.iStore+".user."+result.iUser+".info_user.deleted";
      var value = true;
      var obj = {};
        obj[field] = value;
      var dataold = {id_brand: result.id_brand};
      var datanew = obj;
      var options = {};
      User.update(dataold, datanew, options,callback);
      function callback(err){
        if (err){
          console.log(err);
        } else {
          res.status(200).json({status:'user deleted'});
        };
      };
    };
  });
});

//-------------
// User-Login
router.post('/login', function(req, res, next){
  findUser(req.body.email, function(result){
    if (result == null){
      res.status(404).json({status:'Email Not Found'});
    } else if (result == false){
      res.status(403).json({status:'Status User False'});
    } else {
      //get password hash from mongo
      var data = User.find({});
      data.exec(function(err, result2){
        if (err){
          console.log(err);
        } else {
          if (result2 == null){
            res.status(404).json({status:'Not Found'});
          } else {
            var password_hash = result2[result.iBrand].store[result.iStore].user[result.iUser].info_user.password;
            bcrypt.compare(req.body.password, password_hash, function(err, compare){
              if (err){
                console.log(err);
              } else {
                if (compare == true){
                  var token = jwt.sign(result, app.get('superSecret'),{
                                      expiresIn: 60*60
                              });
                  res.status(200).json({status:'Login Success',
                            token: token,
                            user: result
                          });
                } else {
                  res.status(404).json({status:'Password Wrong'});
                };
              };
            });
          };
        };
      });
    };
  });
});

//-------------
// User-Show User
router.get('/users/:idBrand', function(req, res, next){
  var data = User.find({id_brand:req.params.idBrand});
  data.exec(function(err, result){
    if (err){
      console.log(err);
    } else {
      res.status(200).json(result);
    }
  });
});

//-------------
// User-Check Token
router.get('/checktoken/:token', function(req, res, next){
  var decoded = jwt.decode(req.params.token);
  result = {decoded}
  res.json({result});
});


router.get('/try/:idBrand/:idStore', function(req, res, next){
  generateIdServer(req.params.idBrand, req.params.idStore, function(hasil){
    res.send(hasil);
  })
});
// =====================



function getIdUser (idBrand, idStore, email, idRole, callback){
  var hasil =[]; //isi hasil ada 4 => [ Email Null/Not Null] [Brand Null/Not Null] [Store Null/Not Null] [idUser]
  //check email null/not null dgn user global
  var data = User.aggregate([
              { $match: {$or:[{status: true},{status: false}]}},
              { $unwind: "$store"},
              { $unwind: "$store.user"},
              { $match: {$and:[{"store.user.info_user.email":email},{"store.user.info_user.deleted":false}]}},
              { $project: {  id_brand:"$id_brand", status: "$status", store: "$store"}}
            ]);
  data.exec(function(err, result){
    if(err){
      console.log(err);
    } else {
      if(result == ""){
        hasil.push('Null');
        //check brand null/not null
        var data = User.findOne({id_brand: idBrand});
        data.exec(function(err,result){
          if (err){
            console.log(err);
          } else {
            if (result == null){
              hasil.push('Null');
              callback(hasil);
            } else {
              hasil.push('Not Null');
              var count = result.store.length;
              var available = false;
              //check store null/not null
              for (var i=0; i<count; i++){
                if(result.store[i].id_store == idStore){
                  available = true;
                  break;
                }
              };
              if (available == true){
                  hasil.push('Not Null');
              } else {
                  hasil.push('Null');
              };

              //generateId
              if (idRole == 1){
                generateIdSuperUser(idBrand, idStore, function(idSuperUser){
                  hasil.push(idSuperUser);
                  callback(hasil);
                })
              } else if (idRole == 2){
                generateIdManager(idBrand, idStore,function(idManager){
                  hasil.push(idManager);
                  callback(hasil);
                });
              } else if (idRole == 3){
                generateIdAdmin(idBrand, idStore,function(idAdmin){
                  hasil.push(idAdmin);
                  callback(hasil);
                });
              } else if (idRole == 4){
                generateIdSupervisor(idBrand, idStore,function(idSupervisor){
                  hasil.push(idSupervisor);
                  callback(hasil);
                });
              } else if (idRole == 5){
                generateIdLogistic(idBrand, idStore,function(idLogistic){
                  hasil.push(idLogistic);
                  callback(hasil);
                });
              } else if (idRole == 6){
                generateIdKasir(idBrand, idStore,function(idKasir){
                  hasil.push(idKasir);
                  callback(hasil);
                });
              } else if (idRole == 7){
                generateIdServer(idBrand, idStore,function(idServer){
                  hasil.push(idServer);
                  callback(hasil);
                });
              };
            };
          };
        });
      } else {
        hasil.push('Not Null');
        callback(hasil);
      };
    };
  });
};

function generateIdSuperUser(idBrand, idStore, callback){
  //---NOTE, ID SUPER USER FORMAT, SU.XX(no.urut generate)---//
  var data = User.aggregate([
              { $match: {$and:[{id_brand:idBrand},{status:true}]}}, //cari sesuai id brand
              { $project: {_id: 0, store: 1}}, //struktur result
              { $unwind: "$store"}, //array store dipecah
              { $unwind: "$store.user"}, // array store.user dipecah
              { $match: {"store.id_store":idStore}}, // cari sesuai id store
              { $match: {"store.user.info_user.role.id_role":1}}, //cari sesuai id role
              { $project: { id_store: "$store.id_store" ,info_user: "$store.user.info_user"}} //struktur result
            ]);

  data.exec(function (err, result){
    if (err){
      console.log(err);
    } else {
      var idSuperUser;
      var arrayNumber =[];
      var number;
      if (result == ""){
        idSuperUser = 'SU.01';
      } else {
        //replace result id_user(read: 2 digit terakhir) ke arrayNumber(posisi msh blm urut dr kecil ke besar)
        for (var i=0; i<result.length; i++){
          arrayNumber.push(parseInt(result[i].info_user.id_user.substring(3,5)));
        };
        //sort id_user(read: 2 digit terakhir) dr bilangan kecil ke besar pada arrayNumber
        for (var i=0; i<arrayNumber.length;i++){
          for (var j=0; j<arrayNumber.length; j++){
            if (arrayNumber[j]>arrayNumber[j+1]){
              temp = arrayNumber[j];
              arrayNumber[j] = arrayNumber[j+1];
              arrayNumber[j+1] = temp;
            } 
          }
        }
        //cari nilai id_user(read: 2 digit terakhir) yg blm terpakai dr hasil urutan arrayNumber. 
        //Tujuan: biar gak mubadzir digit. 
        for (var i=0; i<arrayNumber.length; i++){
          if (i == 0 && arrayNumber[i] != 1){
            number = 1;
            break
          } else if (i == (arrayNumber.length-1)){
            number = i+2;
            break;
          } else {
            if (arrayNumber[i+1] != i+2){
              number = i+2;
              break;
            }
          };
        };
        //susun format id_user
        if (number <10) {
          idSuperUser = 'SU.0'+number;
        } else if (number<100){
          idSuperUser = 'SU.'+number;
        } else {
          idSuperUser = 'max';
        };
      };
      callback(idSuperUser);
    };
  });
};

function generateIdManager(idBrand, idStore, callback){
  //---NOTE, ID MANAGER FORMAT, MA.XX(no.urut generate)---//
  var data = User.aggregate([
              { $match: {$and:[{id_brand:idBrand},{status:true}]}}, //cari sesuai id brand
              { $project: {_id: 0, store: 1}}, //struktur result
              { $unwind: "$store"}, //array store dipecah
              { $unwind: "$store.user"}, // array store.user dipecah
              { $match: {"store.id_store":idStore}}, // cari sesuai id store
              { $match: {"store.user.info_user.role.id_role":2}}, //cari sesuai id role
              { $project: { id_store: "$store.id_store" ,info_user: "$store.user.info_user"}} //struktur result
            ]);

  data.exec(function (err, result){
    if (err){
      console.log(err);
    } else {
      var idManager;
      var arrayNumber =[];
      var number;
      if (result == ""){
        idManager = 'MA.01';
      } else {
        //replace result id_user(read: 2 digit terakhir) ke arrayNumber(posisi msh blm urut dr kecil ke besar)
        for (var i=0; i<result.length; i++){
          arrayNumber.push(parseInt(result[i].info_user.id_user.substring(3,5)));
        };
        //sort id_user(read: 2 digit terakhir) dr bilangan kecil ke besar pada arrayNumber
        for (var i=0; i<arrayNumber.length;i++){
          for (var j=0; j<arrayNumber.length; j++){
            if (arrayNumber[j]>arrayNumber[j+1]){
              temp = arrayNumber[j];
              arrayNumber[j] = arrayNumber[j+1];
              arrayNumber[j+1] = temp;
            } 
          }
        }
        //cari nilai id_user(read: 2 digit terakhir) yg blm terpakai dr hasil urutan arrayNumber. 
        //Tujuan: biar gak mubadzir digit. 
        for (var i=0; i<arrayNumber.length; i++){
          if (i == 0 && arrayNumber[i] != 1){
            number = 1;
            break
          } else if (i == (arrayNumber.length-1)){
            number = i+2;
            break;
          } else {
            if (arrayNumber[i+1] != i+2){
              number = i+2;
              break;
            }
          };
        };
        //susun format id_user
        if (number <10) {
          idManager = 'MA.0'+number;
        } else if (number<100){
          idManager = 'MA.'+number;
        } else {
          idManager = 'max';
        };
      };
      callback(idManager);
    };
  });
};

function generateIdAdmin(idBrand, idStore, callback){
  //---NOTE, ID ADMIN FORMAT, AD.XXX(no.urut generate)---//
  var data = User.aggregate([
              { $match: {$and:[{id_brand:idBrand},{status:true}]}}, //cari sesuai id brand
              { $project: {_id: 0, store: 1}}, //struktur result
              { $unwind: "$store"}, //array store dipecah
              { $unwind: "$store.user"}, // array store.user dipecah
              { $match: {"store.id_store":idStore}}, // cari sesuai id store
              { $match: {"store.user.info_user.role.id_role":3}}, //cari sesuai id role
              { $project: { id_store: "$store.id_store" ,info_user: "$store.user.info_user"}} //struktur result
            ]);

  data.exec(function (err, result){
    if (err){
      console.log(err);
    } else {
      var idAdmin;
      var arrayNumber =[];
      var number;
      if (result == ""){
        idAdmin = 'AD.001';
      } else {
        //replace result id_user(read: 3 digit terakhir) ke arrayNumber(posisi msh blm urut dr kecil ke besar)
        for (var i=0; i<result.length; i++){
          arrayNumber.push(parseInt(result[i].info_user.id_user.substring(3,6)));
        };
        //sort id_user(read: 3 digit terakhir) dr bilangan kecil ke besar pada arrayNumber
        for (var i=0; i<arrayNumber.length;i++){
          for (var j=0; j<arrayNumber.length; j++){
            if (arrayNumber[j]>arrayNumber[j+1]){
              temp = arrayNumber[j];
              arrayNumber[j] = arrayNumber[j+1];
              arrayNumber[j+1] = temp;
            } 
          }
        }
        //cari nilai id_user(read: 3 digit terakhir) yg blm terpakai dr hasil urutan arrayNumber. 
        //Tujuan: biar gak mubadzir digit. 
        for (var i=0; i<arrayNumber.length; i++){
          if (i == 0 && arrayNumber[i] != 1){
            number = 1;
            break
          } else if (i == (arrayNumber.length-1)){
            number = i+2;
            break;
          } else {
            if (arrayNumber[i+1] != i+2){
              number = i+2;
              break;
            }
          };
        };
        //susun format id_user
        if (number <10) {
          idAdmin = 'AD.00'+number;
        } else if (number<100){
          idAdmin = 'AD.0'+number;
        } else if (number<1000){
          idAdmin = 'AD.'+number;
        } else {
          idAdmin = 'max';
        };
      };
      callback(idAdmin);
    };
  });
};

function generateIdSupervisor(idBrand, idStore, callback){
  //---NOTE, ID SUPERVISOR FORMAT, SV.XXX(no.urut generate)---//
  var data = User.aggregate([
              { $match: {$and:[{id_brand:idBrand},{status:true}]}}, //cari sesuai id brand
              { $project: {_id: 0, store: 1}}, //struktur result
              { $unwind: "$store"}, //array store dipecah
              { $unwind: "$store.user"}, // array store.user dipecah
              { $match: {"store.id_store":idStore}}, // cari sesuai id store
              { $match: {"store.user.info_user.role.id_role":4}}, //cari sesuai id role
              { $project: { id_store: "$store.id_store" ,info_user: "$store.user.info_user"}} //struktur result
            ]);

  data.exec(function (err, result){
    if (err){
      console.log(err);
    } else {
      var idSupervisor;
      var arrayNumber =[];
      var number;
      if (result == ""){
        idSupervisor = 'SV.001';
      } else {
        //replace result id_user(read: 3 digit terakhir) ke arrayNumber(posisi msh blm urut dr kecil ke besar)
        for (var i=0; i<result.length; i++){
          arrayNumber.push(parseInt(result[i].info_user.id_user.substring(3,6)));
        };
        //sort id_user(read: 3 digit terakhir) dr bilangan kecil ke besar pada arrayNumber
        for (var i=0; i<arrayNumber.length;i++){
          for (var j=0; j<arrayNumber.length; j++){
            if (arrayNumber[j]>arrayNumber[j+1]){
              temp = arrayNumber[j];
              arrayNumber[j] = arrayNumber[j+1];
              arrayNumber[j+1] = temp;
            } 
          }
        }
        //cari nilai id_user(read: 3 digit terakhir) yg blm terpakai dr hasil urutan arrayNumber. 
        //Tujuan: biar gak mubadzir digit. 
        for (var i=0; i<arrayNumber.length; i++){
          if (i == 0 && arrayNumber[i] != 1){
            number = 1;
            break
          } else if (i == (arrayNumber.length-1)){
            number = i+2;
            break;
          } else {
            if (arrayNumber[i+1] != i+2){
              number = i+2;
              break;
            }
          };
        };
        //susun format id_user
        if (number <10) {
          idSupervisor = 'SV.00'+number;
        } else if (number<100){
          idSupervisor = 'SV.0'+number;
        } else if (number<1000){
          idSupervisor = 'SV.'+number;
        } else {
          idSupervisor = 'max';
        };
      };
      callback(idSupervisor);
    };
  });
};

function generateIdLogistic(idBrand, idStore, callback){
  //---NOTE, ID LOGISTIC FORMAT, LS.XXX(no.urut generate)---//
  var data = User.aggregate([
              { $match: {$and:[{id_brand:idBrand},{status:true}]}}, //cari sesuai id brand
              { $project: {_id: 0, store: 1}}, //struktur result
              { $unwind: "$store"}, //array store dipecah
              { $unwind: "$store.user"}, // array store.user dipecah
              { $match: {"store.id_store":idStore}}, // cari sesuai id store
              { $match: {"store.user.info_user.role.id_role":5}}, //cari sesuai id role
              { $project: { id_store: "$store.id_store" ,info_user: "$store.user.info_user"}} //struktur result
            ]);
  data.exec(function (err, result){
    if (err){
      console.log(err);
    } else {
      var idLogistic;
      var arrayNumber =[];
      var number;
      if (result == ""){
        idLogistic = 'LS.001';
      } else {
        //replace result id_user(read: 3 digit terakhir) ke arrayNumber(posisi msh blm urut dr kecil ke besar)
        for (var i=0; i<result.length; i++){
          arrayNumber.push(parseInt(result[i].info_user.id_user.substring(3,6)));
        };
        //sort id_user(read: 3 digit terakhir) dr bilangan kecil ke besar pada arrayNumber
        for (var i=0; i<arrayNumber.length;i++){
          for (var j=0; j<arrayNumber.length; j++){
            if (arrayNumber[j]>arrayNumber[j+1]){
              temp = arrayNumber[j];
              arrayNumber[j] = arrayNumber[j+1];
              arrayNumber[j+1] = temp;
            } 
          }
        }
        //cari nilai id_user(read: 3 digit terakhir) yg blm terpakai dr hasil urutan arrayNumber. 
        //Tujuan: biar gak mubadzir digit. 
        for (var i=0; i<arrayNumber.length; i++){
          if (i == 0 && arrayNumber[i] != 1){
            number = 1;
            break
          } else if (i == (arrayNumber.length-1)){
            number = i+2;
            break;
          } else {
            if (arrayNumber[i+1] != i+2){
              number = i+2;
              break;
            }
          };
        };
        //susun format id_user
        if (number <10) {
          idLogistic = 'LS.00'+number;
        } else if (number<100){
          idLogistic = 'LS.0'+number;
        } else if (number<1000){
          idLogistic = 'LS.'+number;
        } else {
          idLogistic = 'max';
        };
      };
      callback(idLogistic);
    };
  });
};

function generateIdKasir(idBrand, idStore, callback){
  //---NOTE, ID KASIR FORMAT, KS.XXX(no.urut generate)---//
  var data = User.aggregate([
              { $match: {$and:[{id_brand:idBrand},{status:true}]}}, //cari sesuai id brand
              { $project: {_id: 0, store: 1}}, //struktur result
              { $unwind: "$store"}, //array store dipecah
              { $unwind: "$store.user"}, // array store.user dipecah
              { $match: {"store.id_store":idStore}}, // cari sesuai id store
              { $match: {"store.user.info_user.role.id_role":6}}, //cari sesuai id role
              { $project: { id_store: "$store.id_store" ,info_user: "$store.user.info_user"}} //struktur result
            ]);

  data.exec(function (err, result){
    if (err){
      console.log(err);
    } else {
      var idKasir;
      var arrayNumber =[];
      var number;
      if (result == ""){
        idKasir = 'KS.001';
      } else {
        //replace result id_user(read: 3 digit terakhir) ke arrayNumber(posisi msh blm urut dr kecil ke besar)
        for (var i=0; i<result.length; i++){
          arrayNumber.push(parseInt(result[i].info_user.id_user.substring(3,6)));
        };
        //sort id_user(read: 3 digit terakhir) dr bilangan kecil ke besar pada arrayNumber
        for (var i=0; i<arrayNumber.length;i++){
          for (var j=0; j<arrayNumber.length; j++){
            if (arrayNumber[j]>arrayNumber[j+1]){
              temp = arrayNumber[j];
              arrayNumber[j] = arrayNumber[j+1];
              arrayNumber[j+1] = temp;
            } 
          }
        }
        //cari nilai id_user(read: 3 digit terakhir) yg blm terpakai dr hasil urutan arrayNumber. 
        //Tujuan: biar gak mubadzir digit. 
        for (var i=0; i<arrayNumber.length; i++){
          if (i == 0 && arrayNumber[i] != 1){
            number = 1;
            break
          } else if (i == (arrayNumber.length-1)){
            number = i+2;
            break;
          } else {
            if (arrayNumber[i+1] != i+2){
              number = i+2;
              break;
            }
          };
        };
        //susun format id_user
        if (number <10) {
          idKasir = 'KS.00'+number;
        } else if (number<100){
          idKasir = 'KS.0'+number;
        } else if (number<1000){
          idKasir = 'KS.'+number;
        } else {
          idKasir = 'max';
        };
      };
      callback(idKasir);
    };
  });
}

function generateIdServer(idBrand, idStore, callback){
  //---NOTE, ID SERVER FORMAT, SS.XXX(no.urut generate)---//
  var data = User.aggregate([
              { $match: {$and:[{id_brand:idBrand},{status:true}]}}, //cari sesuai id brand
              { $project: {_id: 0, store: 1}}, //struktur result
              { $unwind: "$store"}, //array store dipecah
              { $unwind: "$store.user"}, // array store.user dipecah
              { $match: {"store.id_store":idStore}}, // cari sesuai id store
              { $match: {"store.user.info_user.role.id_role":7}}, //cari sesuai id role
              { $project: { id_store: "$store.id_store" ,info_user: "$store.user.info_user"}} //struktur result
            ]);

  data.exec(function (err, result){
    if (err){
      console.log(err);
    } else {
      var idServer;
      var arrayNumber =[];
      var number;
      if (result == ""){
        idServer = 'SS.001';
      } else {
        //replace result id_user(read: 3 digit terakhir) ke arrayNumber(posisi msh blm urut dr kecil ke besar)
        for (var i=0; i<result.length; i++){
          arrayNumber.push(parseInt(result[i].info_user.id_user.substring(3,6)));
        };
        //sort id_user(read: 3 digit terakhir) dr bilangan kecil ke besar pada arrayNumber
        for (var i=0; i<arrayNumber.length;i++){
          for (var j=0; j<arrayNumber.length; j++){
            if (arrayNumber[j]>arrayNumber[j+1]){
              temp = arrayNumber[j];
              arrayNumber[j] = arrayNumber[j+1];
              arrayNumber[j+1] = temp;
            } 
          }
        }
        //cari nilai id_user(read: 3 digit terakhir) yg blm terpakai dr hasil urutan arrayNumber. 
        //Tujuan: biar gak mubadzir digit. 
        for (var i=0; i<arrayNumber.length; i++){
          if (i == 0 && arrayNumber[i] != 1){
            number = 1;
            break
          } else if (i == (arrayNumber.length-1)){
            number = i+2;
            break;
          } else {
            if (arrayNumber[i+1] != i+2){
              number = i+2;
              break;
            }
          };
        };
        //susun format id_user
        if (number <10) {
          idServer = 'SS.00'+number;
        } else if (number<100){
          idServer = 'SS.0'+number;
        } else if (number<1000){
          idServer = 'SS.'+number;
        } else {
          idServer = 'max';
        };
      };
      callback(idServer);
    };
  });
}

function findUser(email, callback){
  var lB,lS,lU;
  var hasil =[];
  var available = false; 
  var data = User.find({});
  data.exec(function(err, result){
    if (err){
      console.log(err);
    } else {
      console.log(result.length);
      lB = result.length;
      if (lB == 0){
        callback (null);
      } else {
        for (var i=0; i<lB; i++){
          if (available == true){
            break;
          } else {
            lS = result[i].store.length;
            for (var j=0; j<lS; j++){
              if (available == true){
                break;
              } else {
                lU = result[i].store[j].user.length;
                for (var k=0; k<lU; k++){
                  if (available == true){
                    break;
                  } else {
                    if (result[i].store[j].user[k].info_user.email == email && result[i].store[j].user[k].info_user.deleted == false){
                      available = true;
                      if (result[i].status == false){
                        callback(false);
                      } else {
                        var field = [];
                          field[0] = "id_brand";
                          field[1] = "id_store";
                          field[2] = "id_user";
                          field[3] = "email";
                          field[4] = "id_role";
                          field[5] = "iBrand";
                          field[6] = "iStore";
                          field[7] = "iUser";
                          field[8] = "username";
                          field[9] = "path_image";
                          field[10] = "phone"
                        var value = [];
                          value[0] = result[i].id_brand;
                          value[1] = result[i].store[j].id_store;
                          value[2] = result[i].store[j].user[k].info_user.id_user;
                          value[3] = result[i].store[j].user[k].info_user.email;
                          value[4] = result[i].store[j].user[k].info_user.role.id_role;
                          value[5] = i;
                          value[6] = j;
                          value[7] = k;
                          value[8] = result[i].store[j].user[k].info_user.username;
                          value[9] = result[i].store[j].user[k].info_user.path_image;
                          value[10] = result[i].store[j].user[k].info_user.phone;
                        var obj = {};
                        for (var x=0; x<11; x++){
                          obj[field[x]] = value[x];
                        };
                        getDataBrand(result[i].id_brand, result[i].store[j].id_store, function(hasil){
                          var field = [];
                            field[11] = "brand";
                            field[12] = "store";
                          var value = [];
                            value[11] = hasil[0];
                            value[12] = hasil[1];
                          obj[field[11]] = value[11]; 
                          obj[field[12]] = value[12]; 
                          callback(obj);
                        });
                      };
                    } else if (i == lB-1 && j == lS-1 && k == lU-1 && available == false){
                      callback(null);
                    };
                  };
                };
              };
            };
          };
        };
      };
    };
  });
};

function cekUsernameAdd(idBrand, username, callback){
  var data = User.findOne({id_brand: idBrand});
  data.exec(function(err, result){
    console.log(result.store);
    var available = false;
    for (var i=0; i<result.store.length; i++){
      for(var j=0; j<result.store[i].user.length; j++){
        var userResult = result.store[i].user[j].info_user.username.toLowerCase();
        var userInput = username.toLowerCase();
        if (userResult == userInput && result.store[i].user[j].info_user.deleted == false){
          available = true;
          callback('not null');
          break;
        };
        if (i == result.store.length-1 && j == result.store[i].user.length-1 && available == false){
          callback('null');
        };
      };
    };
  });
};

function cekUsernameUpdate(idBrand, iStore, iUser, username, callback){
  var data = User.findOne({id_brand: idBrand});
  data.exec(function(err, result){
    if(err){
      console.log(err);
    } else {
      var available = false;
      for (var i=0; i<result.store.length; i++){
        for(var j=0; j<result.store[i].user.length; j++){
          var userResult = result.store[i].user[j].info_user.username.toLowerCase();
          var userInput = username.toLowerCase();
          if (userResult == userInput && result.store[i].user[j].info_user.deleted == false){
            available = true;
            if(i == iStore && j == iUser){
              callback('null');
              break;
            } else {
              callback('not null');
              break;
            };
          };
          if (i == result.store.length-1 && j == result.store[i].user.length-1 && available == false){
            callback('null');
            break;
          };
        };
      };
    };
  });
};

function getDataBrand(idBrand, idStore, callback){
  var data = Brand.findOne({id_brand: idBrand});
  data.exec(function(err, result){
    if (err){
      console.log(err);
    } else {
      var field = [];
        field[0] = "brand_name";
        field[1] = "brand_province_id"
        field[2] = "brand_province_name";
        field[3] = "brand.city_id";
        field[4] = "brand_city_name";
        field[5] = "brand_address_detail";
        field[6] = "brand_path_image";
        field[7] = "brand_social_media_ig";
        field[8] = "brand_social_media_fb";
        field[9] = "brand_contact_email";
        field[10] = "brand_contact_phone";
      var value = [];
        value[0] = result.name_brand; 
        value[1] = result.address_brand.province_id;
        value[2] = result.address_brand.province_name;
        value[3] = result.address_brand.city_id;
        value[4] = result.address_brand.city_name;
        value[5] = result.address_brand.detail;
        value[6] = result.path_image;
        value[7] = result.social_media.ig;
        value[8] = result.social_media.fb;
        value[9] = result.contact.email;
        value[10] = result.contact.phone;
      var obj = {};
      for (var x=0; x<11; x++){
        obj[field[x]] = value[x];
      };
      getDataStore(result.store, idStore, function(hasil){
        var arr = [];
        arr.push(obj);
        arr.push(hasil);
        callback(arr);
      });
    };
  });
};

function getDataStore(store, idStore, callback){
  var available = false;
  for (var i=0; i<store.length; i++){
    if(store[i].id_store == idStore){
      var field = [];
          field[0] = "store_name";
          field[1] = "store_province_name";
          field[2] = "store_city_name";
          field[3] = "store_address_detail";
          field[4] = "store_path_image";
          field[5] = "store_contact_email";
          field[6] = "store_contact_phone";
      var value = [];
        value[0] = store[i].name_store; 
        value[1] = store[i].address_store.province_name;
        value[2] = store[i].address_store.city_name;
        value[3] = store[i].address_store.detail;
        value[4] = store[i].path_image;
        value[5] = store[i].contact_store.email_store;
        value[6] = store[i].contact_store.phone_store;
      var obj = {};
      for (var x=0; x<7; x++){
        obj[field[x]] = value[x];
      };
      available = true;
      callback(obj);
      break;
    };
    if (i == store.length-1 && available == false){
      callback(null);
    }
  };
};