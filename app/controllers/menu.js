var cors = require('cors');
var express = require('express'),
  app = express();
  router = express.Router(),
  mongoose = require('mongoose'),
  Menu = mongoose.model('Menu');
  BahanBaku = mongoose.model('BahanBaku');

module.exports = function (app) {
  app.use(cors());
  app.use('/v1', router);
};



// =====================
//-------------
// Menu-Add Menu
router.post('/menu', function(req, res, next){
	findNestedMenu(req.body.id_brand, req.body.store.id_store, req.body.store.menu.id_menu, req.body.store.menu.detail_menu.id_bahan_baku, function(result){
		if (result[0] == null ){ //brand null
			var idMenu = req.body.store.id_store.substring(6,10) +'.000001';
			//input brand, store, menu, detail_menu on Collection Menu
			var input = new Menu ({ id_brand: req.body.id_brand,
									store:[{
										id_store: req.body.store.id_store,
										menu:[{
											id_menu: idMenu,
											name_menu: req.body.store.menu.name_menu,
											price: req.body.store.menu.price,
											detail_menu:[{
												id_bahan_baku: req.body.store.menu.detail_menu.id_bahan_baku,
												name_bahan_baku: req.body.store.menu.detail_menu.name_bahan_baku, 
												jumlah: req.body.store.menu.detail_menu.jumlah, 
												satuan:{
													id_satuan: req.body.store.menu.detail_menu.satuan.id_satuan,
													name_satuan: req.body.store.menu.detail_menu.satuan.name_satuan
												}
											}],
											kategori:{
												id_kategori_menu: req.body.store.menu.kategori.id_kategori_menu,
												name_kategori_menu: req.body.store.menu.kategori.name_kategori_menu
											}
										}]
									}]
								 });
			input.save(function(err, result){
				if (err){
					console.log(err);
				} else {
					findMenu(req.body.id_brand, req.body.store.id_store, idMenu, function(result2){
						if(result2 == 0){
							res.status(200).json({status:'Not Success Update Menu', id_menu: idMenu });
						} else {
							res.status(200).json({status:'Success Update Menu', id_menu: idMenu});
						};
					});
				};
			});
		} else {
			if (result[1] == null){ //store null
				var idMenu = req.body.store.id_store.substring(6,10) +'.000001';
				//push store by id_brand, input menu, detail_menu on Collection Menu
				var dataold = {id_brand: req.body.id_brand};
				var datanew = {$push:{store:{ id_store: req.body.store.id_store, 
											  menu:[{
													id_menu: idMenu,
													name_menu: req.body.store.menu.name_menu,
													price: req.body.store.menu.price,
													detail_menu:[{
														id_bahan_baku: req.body.store.menu.detail_menu.id_bahan_baku,
														name_bahan_baku: req.body.store.menu.detail_menu.name_bahan_baku, 
														jumlah: req.body.store.menu.detail_menu.jumlah, 
														satuan:{
															id_satuan: req.body.store.menu.detail_menu.satuan.id_satuan,
															name_satuan: req.body.store.menu.detail_menu.satuan.name_satuan
														}
													}],
													kategori:{
														id_kategori_menu: req.body.store.menu.kategori.id_kategori_menu,
														name_kategori_menu: req.body.store.menu.kategori.name_kategori_menu
													}
											  }]
											}}};
				var options = {};
				Menu.update(dataold, datanew, options, callback);
				function callback (err){
					if (err){
						console.log(err);
					} else {
						findMenu(req.body.id_brand, req.body.store.id_store, idMenu, function(result2){
							if(result2 == 0){
								res.status(200).json({status:'Not Success Update Menu', id_menu: idMenu});
							} else {
								res.status(200).json({status:'Success Update Menu', id_menu: idMenu});
							};
						});
					};
				};
			} else {
				findNameMenu(req.body.id_brand, req.body.store.id_store, req.body.store.menu.id_menu, req.body.store.menu.name_menu, function(nameResult){
					if (nameResult == 0){
						res.status(409).json({status:'Nama Menu Sudah digunakan'});
					} else {
						if (result[2] == null){ //menu null
							//push menu by id_brand and id_store, input detail_menu on Collection Menu
							createIdMenu(result[3], req.body.store.id_store, function(hasil){
								if (hasil != 'maxMenu'){
									var fSat = [];
										fSat[0] = "id_satuan";
										fSat[1] = "name_satuan";
									var vSat = [];
										vSat[0] =  req.body.store.menu.detail_menu.satuan.id_satuan;
										vSat[1] =  req.body.store.menu.detail_menu.satuan.name_satuan;
									var oSat = {};
										oSat[fSat[0]] = vSat[0];
										oSat[fSat[1]] = vSat[1];
									var fDm = [];
										fDm[0] = "id_bahan_baku";
										fDm[1] = "name_bahan_baku";
										fDm[2] = "jumlah";
										fDm[3] = "satuan";
									var vDm = [];
										vDm[0] = req.body.store.menu.detail_menu.id_bahan_baku;
										vDm[1] = req.body.store.menu.detail_menu.name_bahan_baku;
										vDm[2] = req.body.store.menu.detail_menu.jumlah;
										vDm[3] = oSat;
									var oDm = {};
										oDm[fDm[0]] = vDm[0];
										oDm[fDm[1]] = vDm[1];
										oDm[fDm[2]] = vDm[2];
										oDm[fDm[3]] = vDm[3];
									var arrDm = [];
										arrDm.push(oDm);
									var fKm = [];
										fKm[0] = "id_kategori_menu";
										fKm[1] = "name_kategori_menu";
									var vKm = [];
										vKm[0] = req.body.store.menu.kategori.id_kategori_menu;
										vKm[1] = req.body.store.menu.kategori.name_kategori_menu;
									var oKm = {};
										oKm[fKm[0]] = vKm[0];
										oKm[fKm[1]] = vKm[1];
									var fM = [];
										fM[0] = "id_menu";
										fM[1] = "name_menu";
										fM[2] = "price";
										fM[3] = "detail_menu";
										fM[4] = "kategori";
									var vM = [];
										vM[0] = hasil;
										vM[1] = req.body.store.menu.name_menu;
										vM[2] = req.body.store.menu.price;
										vM[3] = arrDm;
										vM[4] = oKm;
									var oM = {};
										oM[fM[0]] = vM[0];
										oM[fM[1]] = vM[1];
										oM[fM[2]] = vM[2];
										oM[fM[3]] = vM[3];
										oM[fM[4]] = vM[4];
									var fiSto = "store."+result[1]+".menu";
									var viSto = oM;
									var obj = {};
										obj[fiSto] = viSto;

									var dataold = {id_brand: req.body.id_brand};
									var datanew = {$push: obj};
									var options = {};
									Menu.update(dataold, datanew, options, callback);
									function callback(err){
										if (err){
											console.log(err);
										} else {
											findMenu(req.body.id_brand, req.body.store.id_store, hasil, function(result2){
												if(result2 == 0){
													res.status(200).json({status:'Not Success Update Menu', id_menu: hasil});
												} else {
													res.status(200).json({status:'Success Update Menu', id_menu: hasil});
												};
											});
										};
									};
								} else {
									res.status(403).json({status:'Menu Max'});
								};
							});
						} else { 
							//update Menu(name,price, kategori menu)
							var fKm = [];
								fKm[0] = "id_kategori_menu";
								fKm[1] = "name_kategori_menu";
							var vKm = [];
								vKm[0] = req.body.store.menu.kategori.id_kategori_menu;
								vKm[1] = req.body.store.menu.kategori.name_kategori_menu;
							var oKm = {};
								oKm[fKm[0]] = vKm[0];
								oKm[fKm[1]] = vKm[1];
							var f = [];
								f[0] = "store."+result[1]+".menu."+result[2]+".name_menu";
							    f[1] = "store."+result[1]+".menu."+result[2]+".price";
							    f[2] = "store."+result[1]+".menu."+result[2]+".kategori"; 
							var v = [];
							    v[0] = req.body.store.menu.name_menu;
							    v[1] = req.body.store.menu.price;
							    v[2] = oKm;
							var objM = {};
								objM[f[0]] = v[0];
								objM[f[1]] = v[1];
								objM[f[2]] = v[2];
							var dataold = {id_brand: req.body.id_brand};
							var datanew = objM;
							var options = {};
							Menu.update(dataold, datanew, options, callback);
							function callback (err){
								if (err){
									console.log(err);
								} else {
									if (result[3] == null){ //detail_menu null
										//push detail_menu by id_brand and id_store and id_menu on Collection Menu  
										var fSat = [];
											fSat[0] = "id_satuan";
											fSat[1] = "name_satuan";
										var vSat = [];
											vSat[0] =  req.body.store.menu.detail_menu.satuan.id_satuan;
											vSat[1] =  req.body.store.menu.detail_menu.satuan.name_satuan;
										var oSat = {};
											oSat[fSat[0]] = vSat[0];
											oSat[fSat[1]] = vSat[1];
										var fDm = [];
											fDm[0] = "id_bahan_baku";
											fDm[1] = "name_bahan_baku";
											fDm[2] = "jumlah";
											fDm[3] = "satuan";
										var vDm = [];
											vDm[0] = req.body.store.menu.detail_menu.id_bahan_baku;
											vDm[1] = req.body.store.menu.detail_menu.name_bahan_baku;
											vDm[2] = req.body.store.menu.detail_menu.jumlah;
											vDm[3] = oSat;
										var oDm = {};
											oDm[fDm[0]] = vDm[0];
											oDm[fDm[1]] = vDm[1];
											oDm[fDm[2]] = vDm[2];
											oDm[fDm[3]] = vDm[3];
										var fiDm = "store."+result[1]+".menu."+result[2]+".detail_menu";
										var viDm = oDm;
										var obj = {};
											obj[fiDm] = viDm;

										var dataold = {id_brand: req.body.id_brand};
										var datanew = {$push: obj};
										var options = {};
										Menu.update(dataold, datanew, options, callback); 
										function callback(err){
											if (err){
												console.log(err);
											} else {
												findMenu(req.body.id_brand, req.body.store.id_store, req.body.store.menu.id_menu,  function(result2){
													if(result2 == 0){
														res.status(200).json({status:'Not Success Update Menu'});
													} else {
														res.status(200).json({status:'Success Update Menu'});
													};
												});
											};
										};
									} else {
										//update detail_menu on Collection menu
										var fiDm = "store."+result[1]+".menu."+result[2]+".detail_menu."+result[3]+".jumlah";
										var viDm = req.body.store.menu.detail_menu.jumlah;
										var obj = {};
											obj[fiDm] = viDm;
										var dataold = {id_brand: req.body.id_brand};
										var datanew = obj;
										var options = {};
										Menu.update(dataold, datanew, options, callback); 
										function callback(err){
											if (err){
												console.log(err);
											} else {
												findMenu(req.body.id_brand, req.body.store.id_store, req.body.store.menu.id_menu,  function(result2){
													if(result2 == 0){
														res.status(200).json({status:'Not Success Update Menu'});
													} else {
														res.status(200).json({status:'Success Update Menu'});
													};
												});
											};
										};
									};
								};
							};
						};
					};
				});
			};
		};
	});
});

//-------------
// Menu-Edit Gambar Menu
router.put('/menu/image', function(req, res, next){
	findNestedMenu(req.body.id_brand, req.body.id_store, req.body.id_menu, "", function(result){
		var f = "store."+result[1]+".menu."+result[2]+".path_image";
		var v = req.body.path_image;
		var obj = {};
			obj[f] = v;
		var dataold = {id_brand: req.body.id_brand};
		var datanew = obj
		var options = {};
		Menu.update(dataold, datanew, options, callback);
		function callback(err){
			if (err){
				console.log(err);
			} else {
				res.status(200).json({status:'Success'});
			};
		};
	});
});

//-------------
// Menu-Delete Menu
router.put('/menu/delete/menu', function(req, res, next){
	findNestedMenu(req.body.id_brand, req.body.id_store, req.body.id_menu, "", function(result){
		var f = "store."+result[1]+".menu."+result[2]+".deleted";
		var v = true;
		var obj = {};
			obj[f] = v;
		var dataold = {id_brand: req.body.id_brand};
		var datanew = obj;
		var options = {};
		Menu.update(dataold, datanew, options, callback);
		function callback(err){
			if (err){
				console.log(err);
			} else {
				res.status(200).json({status:'Success'});
			};
		};
	});
});

//-------------
// Menu-Delete Detail Menu
router.put('/menu/delete/detail', function(req, res, next){
	findNestedMenu(req.body.id_brand, req.body.id_store, req.body.id_menu, req.body.id_bahan_baku, function(result){
		var f = "store."+result[1]+".menu."+result[2]+".detail_menu."+result[3]+".deleted";
		var v = true;
		var obj = {};
			obj[f] = v;
		var dataold = {id_brand: req.body.id_brand};
		var datanew = obj;
		var options = {};
		Menu.update(dataold, datanew, options, callback);
		function callback(err){
			if (err){
				console.log(err);
			} else {
				findMenu(req.body.id_brand, req.body.id_store, req.body.id_menu, function(result2){
					console.log(result2);
					if(result2 == 0){
						res.json({status:'Not Success'});
					} else {
						res.status(200).json({status:'Success'});
					};
				});
			};
		};
	});
});

//-------------
// Menu-Show Menu
router.get('/menu/:idBrand/:idStore', function(req, res, next){
	var data = Menu.findOne({id_brand: req.params.idBrand});
	data.exec(function(err, result){
		if (err){
			console.log(err);
		} else {
			if (result == null){
				res.status(404).json({status:'Not Found'});
			} else {
				var lS = result.store.length;
				for (var i=0; i<lS; i++){
					if (result.store[i].id_store == req.params.idStore){
						res.status(200).json({result: result.store[i].menu});
						break;
					};
					if (i == lS-1){
						res.status(404).json({status:'Not Found'});
					};
				};
			};
		};
	});
});
// =====================



function createIdMenu(dataMenu, idStore, callback){
	//format idMenu : idStore.No_urut (XXX.XXXXXX)
	var idMenu;
	var number;
	var arrayNumber = [];
	if (dataMenu == 0){
		idMenu = idStore.substring(6,10)+'.000001';
		callback(idMenu);
	} else {
		if (dataMenu < 9){
			idMenu = idStore.substring(6,10)+'.00000'+(dataMenu+1);
		} else if (dataMenu < 99 ){
			idMenu = idStore.substring(6,10)+'.0000'+(dataMenu+1);
		} else if (dataMenu < 999){
			idMenu = idStore.substring(6,10)+'.000'+(dataMenu+1);
		} else if (dataMenu < 9999){
			idMenu = idStore.substring(6,10)+'.00'+(dataMenu+1);
		} else if (dataMenu < 99999){
			idMenu = idStore.substring(6,10)+'.0'+(dataMenu+1);
		} else if (dataMenu < 999999){
			idMenu = idStore.substring(6,10)+"."+(dataMenu+1);
		} else {
			idMenu = 'maxMenu';
		};
		callback(idMenu);
	};
};

function findNestedMenu(idBrand, idStore, idMenu, idBahanBaku, callback){
	var hasil = [];
	var availableStore = false;
	var availableMenu = false;
	var availableBahanBaku = false;
	var data = Menu.findOne({id_brand: idBrand}); 
	data.exec(function(err, result){
		if (err){
			console.log(err);
		} else {
			if (result == null){
				hasil.push(null) //0, brand null
				callback(hasil);
			} else {
				hasil.push('not null') //0, brand not null
				var lS = result.store.length;
				for (var i = 0; i<lS; i++){
					if (availableBahanBaku == true){
						break;
					} else {
						if (result.store[i].id_store == idStore){
							availableStore = true;
							hasil.push(i); //1, store not null
							var lM = result.store[i].menu.length;
							for (var j=0; j<lM; j++){
								if (availableBahanBaku == true){
									break;
								} else {
									if (result.store[i].menu[j].id_menu == idMenu && result.store[i].menu[j].deleted == false){
										availableMenu = true;
										hasil.push(j); //2, menu not null
										var lBB = result.store[i].menu[j].detail_menu.length;
										for (var k = 0; k<lBB; k++){
											if (result.store[i].menu[j].detail_menu[k].id_bahan_baku == idBahanBaku && result.store[i].menu[j].detail_menu[k].deleted == false){
												availableBahanBaku = true;
												hasil.push(k); //3, detail_menu(id_bahan_baku) not null
												break;
											};
											if (k == lBB-1 && availableBahanBaku == false){
												hasil.push(null); //3, detail_menu(id_bahan_baku) null
											};
										};
									};
									if (j == lM-1 && availableMenu == false){
										hasil.push(null); //2, menu null
										hasil.push(result.store[i].menu.length) //3, data menu
									};
								};
							};
						};
						if (i == lS-1 && availableStore == false){
							hasil.push(null); //1, store null
						};
					};
				};
				callback(hasil);
			};
		};
	});
};

function findNameMenu(idBrand, idStore, idMenu, nameMenu, callback){
	var data = Menu.findOne({id_brand: idBrand});
	data.exec(function(err, result){
		if(err){
			console.log(err);
		} else {
			if(result == null){
				callback(1);
			} else {
				var lS = result.store.length;
				var aS = false;
				for(var i=0; i<lS; i++){
					if (result.store[i].id_store == idStore){
						aS = true;
						var lM = result.store[i].menu.length;
						var aM = false;
						for(var j=0; j<lM; j++){ //looping utk detect input/update menu
							if (result.store[i].menu[j].id_menu == idMenu && result.store[i].menu[j].deleted == false){ //update name menu
								aM = true;
								var aName = false;
								for (var k=0; k<lM; k++){ //looping name menu
									var menuResult = result.store[i].menu[k].name_menu.toLowerCase();
									var menuInput = nameMenu.toLowerCase();
									if ((menuResult == menuInput) && (result.store[i].menu[k].id_menu != idMenu) && (result.store[i].menu[k].deleted == false)){ //nama menu terdeteksi ada
										aName = true;
										callback(0);
										break;
									};
									if (k == lM-1 && aName == false){ //nama menu blm ada
										callback(1); //lanjut
									};
								};
								break;
							};
							if (j == lM-1 && aM == false){ //input name menu
								var aName = false;
								for (var k=0; k<lM; k++){ //looping name menu
									var menuResult = result.store[i].menu[k].name_menu.toLowerCase();
									var menuInput = nameMenu.toLowerCase();
									if ((menuResult == menuInput) && (result.store[i].menu[k].deleted == false)){ //nama menu terdeteksi ada
										aName = true
										callback(0); //nama menu sdh digunakan
										break;
									};
									if (k == lM-1 && aName == false){ //nama menu blm ada
										callback(1); //lanjut
									};
								};
							}; 
						};
					};
					if (i == lS-1 && aS == false){ 
						callback(1);
					};
				};
			};
		};
	});
};

function findMenu (idBrand, idStore, idMenu, callback){
	//1, success;
	//2, not success;
	var hasil;
	var data = Menu.findOne({id_brand: idBrand});
	data.exec(function(err, result){
		if (err){
			console.log(err);
		} else {
			if (result == null || result == ""){
				callback(0);
			} else {
				var availableMenu = false;
				var availableStore = false;
				var lS = result.store.length;
				for (var i =0; i<lS; i++){
					if (result.store[i].id_store == idStore){
						availableStore = true;
						var lM = result.store[i].menu.length;
						for (j=0; j<lM; j++){
								if (result.store[i].menu[j].id_menu == idMenu && result.store[i].menu[j].deleted == false){
									availableMenu = true;
									findBahanBaku(idBrand, idStore, idMenu, result.store[i].menu[j].detail_menu, i, j, function(result2){											
										if (result2 == 1){
											callback(1);
										} else {
											callback(0);
										};
									});
									break;
								};
								if (j == lM-1 && availableMenu == false){
									callback(0);
								};
						};
					};
					if (i == lS-1 && availableStore == false){
						callback(0);
					};
				};
			};
		};
	});
};

function findBahanBaku (idBrand, idStore, idMenu, detMenu, iStore, iMenu, callback){
	var data = BahanBaku.findOne({id_brand: idBrand});
	data.exec(function(err, result){
		if (err){
			console.log(err);
		} else {
			console.log(result);
			if (result == null || result == ""){
				callback(0);
			} else {
				var availableStore = false;
				var lS = result.store.length;
				for (var i=0; i<lS; i++){
					if (result.store[i].id_store == idStore){
						availableStore = true;
						updateStokMenu(result.store[i].bahan_baku, detMenu, idBrand, idStore, idMenu, iStore, iMenu, function(result2){
							if(result2 == 1){
								callback(1);
							} else {
								callback(0)
							}
						});
						break;
					};
					if (i == lS-1 && availableStore == false){
						callback(0);
					};
				};
			};
		};
	});
};

function updateStokMenu (bahanBaku, detMenu, idBrand, idStore, idMenu, iStore, iMenu, callback){
	var a = [];
	var min;
	if (detMenu.length == 0){
		var field = "store."+iStore+".menu."+iMenu+".stok";
		var value = 0;
		var obj ={};
			obj[field] = value;
		var dataold = {id_brand: idBrand};
		var datanew = obj;
		var options = {};
		Menu.update(dataold, datanew, options, callback);
		function callback (err){
			if (err){
				console.log(err);
			} else {
				callback(1);
			};
		};
	} else {
		for (var i=0; i<detMenu.length; i++){
			for (var j=0; j<bahanBaku.length; j++){
				if (detMenu[i].id_bahan_baku == bahanBaku[j].id_bahan_baku && bahanBaku[j].deleted == false && detMenu[i].deleted == false){
					var  b = bahanBaku[j].stok/detMenu[i].jumlah;
					a.push(Math.floor(b));
				};
			};
			if (i == detMenu.length-1){
				if (a.length == 0){
					var field = "store."+iStore+".menu."+iMenu+".stok";
					var value = 0;
					var obj ={};
						obj[field] = value;
					var dataold = {id_brand: idBrand};
					var datanew = obj;
					var options = {};
					Menu.update(dataold, datanew, options, success);
					function success (err){
						if (err){
							console.log(err);
						} else {
							callback(1);
						};
					};
				} else {
					if (a.length == 1){
						min = a[0];
					} else if (a.length == 2){
						if (a[0] <= a[1]){
							min = a[0]
						} else {
							min = a[1];
						}
					} else {
						for (var k=0; k<a.length; k++){
							if (k == 0){
								min = a[k];
							} else {
								if (a[k]<= min){
									min = a[k]
								};
							};
						};
					};
					if (iMenu != null){
						var field = "store."+iStore+".menu."+iMenu+".stok";
						var value = min;
						var obj ={};
							obj[field] = value;
						var dataold = {id_brand: idBrand};
						var datanew = obj;
						var options = {};
						Menu.update(dataold, datanew, options, success);
						function success (err){
							if (err){
								console.log(err);
							} else {
								callback(1);
							};
						};
					} else {
						var data = Menu.findOne({id_brand:idBrand});
						data.exec(function(err, result){
							if (result ==null || result == ""){
								callback(0);
							} else {
								var lS = result.store.length;
								var availableStore = false;
								for (var m = 0; m<lS; m++){
									if (result.store[m].id_store == idStore){
										availableStore = true;
										var lM = result.store[m].menu.length;
										for (var n=0; n<lM; n++){
											if (result.store[m].menu[n].id_menu == idMenu){
												var field = "store."+iStore+".menu."+n+".stok";
												var value = min;
												var obj ={};
													obj[field] = value;
												var dataold = {id_brand: idBrand};
												var datanew = obj;
												var options = {};
												Menu.update(dataold, datanew, options, success);
												function success (err){
													if (err){
														console.log(err);
													} else {
														callback(1);
													};
												};
												break;
											};
										};
									};
									if (m == lS-1 && availableStore == false){
										callback(0);
									};
								};
							};
						});
					};
				};
			};
		};
	};
};
