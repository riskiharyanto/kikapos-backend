var cors = require('cors');
var express = require('express'),
  app = express();
  router = express.Router(),
  mongoose = require('mongoose'),
  BahanBaku = mongoose.model('BahanBaku'),
  Menu = mongoose.model('Menu');

module.exports = function (app) {
  app.use(cors());
  app.use('/v1', router);
};



// =====================
//-------------
// BahanBaku-Add 
router.post('/bahanbaku', function(req, res, next){
	createIdBahanBaku(req.body.id_brand, req.body.id_store, req.body.bahan_baku.name_bahan_baku, function(result){
		if (result[0] == 'null'){ //cek idBrand null or not null
			var input = new BahanBaku ({id_brand: req.body.id_brand,
										store:  { id_store: req.body.id_store,
												  bahan_baku:  { id_bahan_baku: result[1],
												 		  		name_bahan_baku: req.body.bahan_baku.name_bahan_baku,
												 		  		stok: req.body.bahan_baku.stok,
												 		  		satuan: { 	id_satuan: req.body.bahan_baku.satuan.id_satuan,
												 		  					name_satuan: req.body.bahan_baku.satuan.name_satuan
												 		  				}
												 		  		}
												 }
										});
			input.save(function(err){
				if(err){
					console.log(err);
				} else {
					res.status(200).json({status:'Input Bahan Baku Success'});
				};
			});
		} else {
			if (result[1] == 'null'){ //cek idStore null or not null
				var dataold = {id_brand:req.body.id_brand};
				var datanew = {$push:{store:{id_store:req.body.id_store,
											 bahan_baku:{ id_bahan_baku: result[2],
											 			  name_bahan_baku: req.body.bahan_baku.name_bahan_baku,
											 			  stok: req.body.bahan_baku.stok,
											 			  satuan:{ id_satuan: req.body.bahan_baku.satuan.id_satuan,
											 			  		   name_satuan: req.body.bahan_baku.satuan.name_satuan 
											 			  }}}}};
				var options = {};
				BahanBaku.update(dataold, datanew, options, callback);
				function callback(err){
					if (err){
						console.log(err);
					} else {
						res.status(200).json({status:'Input Bahan Baku Success'});
					};
				};
			} else {
				if (result[2] == 'not null'){ //cek nameBahanBaku null or not null
					res.status(409).json({status:'Conflict Name Bahan Baku'});
				} else {
					if (result[3] != 'maxBahanBaku'){
						var dataold = {id_brand:req.body.id_brand, 'store.id_store': req.body.id_store};
						var datanew = {$push: { 'store.$.bahan_baku':{ id_bahan_baku: result[3],
																			 name_bahan_baku: req.body.bahan_baku.name_bahan_baku,
																			 stok: req.body.bahan_baku.stok,
																			 satuan:{ id_satuan: req.body.bahan_baku.satuan.id_satuan,
																			 		  name_satuan: req.body.bahan_baku.satuan.name_satuan
																			 		}}}};
						var options = {};
						BahanBaku.update(dataold, datanew, options, callback);
						function callback(err){
							if(err){
								console.log(err);
							} else {
								res.status(200).json({status:'Input Bahan Baku Success'});
							};
						};
					} else {
						res.status(403).json({status:'Max Bahan Baku'});
					};
				};
			};
		};
	});
});

//-------------
// BahanBaku-Edit Name and Satuan
router.put('/bahanbaku', function(req, res, next){
	findBahanBaku(req.body.id_brand, req.body.id_store, req.body.bahan_baku.id_bahan_baku, req.body.bahan_baku.name_bahan_baku, function(result){
		console.log(result);
		if (result[0] == 'null'){
			console.log('Brand Not Found');
			res.status(404).json({status:'Brand Not Found'});
		} else {
			if(result[1] == 'null'){
				console.log('Store Not Found');
				res.status(404).json({status:'Store Not Found'});
			}else {
				if (result[2] == 'null'){
					console.log('Bahan Baku Not Found');
					res.status(404).json({status:'Bahan Baku Not Found'});
				} else {
					if (result[3] == 'not null'){
						console.log('Conflict Name Bahan Baku');
						res.status(409).json({status:'Conflict Name Bahan Baku'});
					} else {
						var field = [];
						    field[0] = "store."+result[1]+".bahan_baku."+result[2]+".name_bahan_baku";
						    field[1] = "store."+result[1]+".bahan_baku."+result[2]+".satuan.id_satuan";
						    field[2] = "store."+result[1]+".bahan_baku."+result[2]+".satuan.name_satuan"; 
						var value = [];
						    value[0] = req.body.bahan_baku.name_bahan_baku;
						    value[1] = req.body.bahan_baku.satuan.id_satuan;
						    value[2] = req.body.bahan_baku.satuan.name_satuan;
						var obj = {};
						    obj[field[0]] = value[0];
						    obj[field[1]] = value[1];
						    obj[field[2]] = value[2];

						var dataold = {id_brand: req.body.id_brand};
						var datanew = {$set: obj};
						var options = {};
						BahanBaku.update(dataold, datanew, options, callback);
						function callback(err){
							if(err){
								console.log(err);
							} else {
								res.status(200).json({status:'Update Success'});
							};
						};
					};
				};
			};
		};
	});
});

//-------------
// BahanBaku-Delete
router.put('/delete/bahanbaku', function(req, res, next){
	findBahanBaku(req.body.id_brand, req.body.id_store, req.body.id_bahan_baku, req.body.name_bahan_baku, function(result){
		if (result[0] == 'null'){
			console.log('Brand Not Found');
			res.status(400).json({status:'Brand Not Found'});
		} else {
			if(result[1] == 'null'){
				console.log('Store Not Found');
				res.status(404).json({status:'Store Not Found'});
			}else {
				if (result[2] == 'null'){
					console.log('Bahan Baku Not Found');
					res.status(404).json({status:'Bahan Baku Not Found'});
				} else {
					var field = "store."+result[1]+".bahan_baku."+result[2]+".deleted";
					var value = true;
					var obj = {};
						obj[field] = value;
					var dataold = {id_brand: req.body.id_brand};
					var datanew = obj;
					var options = {};
					BahanBaku.update(dataold, datanew, options, callback);
					function callback(err){
						if (err){
							console.log(err);
						} else {
							deleteDetailMenu(req.body.id_brand, req.body.id_store, req.body.id_bahan_baku, function(hasil){
								res.status(200).json({status:'Success'});
							});
						};
					};
				};
			};
		};
	});
});

//-------------
// BahanBaku-Show
router.get('/bahanbaku/:idBrand/:idStore', function(req, res, next){
	var data = BahanBaku.aggregate ([ {$match:{id_brand: req.params.idBrand}},
									  {$unwind: "$store"},
									  {$match:{"store.id_store": req.params.idStore}},
									  {$unwind: "$store.bahan_baku"},
									  {$match:{"store.bahan_baku.deleted": false}},
									  {$project:{bahan_baku: "$store.bahan_baku"} }
									]);
	data.exec(function(err, result){
		if(err){
			console.log(err);
		} else {
			if (result == ""){
				res.status(404).json({status:'Not Found'});
			} else {
				res.status(200).json(result);
			};
		};
	});
});
// =====================



function createIdBahanBaku(idBrand, idStore, nameBahanBaku, callback){
	//format BahanBaku = BB.XXXXXXXXXX
	var hasil = [];
	var idBahanBaku;
	var data = BahanBaku.findOne({id_brand: idBrand});
	data.exec(function(err, result){
		if(err){
			console.log(err);
		} else {
			if(result == null){
				hasil.push('null');//0
				idBahanBaku = "BB.0000000001";
				hasil.push(idBahanBaku);//1
				callback(hasil);
			} else {
				hasil.push('not null');//0
				var lS = result.store.length;
				var availabe = false;
				for (var i=0; i<lS; i++){
					if (availabe == true){
						break;
					} else {
						if (result.store[i].id_store == idStore){
							hasil.push('not null'); //1
							var lbb = result.store[i].bahan_baku.length;
							for (var j=0; j<lbb; j++){
								var bahanBakuResult = result.store[i].bahan_baku[j].name_bahan_baku.toLowerCase();
								var bahanBakuInput = nameBahanBaku.toLowerCase();
								if (bahanBakuResult == bahanBakuInput && result.store[i].bahan_baku[j].deleted == false){
									availabe = true;
									hasil.push('not null');//2
									break;
								};
								if (j == lbb-1 && availabe == false){
									hasil.push('null'); //2
									if (lbb+1 < 10){
										idBahanBaku = 'BB.000000000'+(lbb+1);
									} else if (lbb+1 < 100){
										idBahanBaku = 'BB.00000000'+ (lbb+1);
									} else if (lbb+1 <= 1000){
										idBahanBaku = 'BB.0000000'+ (lbb+1);
									} else if (lbb+1 <= 10000){
										idBahanBaku = 'BB.000000'+ (lbb+1);
									} else if (lbb+1 <= 100000){
										idBahanBaku = 'BB.00000'+ (lbb+1);
									} else if (lbb+1 <= 1000000){
										idBahanBaku = 'BB.0000'+ (lbb+1);
									} else if (lbb+1 <= 10000000){
										idBahanBaku = 'BB.000'+ (lbb+1);
									} else if (lbb+1 <= 100000000){
										idBahanBaku = 'BB.00'+ (lbb+1);
									} else if (lbb+1 <= 1000000000){
										idBahanBaku = 'BB.0'+ (lbb+1);
									} else if (lbb+1 <= 10000000000){
										idBahanBaku = 'BB.'+ (lbb+1);
									} else {
										idBahanBaku = 'maxBahanBaku';
									};
									hasil.push(idBahanBaku);//3
								};
							};
						};
					};
					if (availabe == false && i == lS-1){
						hasil.push('null'); //1
						idBahanBaku = 'BB.0000000001';
						hasil.push(idBahanBaku);//2
					};
				};
				callback(hasil);
			};
		};
	});	
};

function findBahanBaku(idBrand, idStore, idBahanBaku, nameBahanBaku, callback){
	var data = BahanBaku.findOne({id_brand:idBrand});
	data.exec(function(err, result){
		if(err){
			console.log(err);
		} else {
		var index;
		var hasil =[];
		var availableStore = false;
		var availableBahanBaku = false;
		var availableNameBahanBaku = false;
		var lS, lBB;
			if (result == null){
				hasil.push('null');//0
				callback(hasil);
			} else {
				hasil.push('not null');//0
				lS = result.store.length;
				for(var i=0; i<lS; i++){
					if (availableNameBahanBaku == true){
						break;
					} else {
						if (result.store[i].id_store == idStore){
							availableStore = true;
							hasil.push(i);//1
							lBB = result.store[i].bahan_baku.length;
							//cek id_bahan baku null or not null
							for (var j=0; j<lBB; j++){
								if (result.store[i].bahan_baku[j].id_bahan_baku == idBahanBaku && result.store[i].bahan_baku[j].deleted == false){
									hasil.push(j);//2 
									availableBahanBaku = true;
									for (var k=0; k<lBB; k++){
										var bahanBakuResult = result.store[i].bahan_baku[k].name_bahan_baku.toLowerCase();
										var bahanBakuInput = nameBahanBaku.toLowerCase();
										if (bahanBakuResult == bahanBakuInput && result.store[i].bahan_baku[k].id_bahan_baku != idBahanBaku && result.store[i].bahan_baku[k].deleted == false){
											availableNameBahanBaku = true;
											hasil.push('not null');//3
											break;
										};
										if (k == lBB-1 && availableNameBahanBaku == false){
											hasil.push('null');//3
										};
									};
									break;
								};
								if (j == lBB-1 && availableBahanBaku == false){
									hasil.push('null')//2
								};
							};
						};
						if (i == lS-1 && availableStore == false){
							hasil.push('null')//1
						};
					};
				};
				callback(hasil);
			};
		};
	});
};

function deleteDetailMenu(idBrand, idStore, idBahanBaku, callback){
	var data = Menu.findOne({id_brand: idBrand});
	data.exec(function(err, result){
		if(err){
			console.log(err);
		} else {
			if (result == null){
				callback(0); 
			} else {
				var lS = result.store.length;
				var aS = false;
				for (var i=0; i< lS; i++){ //store
					if(result.store[i].id_store == idStore){
						aS = true;
						var lM = result.store[i].menu.length;
						for ( var j=0; j< lM; j++){ //menu
							var lDM = result.store[i].menu[j].detail_menu.length;
							for (var k=0; k< lDM; k++){ //detail_menu
								if (result.store[i].menu[j].detail_menu[k].id_bahan_baku == idBahanBaku){ 
									var f = "store."+i+".menu."+j+".detail_menu."+k+".deleted";
									var v = true;
									var obj = {};
										obj[f] = v;
									var dataold = {id_brand: idBrand};
									var datanew = obj;
									var options = {};
									Menu.update(dataold, datanew, options, success);
									function success(err){
										if(err){
											console.log(err);
										};
									};
								};
								if (j == lM-1 && k == lDM-1){ //jk semua detail_menu sdh dihapus
									getBahanBakuNew( idBrand, idStore, function(hasil){
										callback(hasil);
									});
								}
							};
						};
					};
					if(aS == false && i == lS-1){
						callback(0); 
					};
				};
			};
		};
	});
};

function getBahanBakuNew (idBrand, idStore, callback){
	var data = BahanBaku.findOne({id_brand: idBrand});
	data.exec(function(err, result){
		if(err){
			console.log(err);
		} else {
			if (result == null){
				callback(0);
			} else {
				var lS = result.store.length;
				var aS = false;
				for (var i=0; i<lS; i++){
					if (result.store[i].id_store == idStore){ //store not null
						aS = true;
						updateAllStokMenu(idBrand, idStore, result.store[i].bahan_baku, function(hasil){
							callback(hasil);
						});
						break;
					};
					if (i == lS-1 && aS == false){ //store null
						callback(1);
					};
				};
			};
		};
	});
};

function updateAllStokMenu(idBrand, idStore, bahanBaku, callback){
	var data  = Menu.findOne({id_brand:idBrand});
	data.exec(function(err, result){
		if(err){
			console.log(err);
		} else {
			if(result == null){
				callback(1);
			} else {
				var lS = result.store.length;
				var aS = false;
				for (var i=0; i<lS; i++){
					if (result.store[i].id_store == idStore){ //store on collection menu
						aS = true;
						var lM = result.store[i].menu.length;
						for (var j=0; j<lM; j++){ //looping menu
							if (result.store[i].menu[j].deleted == false){ //menu yg blm dihapus
								var detailMenu = result.store[i].menu[j].detail_menu;
								var a = [];
								var min;
								if (detailMenu.length == 0){
									var field = "store."+i+".menu."+j+".stok";
									var value = 0;
									var obj ={};
										obj[field] = value;
									var dataold = {id_brand: idBrand};
									var datanew = obj;
									var options = {};
									Menu.update(dataold, datanew, options, success);
									function success (err){
										if (err){
											console.log(err);
										};
									};
								} else {
									for (var l=0; l<detailMenu.length; l++){ // looping detailMenu
										for (var m=0; m<bahanBaku.length; m++){ // looping bahanBaku
											if (detailMenu[l].id_bahan_baku == bahanBaku[m].id_bahan_baku && bahanBaku[m].deleted == false && detailMenu[l].deleted == false){ //jk id_bahan_baku collection detail_menu = id_bahan_baku collection bahan baku dan keduanya blm dihapus 
												var  b = bahanBaku[m].stok/detailMenu[l].jumlah;
												a.push(Math.floor(b)); // push hasil bagi ke var a
											};
										};
										if (l == detailMenu.length-1){ // cari nilai terkecil dari var array a
											if (a.length == 0){ 
												var field = "store."+i+".menu."+j+".stok";
												var value = 0;
												var obj ={};
													obj[field] = value;
												var dataold = {id_brand: idBrand};
												var datanew = obj;
												var options = {};
												Menu.update(dataold, datanew, options, success);
												function success (err){
													if (err){
														console.log(err);
													};
												};
											} else {
												if (a.length == 1){
													min = a[0];
												} else if (a.length == 2){
													if (a[0] <= a[1]){
														min = a[0]
													} else {
														min = a[1];
													}
												} else {
													for (var k=0; k<a.length; k++){
														if (k == 0){
															min = a[k];
														} else {
															if (a[k]<= min){
																min = a[k]
															};
														};
													};
												};
												var field = "store."+i+".menu."+j+".stok";
												var value = min;
												var obj ={};
													obj[field] = value;
												var dataold = {id_brand: idBrand};
												var datanew = obj;
												var options = {};
												console.log(datanew);
												Menu.update(dataold, datanew, options, success);
												function success (err){
													if (err){
														console.log(err);
													};
												};
											};
										};
									};
								};
							};
						};
						callback(1);
						break;
					};
					if (i == lS-1 && aS == false){
						callback(1);
						break;
					};
				};
			};
		};
	});
};