const saltRounds = 10;
var bcrypt = require('bcrypt');
var cors = require('cors');
var jwt = require('jsonwebtoken');
var nodemailer = require('nodemailer');
var uniqid = require('uniqid');
var path = require('path');
var hbs = require('nodemailer-express-handlebars');
var express = require('express'),
  app = express();
  router = express.Router(),
  mongoose = require('mongoose'),
  Registration = mongoose.model('Registration');
  User = mongoose.model('User');

module.exports = function (app) {
  app.use(cors());
  app.use('/v1', router);
};
app.set('superSecret', 'LoginRegister');



// =====================
//-------------
// Registration-Register
router.post('/register', function(req, res, next){
	//activation nya di link ke halaman login (email dan password)
	var data = Registration.findOne({$and:[{email: req.body.email}]});
	data.exec(function(err, result){
		if (err){
			console.log(err);
		} else {
			if (result == null){
					var activation_code = uniqid('kikaPOS');
					bcrypt.hash(req.body.password, saltRounds, function(err, hash){
						var input = new Registration ({ email: req.body.email,
														password: hash,
														activation_code: activation_code,
														status: false
													 });
						input.save(function(err, result){
							if(err){
								console.log(err);
							} else {
								res.status(200).json({status:'Success'});
								//sendmail sementara di tiadakan, tunggu mail server available
								/*sendMailRegister(req.body.email, activation_code, function(message){
									if(message == 'error'){
										var data = Registration.remove({_id: result._id});
										data.exec(function(err){
											if(err){
												console.log(err);
											} else {
												res.status(400).json({status:'Email Not Send please input form again'});
											}
										});
									} else {
										res.status(200).json({status:'Email sent succesfully'});
									};
								});*/
							};
						});
					});
			} else {
				res.status(409).json({status:'Email Conflict'});
			};
		};
	});
});

//-------------
// Registration-Login
router.post('/loginregister', function(req, res, next){
	var data = Registration.findOne({$and:[{status:false},{email:req.body.email}]});
	data.exec(function(err, result){
		if (err){
			console.log(err);
		} else {
			if(result == null){
				res.status(404).json({status:'Email Not Found'});
			} else {
				bcrypt.compare(req.body.password, result.password, function(err, compare){
					if (compare == true){
						var token = jwt.sign(result, app.get('superSecret'),{
							expiresIn: 60*60
						});
						info_token = jwt.decode(token);
						res.status(200).json({status:'Login Success',
								  email: info_token._doc.email,
								  password: info_token._doc.password
								});
					} else {
						res.status(404).json({status:'Password Not Found'});
					};
				});
			};
		};
	});
});


//-------------
// Registration-Check Activation
router.get('/register/:activation', function(req, res, next){
	var data = Registration.findOne({activation_code: req.params.activation});
	data.exec(function(err, result){
		if(err){
			console.log(err);
		} else {
			if (result == null){
				res.status(404).json({status:'Not Found'});
			} else {
				res.status(200).json({status:'Success'});
			};
		};
	});
});


//-------------
// Registration-Delete Activation
router.put('/register/deleteActivation', function(req, res, next){
	var data = Registration.findOne({email: req.body.email});
	data.exec(function(err, result){
		if (err){
			console.log(err);
		} else {
			if (result == null){
				res.status(404).json({status:'Not Found'});
			} else {
				var dataold = {_id: result._id};
				var datanew = {activation_code: ""};
				var options = {};
				Registration.update(dataold, datanew, options, callback);
				function callback(err) {
					if (err){
						console.log(err);
					} else {
						res.status(200).json({status:'Success'});
					}
				};
			};
		};
	});
});
// =====================



function sendMailRegister(email,activationCode,callback){
	var smtpTransport = nodemailer.createTransport({
		host: 'smtp.mailgun.org',
		secureConnection: true,
		port: 465,
		auth: {
			user: 'no-reply@mail.erixsoekamti.com',
    		pass: 'doesuniversity1787'
		}
	});
	var options = {
		viewEngine: {
			extname: 'hbs',
			layoutsDir: path.dirname('app/views/email/...'),
			defaultLayout: 'welcome-bfl',
			partialsDir: path.dirname('app/views/partials/...')
		},
		viewPath: 'app/views/email',
		extName: '.hbs'
	};
	smtpTransport.use('compile', hbs(options));
	var mailOptions = {
		from: 'KikaPOS <support@KikaPOS.com>',
		to: email,
		subject: 'Activate KikaPOS License',
		template: 'welcome-bfl', 
		context: {
			email: email,
			link: activationCode
		}
	};
	smtpTransport.sendMail(mailOptions, function(error, response){
		if(error){
			console.log('Email could not sent, error:'+error);
			callback('error');
		} else {
			console.log('Email has been sent succesfully');
			callback('success');
		};
	});
};

function findUser(email, callback){
  var lB,lS,lU;
  var hasil =[];
  var available = false; 
  var data = User.find({});
  data.exec(function(err, result){
    if (err){
      console.log(err);
    } else {
      console.log(result.length);
      lB = result.length;
      if (lB == 0){
        callback (null);
      } else {
        for (var i=0; i<lB; i++){
          if (available == true){
            break;
          } else {
            lS = result[i].store.length;
            for (var j=0; j<lS; j++){
              if (available == true){
                break;
              } else {
                lU = result[i].store[j].user.length;
                for (var k=0; k<lU; k++){
                  if (available == true){
                    break;
                  } else {
                    if (result[i].store[j].user[k].info_user.email == email){
                      available = true;
                      if (result[i].status == false){
                        callback(false);
                      } else {
                        var field = [];
                          field[0] = "id_brand";
                          field[1] = "id_store";
                          field[2] = "id_user";
                          field[3] = "email";
                          field[4] = "id_role";
                          field[5] = "iBrand";
                          field[6] = "iStore";
                          field[7] = "iUser";
                        var value = [];
                          value[0] = result[i].id_brand;
                          value[1] = result[i].store[j].id_store;
                          value[2] = result[i].store[j].user[k].info_user.id_user;
                          value[3] = result[i].store[j].user[k].info_user.email;
                          value[4] = result[i].store[j].user[k].info_user.role.id_role;
                          value[5] = i;
                          value[6] = j;
                          value[7] = k;
                        var obj = {};
                        for (var x=0; x<8; x++){
                          obj[field[x]] = value[x];
                        };
                        callback(obj);
                      };
                    } else if (i == lB-1 && j == lS-1 && k == lU-1 && available == false){
                      callback(null);
                    };
                  };
                };
              };
            };
          };
        };
      };
    };
  });
};