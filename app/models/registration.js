var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var RegistrationSchema = new Schema({
    email:{
        type: String
    },
    password:{
        type: String
    },
    activation_code: {
        type: String
    },
    status:{
        type: Boolean
    }
}); 

mongoose.model('Registration', RegistrationSchema);