var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var MenuSchema = new Schema({
  id_brand:{
    type: String
  },
  store:[{
    id_store:{
        type: String
    },
    menu:[{
        id_menu:{
          type: String
        },
        name_menu:{
          type: String
        },
        price:{
          type: Number
        },
        stok:{
          type: Number,
          default: 0
        },
        path_image:{
          type: String,
          default: "http://localhost:3001/v1/public/image_upload/image_menu/defaultMenu.jpg"
        },
        detail_menu:[{
          id_bahan_baku:{
            type: String
          },
          name_bahan_baku:{
            type: String
          },
          jumlah:{
            type: Number
          },
          satuan:{
            id_satuan:{
              type: String
            }, 
            name_satuan:{
              type: String
            }
          },
          deleted:{
            type: Boolean, 
            default: false
          }
        }],
        kategori:{
          id_kategori_menu:{
            type:String
          },
          name_kategori_menu:{
            type: String
          }
        }, 
        deleted:{
          type: Boolean, 
          default: false
        }
    }]
  }]
}); 

mongoose.model('Menu', MenuSchema);