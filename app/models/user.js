var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var UserSchema = new Schema({
    id_brand:{
      type: String
    },
    name_brand:{
      type: String
    },
    store:[{
      id_store:{
        type: String
      }, 
      name_store:{
        type: String
      },
      user:[{
        info_user:{
          id_user:{
            type: String
          },
          email:{
            type: String
          },
          username:{
            type: String
          }, 
          password:{
            type: String
          },
          phone:{
            type: String
          },
          path_image:{
            type: String,
            default: "http://localhost:3001/v1/public/image_upload/image_user/defaultAva.png"
          },
          role: {
            id_role:{
              type: Number
            },
            name_role:{
              type: String
            } 
          },
          deleted:{
            type: Boolean,
            default: false
          }
        }
      }]
    }],
    status:{
      type: Boolean
    }
}); 

mongoose.model('User', UserSchema);