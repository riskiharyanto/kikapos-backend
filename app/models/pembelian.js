var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var PembelianSchema = new Schema({
  id_brand:{
    type: String
  },
  store:[{
    id_store:{
        type: String
    },
    pembelian:[{
        id_pembelian:{
          type: String
        },
        tanggal:{
          type: Date
        },
        jam:{
          type: Date
        },
        user:{
          id_user:{
            type: String
          },
          username:{
            type: String
          }
        },
        detail_pembelian:[{
          id_bahan_baku:{
            type: String
          },
          name_bahan_baku:{
            type: String
          },
          satuan:{
            id_satuan:{
              type: String
            },
            name_satuan:{
              type: String
            }
          },
          price:{
            type: Number
          },
          qty:{
            type: Number
          },
          sub_total:{
            type: Number
          },
          cancel:{
            type: Boolean,
            default: false
          }
        }],
        total_pembelian:{
          type: Number
        },
        deleted:{
          type: Boolean,
          default: false
        },
        cancel:{
          type: Boolean,
          default: false
        }
    }]
  }]
}); 

mongoose.model('Pembelian', PembelianSchema);