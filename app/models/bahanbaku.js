var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var BahanBakuSchema = new Schema({
  id_brand:{
    type: String
  },
  store:[{
    id_store:{
        type: String
    }, 
    bahan_baku:[{
        id_bahan_baku:{
          type: String
        },
        name_bahan_baku:{
          type: String
        },
        stok:{
          type: Number
        },
        satuan:{
          id_satuan:{
            type: String
          },          
          name_satuan:{
            type: String
          }
        },
        deleted:{
          type: Boolean, 
          default: false
        }
    }]
  }]
}); 

mongoose.model('BahanBaku', BahanBakuSchema);