var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var PenjualanSchema = new Schema({
  id_brand:{
    type: String
  },
  store:[{
    id_store:{
        type: String
    },
    penjualan:[{
        id_penjualan:{
          type: String
        },
        tanggal:{
          type: Date
        },
        jam:{
          type: Date,
          default: Date.now
        },
        user:{
          id_user:{
            type: String
          },
          username:{
            type: String
          }
        },
        detail_penjualan:[{
          id_menu:{
            type: String
          },
          name_menu:{
            type: String
          },
          price:{
            type: Number
          },
          qty:{
            type: Number
          },
          sub_total:{
            type: Number
          },
          cancel:{
            type: Boolean,
            default: false
          },
          note:{
            type: String, 
            default: null,
          },
          take_away:{
            type: Boolean
          }, 
          eat_in:{
            type: Boolean
          },
          kategori:{
            id_kategori_menu:{
              type:String
            },
            name_kategori_menu:{
              type: String
            }
          }
        }],
        total_penjualan:{
          type: Number
        },
        customer:{
          type: String,
          default: null
        },
        bayar:{
          type: Number,
          default: null
        },
        kembalian:{
          type: Number,
          default: null
        },
        checkout:{
          type: Boolean, 
          default: false
        },
        deleted:{
          type: Boolean,
          default: false
        },
        cancel:{
          type: Boolean,
          default: false
        }
    }]
  }]
}); 

mongoose.model('Penjualan', PenjualanSchema);