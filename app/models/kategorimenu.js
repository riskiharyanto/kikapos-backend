var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var KategoriMenuSchema = new Schema({
	id_brand:{
 		type: String
	}, 
	kategori_menu:[{
 		id_kategori_menu:{
  			type: String
 		},
 		name_kategori_menu:{
  			type: String
 		},
 		deleted:{
 			type: Boolean, 
 			default: false
 		}
	}]
}); 

mongoose.model('KategoriMenu', KategoriMenuSchema);