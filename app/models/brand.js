var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var BrandSchema = new Schema({
    id_brand:{
      type: String,
      unique: true
    }, 
    name_brand:{
      type: String
    },
    path_image:{
      type: String,
      default: "http://localhost:3001/v1/public/image_upload/image_brand/defaultBrand.jpg"
    },
    address_brand:{
      detail:{
        type: String
      },
      city_id:{
        type: Number
      }, 
      city_name:{
        type:String
      },
      province_id:{
        type: Number
      },
      province_name:{
        type: String
      }
    }, 
    contact:{
      email:{
        type: String
      },
      phone:{
        type: String 
      }
    }, 
    social_media:{
      fb:{
        type: String,
        default: null
      }, 
      ig:{
        type: String,
        default: null
      }
    },
    store:[{
      id_store:{
        type: String
      },
      name_store:{
        type: String, 
        default: null
      },
      path_image:{
        type: String,
        default: "http://localhost:3001/v1/public/image_upload/image_brand/defaultBrand.jpg"
        },
      address_store:{
        detail:{
          type: String
        },
        city_id:{
          type: Number
        }, 
        city_name:{
          type:String
        },
        province_id:{
          type: Number
        },
        province_name:{
          type: String
        }
      },
      social_media_store:{
        fb:{
          type: String,
          default: null
        }, 
        ig:{
          type: String,
          default: null
        }
      },
      contact_store:{
        email_store:{
          type: String,
          default: null
        },
        phone_store:{
          type: String,
          default: null
        }
      },
      deleted:{
        type: Boolean,
        default: false
      }
    }],
    status:{
      type: Boolean,
      default: false
    }, 
    date_member:{
      type: Date,
      default: null
    }   
});

mongoose.model('Brand', BrandSchema);