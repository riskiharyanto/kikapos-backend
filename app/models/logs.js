var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var LogsSchema = new Schema({
	id_brand:{
 		type: String
	},
	store:[{
	 	id_store:{
	  		type: String
	 	},
	 	activity:[{
	  		id_activity:{
	   			type: String
	  		},
	  		name_activity:{
	   			type: String
	  		},
	  		user:{
	   			id_user:{
	    			type: String
	   			},
	   			name_user:{
	    			type: String
	   			}
	  		},
	  		tanggal:{
	   			type: Date
	  		},
	  		jam:{
	   			type: Date
	  		},
	  		menu:{
	   			id_menu:{
	    			type: String
	   			},
	   			name_menu_old:{
	    			type: String
	   			},
	   			name_menu_new:{
	    			type: String
	   			},
	   			price_old:{
	    			type: Number
	   			},
	   			price_new:{
	    			type: Number
	   			},
	   			detail_menu_old:[{
	    			id_bahan_baku:{
	     				type: String
	    			},
	    			name_bahan_baku:{
	     				type: String
	    			},
	    			jumlah:{
	     				type: Number
	    			},
	    			name_satuan:{
	     				type: String
	    			}
	   			}],
	   			detail_menu_new:[{
	    			id_bahan_baku:{
	     				type: String
	    			},
	    			name_bahan_baku:{
	     				type: String
	    			},
	    			jumlah:{
	     				type: Number
	    			},
	    			name_satuan:{
	     				type: String
	    			}
	   			}]
	  		}
	 	}]
	}]
}); 

mongoose.model('Logs', LogsSchema);