var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var SatuanBahanSchema = new Schema({
	id_brand:{
 		type: String,
 		unique: true
	}, 
	satuan:[{
 		id_satuan:{
  			type: String
 		},
 		name_satuan:{
  			type: String
 		},
 		deleted:{
 			type: Boolean, 
 			default: false
 		}
	}]
}); 

mongoose.model('SatuanBahan', SatuanBahanSchema);