var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'kikapos-backend-push'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/kikapos-backend-push-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'kikapos-backend-push'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/kikapos-backend-push-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'kikapos-backend-push'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/kikapos-backend-push-production'
  }
};

module.exports = config[env];
